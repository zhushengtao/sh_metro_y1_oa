﻿using System.Configuration;
using System.Data.Entity;
using GeneralFramework.Model;
namespace GeneralFramework.EntityFreamework
{
    //[DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class GeneralLogContext : DbContext
    {
        public GeneralLogContext()
            : base("Name=GeneralLogContext")
        {
            var strategy = new LogContextInitializer();
            if (ConfigurationManager.AppSettings["AutomaticMigrationsEnabled"] == "false")
            {
                strategy = null;
            }
            Database.SetInitializer(strategy);
        }

        public DbSet<LogsBase> LogsBase { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties<decimal>().Configure(config => config.HasPrecision(20, 6));
        }
    }

    /// <summary>
    /// The db initialize configuration class
    /// </summary>
    public class LogContextInitializer : MigrateDatabaseToLatestVersion<GeneralLogContext, LogMigrationConfiguration>
    {

    }
}
