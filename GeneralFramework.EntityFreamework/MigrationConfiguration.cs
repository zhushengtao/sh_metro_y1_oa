﻿using System.Data.Entity.Migrations;

namespace GeneralFramework.EntityFreamework
{
    public class MigrationConfiguration : DbMigrationsConfiguration<GeneralContext>
    {
        public MigrationConfiguration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }
    }

    public class LogMigrationConfiguration : DbMigrationsConfiguration<GeneralLogContext>
    {
        public LogMigrationConfiguration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }
    }

   
}
