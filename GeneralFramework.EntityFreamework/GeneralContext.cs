﻿using System.Configuration;
using System.Data.Entity;
using GeneralFramework.Model;
namespace GeneralFramework.EntityFreamework
{
    //[DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class GeneralContext : DbContext
    {
        public GeneralContext()
            : base("Name=GeneralContext")
        {
            var strategy = new RASContextInitializer();
            //if (ConfigurationManager.AppSettings["AutomaticMigrationsEnabled"] == "false")
            //{
            //    strategy = null;
            //}
            Database.SetInitializer(strategy);
            Database.CommandTimeout = 60;
        }
        public DbSet<SystemUser> SystemUser { get; set; }
        public DbSet<System_CompanyBase> System_CompanyBase { get; set; }
        public DbSet<System_MenuBase> System_MenuBase { get; set; }
        public DbSet<System_DepartmentBase> System_DepartmentBase { get; set; }
        public DbSet<System_RolesBase> System_RolesBase { get; set; }
        public DbSet<System_JobBase> System_JobBase { get; set; }
        public DbSet<System_MenuMappingBase> System_MenuMappingBase { get; set; }
        public DbSet<Human_EmpExtendMappingBase> Human_EmpExtendMappingBase { get; set; }
        public DbSet<Human_EmployeeBase> Human_EmployeeBase { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties<decimal>().Configure(config => config.HasPrecision(20, 6));
        }
    }

    /// <summary>
    /// The db initialize configuration class
    /// </summary>
    public class RASContextInitializer : MigrateDatabaseToLatestVersion<GeneralContext, MigrationConfiguration>
    {

    }
}
