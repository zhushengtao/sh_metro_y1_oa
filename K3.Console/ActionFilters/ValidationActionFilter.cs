﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.ModelBinding;
using Newtonsoft.Json.Linq;
using GeneralFramework.BLL;
using GeneralFramework.Infrastructure.Utilities;
using GeneralFramework.Security;
using GeneralFramework.DependencyResolution;
using GeneralFramework.ViewModel;
using GeneralFramework.Infrastructure;

namespace GeneralFramework.Infrastructure.ActionFilters
{
    public class ValidationActionFilter : ActionFilterAttribute
    {

        public static readonly List<SimpleAccessLog> SimpleAccessLogs = new List<SimpleAccessLog>();

        private static readonly int VisitLimitPerMinutePerIP = 1000;
        private static readonly int VisitLimitPerMinutePerUser = 1000;
        private static readonly int VisitLimitPerMinutePerSession = 1000;

        public override void OnActionExecuting(HttpActionContext context)
        {
            SessionHelper.Add("SessionId", HttpContext.Current.Session.SessionID);
            var accountService = IoC.GetContainer().GetInstance<SystemUserService>();

            var identity = context.RequestContext.Principal.Identity;

            if (identity != null && identity.IsAuthenticated)
            {
                var lsIdentity = identity as GeneralIdentity;

                if (lsIdentity != null)
                {
                    if (SessionHelper.CurrentUser != null)
                    {
                        if (lsIdentity.SystemUserId != SessionHelper.CurrentUser.Id)
                        {
                            SessionHelper.CurrentUser = new SystemUserVM(accountService.GetById(lsIdentity.SystemUserId));
                        }
                    }
                    else
                    {
                        SessionHelper.CurrentUser = new SystemUserVM(accountService.GetById(lsIdentity.SystemUserId));
                    }
                }
            }

            var sLog = new SimpleAccessLog
            {
                AccessTime = DateTime.Now,
                IPAddress = HttpContext.Current.Request.UserHostAddress
            };

            if (HttpContext.Current.Session != null)
            {
                sLog.SessionId = HttpContext.Current.Session.SessionID;

                if (SessionHelper.CurrentUser != null)
                {
                    sLog.AccountId = SessionHelper.CurrentUser.Id;
                }
            }

            lock (SimpleAccessLogs)
            {
                SimpleAccessLogs.Add(sLog);
                var minuteAgo = DateTime.Now.AddMinutes(-1);
                SimpleAccessLogs.RemoveAll(l => l.AccessTime < minuteAgo);
                var listOfThisIp = SimpleAccessLogs.Where(x => x.IPAddress == sLog.IPAddress).ToList();
                if (listOfThisIp.Count > VisitLimitPerMinutePerIP)
                {
                    HttpContext.Current.Response.Close();
                }
                if (sLog.AccountId != null)
                {
                    var listOfThisUser = SimpleAccessLogs.Where(x => x.AccountId == sLog.AccountId).ToList();
                    if (listOfThisUser.Count > VisitLimitPerMinutePerUser)
                    {
                        HttpContext.Current.Response.Close();
                    }
                }
                if (sLog.SessionId != null)
                {
                    var listOfThisSession = SimpleAccessLogs.Where(x => x.SessionId == sLog.SessionId).ToList();
                    if (listOfThisSession.Count > VisitLimitPerMinutePerSession)
                    {
                        HttpContext.Current.Response.Close();
                    }
                }
            }

            ModelStateDictionary modelState = context.ModelState;

            if (modelState.IsValid)
            {
                return;
            }
            JObject errors = new JObject();
            foreach (string key in modelState.Keys)
            {
                var state = modelState[key];
                if (state.Errors.Count == 0)
                {
                    continue;
                }
                string errorMessage = state.Errors[0].ErrorMessage;
                errors[key] = errorMessage;
            }
            context.Response = context.Request.CreateResponse(HttpStatusCode.BadRequest, errors);
        }
    }
}
