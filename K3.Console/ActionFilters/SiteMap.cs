﻿namespace GeneralFramework.Infrastructure.ActionFilters
{
    public enum SiteMap
    {
        DeFault,
    }

    public static class SiteMapHelper
    {
        public static string GetDefaultLoginPage(this SiteMap siteMap)
        {
            var url = "/";
            switch (siteMap)
            {
                case SiteMap.DeFault:
                    {
                        url = "/Web/Login";
                    }
                    break;
              
            }
            return url;
        }
    }
}
