﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http;
using GeneralFramework.Common;
using GeneralFramework.Common.Exceptions;
using GeneralFramework.Common.Utilities;
using GeneralFramework.BLL;

using System.Net;
using System.Net.Http;
using GeneralFramework.DependencyResolution;
using GeneralFramework.Infrastructure.Utilities;
using GeneralFramework.ViewModel;
using GeneralFramework.Security;

namespace GeneralFramework.Infrastructure.ActionFilters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    public class ApiAuthorizeAttribute : AuthorizationFilterAttribute
    {
        private readonly string[] _privileges;

        public ApiAuthorizeAttribute(params string[] privileges)
        {
            _privileges = privileges;
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
          
            if (SkipAuthorization(actionContext))
            {
                return;
            }

            var identity = actionContext.RequestContext.Principal?.Identity;

            if (identity != null && identity.IsAuthenticated)
            {
                var GeneralIdentity = identity as GeneralIdentity;

                if (GeneralIdentity == null)
                {
                    throw new AuthorizeException(ErrorCode.Unauthorized);
                }

                if (SessionHelper.CurrentUser == null)
                {
                    SessionHelper.CurrentUser = new SystemUserVM(IoC.GetContainer().GetInstance<SystemUserService>().GetById(GeneralIdentity.SystemUserId).Entity);
                }



                if (_privileges.IsNullOrEmpty())
                {
                    return;
                }
            }
            else
            {
                SessionHelper.CurrentUser = null;
              
            }
        }

        private static bool SkipAuthorization(HttpActionContext actionContext)
        {
            if (actionContext == null)
            {
                throw new ArgumentNullException("actionContext");
            }

            bool shouldSkip = actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>(true).Any() || actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>(true).Any();
            return shouldSkip;
        }
    }
}
