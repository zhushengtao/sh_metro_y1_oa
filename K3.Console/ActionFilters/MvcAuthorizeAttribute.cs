﻿using System;
using System.Security.Principal;
using System.Web.Mvc;
using GeneralFramework.BLL;
using GeneralFramework.Common.Utilities;
using GeneralFramework.Infrastructure.Utilities;
using GeneralFramework.Security;
using System.Threading;
using System.Globalization;
using GeneralFramework.DependencyResolution;
using GeneralFramework.Common.Exceptions;
using GeneralFramework.Common;
using System.IO;
using GeneralFramework.ViewModel;
using System.Linq;
using System.Collections.Generic;

namespace GeneralFramework.Infrastructure.ActionFilters
{

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    public class MvcAuthorizeAttribute : AuthorizeAttribute
    {
        private readonly string[] _privileges;
        private readonly SiteMap _siteMap;

        public MvcAuthorizeAttribute(SiteMap siteMap = SiteMap.DeFault, params string[] privileges)
        {
            _privileges = privileges;
            _siteMap = siteMap;
        }
        public override void OnAuthorization(AuthorizationContext authorizationContext)
        {
            if (SkipAuthorization(authorizationContext))
            {
                return;
            }

            IIdentity identity = authorizationContext.RequestContext.HttpContext.User == null ? null : authorizationContext.RequestContext.HttpContext.User.Identity;

            if (identity != null && identity.IsAuthenticated)
            {
                if (SessionHelper.CurrentUser == null)
                {
                    if (identity is GeneralIdentity)
                    {
                        //不是通过api登录的，如cookie登录。 CurrentUser需要赋值。
                        SessionHelper.CurrentUser = new SystemUserVM(IoC.GetContainer().GetInstance<SystemUserService>().GetById((identity as GeneralIdentity).SystemUserId));
                    }
                    else
                    {
                        if (identity.AuthenticationType == "Jasig CAS")
                        {
                            authorizationContext.HttpContext.Response.Write(string.Format("<script>top.location.href='{0}'</script>", _siteMap.GetDefaultLoginPage()));
                            authorizationContext.HttpContext.Response.End();
                            return;
                        }

                    }
                }

              
                if (_privileges.IsNullOrEmpty())
                {
                    return;
                }

                var GeneralIdentity = identity as GeneralIdentity;
                // todo: ensure this check is required
                if (GeneralIdentity == null)
                {
                    if (identity.AuthenticationType == "Jasig CAS")
                    {
                        authorizationContext.HttpContext.Response.Write(string.Format("<script>top.location.href='{0}'</script>", _siteMap.GetDefaultLoginPage()));
                        authorizationContext.HttpContext.Response.End();
                        return;
                    }
                    throw new AuthorizeException(ErrorCode.Unauthorized);
                }               
            }
            else
            {

               
                authorizationContext.HttpContext.Response.Write(string.Format("<script>top.location.href='{0}'</script>", _siteMap.GetDefaultLoginPage()));
                authorizationContext.HttpContext.Response.End();
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            //filterContext.Result=new RedirectResult(_siteMap.GetDefaultLoginPage());
            //由于是iframe 前端显示是部分iframe里边显示为login页。不是iframe的话推荐使用上边的。
            //所以改成js强制刷新一下

            filterContext.HttpContext.Response.Write(string.Format("<script>top.location.href='{0}'</script>", _siteMap.GetDefaultLoginPage()));
            filterContext.HttpContext.Response.End();
        }

        private static bool SkipAuthorization(AuthorizationContext authorizationContext)
        {
            if (authorizationContext == null)
            {
                throw new ArgumentNullException("authorizationContext");
            }

            bool skipAuthorization = authorizationContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true)
                || authorizationContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true);

            return skipAuthorization;
        }

       
    }

    public static class HelperExtensions
    {
        public static string RenderViewToString(this ControllerContext context, string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = context.RouteData.GetRequiredString("action");

            context.Controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(context, viewName);
                var viewContext = new ViewContext(context, viewResult.View, context.Controller.ViewData, context.Controller.TempData, sw);
                try
                {
                    viewResult.View.Render(viewContext, sw);
                }
                catch (Exception ex)
                {
                    throw;
                }

                return sw.GetStringBuilder().ToString();
            }
        }
    }
}
