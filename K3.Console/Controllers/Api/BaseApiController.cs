﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using GeneralFramework.Infrastructure.Utilities;
using GeneralFramework.ViewModel;
using GeneralFramework.Common;
using GeneralFramework.Model;
using GeneralFramework.BLL;
using System.Net;
using System.ServiceModel.Channels;
namespace GeneralFramework.Console.Controllers.Api
{
    public class BaseApiController : ApiController
    {
        public BaseApiController()
        {
        }

        public static SystemUserVM CurrentUser
        {
            get { return SessionHelper.CurrentUser; }
            set { SessionHelper.CurrentUser = value; }
        }

        protected BaseResponse RelResponse(EntityResponse response)
        {
            if (response.IsSuccess && response.Entity != null)
            {
                return BaseResponse.Success();
            }

            if (response.IsSuccess && response.Entity == null)
            {
                return ErrorCode.DatabaseError;
            }

            return response.ErrorCode;
        }

        /// <summary>
        /// 返回true 为安全，否则，可能会有sql注入攻击
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        protected bool ProcessSqlStr(string inputString)
        {
            var sqlStr = @"and|or|exec|execute|insert|select|delete|update|alter|create|drop|count|\*|chr|char|asc|mid|substring|master|truncate|declare|xp_cmdshell|restore|backup|net +user|net +localgroup +administrators";
            try
            {
                if (!string.IsNullOrEmpty(inputString))
                {
                    var reg = new Regex(@"\b(" + sqlStr + @")\b", RegexOptions.IgnoreCase);
                    if (reg.IsMatch(inputString))
                    { return false; }
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

    
        protected virtual ResponseT Execute<ResponseT, ParamT>(string functionName, ParamT param, Func<LogsBase, ResponseT> Handle)
        {
            var sw = new Stopwatch();
            sw.Start();

            ResponseT response = default(ResponseT);
            var log = new LogsBase()
            {
                Id = Guid.NewGuid(),
                methodName = functionName,
                typeCode = LogTypeCode.WebApi,
                tags = LogTags.Info,
                Remarks = Dns.GetHostName()
            };
            try
            {
                log.requestContent = SystemLogsBaseService.GetJson(param);
                log.AppendLine("访问者IP：" + GetClientIp(Request));
                if (CurrentUser != null)
                {
                    log.ModifiedBy = CurrentUser.Id;
                    log.AppendLine(":" + CurrentUser.Username);
                }
                response = Handle(log);
                try
                {
                    log.responseContent = SystemLogsBaseService.GetJson(response);
                }
                catch (Exception e)
                {
                    log.AppendLine("response 序列化失败 使用JsonConvert 记录");
                    try
                    {
                        log.responseContent = JsonConvert.SerializeObject(response);
                    }
                    catch (Exception)
                    {

                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                log.tags = LogTags.Error;

                log.Id = Guid.NewGuid();
                log.AppendLine("Exception:" + ex.ToString());
                throw ex;
            }
            finally
            {
                var baseResponse = response as BaseResponse;
                if (baseResponse != null)
                {
                    if (!baseResponse.IsSuccess)
                    {
                        log.tags = LogTags.Warn;
                    }
                }
                sw.Stop();
                log.duration = sw.ElapsedMilliseconds;
                SystemLogsBaseService.Instance.Create(log, true);
            }
        }

        protected string GetClientIp(HttpRequestMessage request)
        {
            if (request.Properties.ContainsKey("MS_HttpContext"))
            {
                return ((HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;
            }
            else if (request.Properties.ContainsKey(RemoteEndpointMessageProperty.Name))
            {
                var prop = request.Properties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
                return prop != null ? prop.Address : "";
            }
            else if (HttpContext.Current != null)
            {
                return HttpContext.Current.Request.UserHostAddress;
            }
            else
            {
                return null;
            }
        }     
    }
}