﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GeneralFramework.BLL;
using GeneralFramework.Common;
using GeneralFramework.ViewModel;
using System.Web.Http;
using GeneralFramework.Model;
using System.Net;
using GeneralFramework.Security;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Net.Http;
using GeneralFramework.Infrastructure.Utilities;

namespace GeneralFramework.Console.Controllers.Api
{
    [RoutePrefix("api/employee")]
    public class Human_EmployeeBaseApiController : BaseApiController
    {
        private readonly SystemUserService _systemUserService;
        private readonly Human_EmployeeBaseService _employeeService;
        public Human_EmployeeBaseApiController(SystemUserService systemUserService, Human_EmployeeBaseService employeeService)
        {
            _employeeService = employeeService;
            _systemUserService = systemUserService;
        }
        /// <summary>
        /// 创建
        /// </summary>
        /// <returns></returns>
        [Route("add"), HttpPost]
        public BaseResponse add(Human_EmployeeBaseVM vm)
        {
            return SystemLogsBaseService.Execute("employeeAdd", LogTypeCode.WebApi, "", (log) =>
            {
                if (string.IsNullOrEmpty(vm.EmpName))
                {
                    return new BaseResponse(ErrorCode.DatabaseError, "员工姓名为空");
                }

                if (string.IsNullOrEmpty(vm.EmpWorkID))
                {
                    return new BaseResponse(ErrorCode.DatabaseError, "工号为空");
                }

                var isExt = _employeeService.GetAll().Any(c=>c.EmpWorkID==vm.EmpWorkID);
                if (isExt)
                {
                    return new BaseResponse(ErrorCode.DatabaseError, "工号已存在");
                }
               
                var entity = new Human_EmployeeBase();

                entity.EmpName = vm.EmpName;
                entity.EmpUsedName = vm.EmpUsedName;
                entity.EmpSex = vm.EmpSex;
                entity.EmpBirthday = vm.EmpBirthday;
                entity.EmpDdentityNumber = vm.EmpDdentityNumber;
                entity.EmpWorkID = vm.EmpWorkID;
                entity.EmpOldWorkID = vm.EmpOldWorkID;
                entity.EmpEmail = vm.EmpEmail;
                entity.EmpTelePhone = vm.EmpTelePhone;
                entity.EmpOfficePhone = vm.EmpOfficePhone;
                entity.EmpFax = vm.EmpFax;
                entity.EmpCoID = vm.EmpCoID;
                entity.EmpDeptID = vm.EmpDeptID;
                entity.EmpJobID = vm.EmpJobID;
                entity.EmpJobNote = vm.EmpJobNote;
                entity.EmpBirthplace = vm.EmpBirthplace;
                entity.EmpNationality = vm.EmpNationality;
                entity.EmpPlaceOfBirth = vm.EmpPlaceOfBirth;
                entity.EmpAddress = vm.EmpAddress;
                entity.EmpPoliticalStatus = vm.EmpPoliticalStatus;
                entity.EmpType = vm.EmpType;
                entity.EmpJoinYearMonth = vm.EmpJoinYearMonth;
                entity.EmpPosition = vm.EmpPosition;
                entity.EmpIdentitydate = vm.EmpIdentitydate;
                entity.EmpEducation = vm.EmpEducation;
                entity.EmpDegree = vm.EmpDegree;
                entity.EmpBankCardNumber = vm.EmpBankCardNumber;
                entity.EmpProvidentFundAccount = vm.EmpProvidentFundAccount;
                entity.EmpMaritalStatus = vm.EmpMaritalStatus;
                entity.EmpZipCode = vm.EmpZipCode;
                entity.EmpMainAccountLocation = vm.EmpMainAccountLocation;
                entity.EmpJoinWorkTime = vm.EmpJoinWorkTime;
                entity.EmpJoinCompanyYear = vm.EmpJoinCompanyYear;
                entity.EmpHomePhone = vm.EmpHomePhone;
                entity.EmpLanguageLevel = vm.EmpLanguageLevel;
                entity.EmpSocialSecurityMonth = vm.EmpSocialSecurityMonth;
                entity.EmpNote = vm.EmpNote;
                entity.EmpSortNo = vm.EmpSortNo;
                entity.EmpState = vm.EmpState;

                var rel = _employeeService.Create(entity) ;

                if (!rel.IsSuccess)
                {
                    return rel.ErrorCode;
                }
                _employeeService.CreateExtendMapping(rel.Entity.Id,rel.Entity.EmpDeptID, 1);
                _employeeService.CreateExtendMapping(rel.Entity.Id, rel.Entity.EmpJobID, 2);
                return new BaseResponse();
            });
        }
        [Route("update"), HttpPost]
        public BaseResponse update(Human_EmployeeBaseVM vm)
        {
            return SystemLogsBaseService.Execute("employeeUpdate", LogTypeCode.WebApi, "", (log) =>
            {
                var oldDeptID = Guid.Empty;
                var oldJobId = Guid.Empty;
                if (string.IsNullOrEmpty(vm.EmpName))
                {
                    return new BaseResponse(ErrorCode.DatabaseError, "");
                }

                if (string.IsNullOrEmpty(vm.EmpWorkID))
                {
                    return new BaseResponse(ErrorCode.DatabaseError, "");
                }

                var entity = _employeeService.GetById(vm.Id).Entity;
                if (entity.EmpWorkID != vm.EmpWorkID)
                {
                    var isExt = _employeeService.GetAll().Any(c => c.EmpWorkID == vm.EmpWorkID);
                    if (isExt)
                    {
                        return new BaseResponse(ErrorCode.DatabaseError, "工号已存在");
                    }
                }
                oldDeptID = entity.EmpDeptID;
                oldJobId = entity.EmpJobID;
                entity.EmpName = vm.EmpName;
                entity.EmpUsedName = vm.EmpUsedName;
                entity.EmpSex = vm.EmpSex;
                entity.EmpBirthday = vm.EmpBirthday;
                entity.EmpDdentityNumber = vm.EmpDdentityNumber;
                entity.EmpWorkID = vm.EmpWorkID;
                entity.EmpOldWorkID = vm.EmpOldWorkID;
                entity.EmpEmail = vm.EmpEmail;
                entity.EmpTelePhone = vm.EmpTelePhone;
                entity.EmpOfficePhone = vm.EmpOfficePhone;
                entity.EmpFax = vm.EmpFax;
                entity.EmpCoID = vm.EmpCoID;
                entity.EmpDeptID = vm.EmpDeptID;
                entity.EmpJobID = vm.EmpJobID;
                entity.EmpJobNote = vm.EmpJobNote;
                entity.EmpBirthplace = vm.EmpBirthplace;
                entity.EmpNationality = vm.EmpNationality;
                entity.EmpPlaceOfBirth = vm.EmpPlaceOfBirth;
                entity.EmpAddress = vm.EmpAddress;
                entity.EmpPoliticalStatus = vm.EmpPoliticalStatus;
                entity.EmpType = vm.EmpType;
                entity.EmpJoinYearMonth = vm.EmpJoinYearMonth;
                entity.EmpPosition = vm.EmpPosition;
                entity.EmpIdentitydate = vm.EmpIdentitydate;
                entity.EmpEducation = vm.EmpEducation;
                entity.EmpDegree = vm.EmpDegree;
                entity.EmpBankCardNumber = vm.EmpBankCardNumber;
                entity.EmpProvidentFundAccount = vm.EmpProvidentFundAccount;
                entity.EmpMaritalStatus = vm.EmpMaritalStatus;
                entity.EmpZipCode = vm.EmpZipCode;
                entity.EmpMainAccountLocation = vm.EmpMainAccountLocation;
                entity.EmpJoinWorkTime = vm.EmpJoinWorkTime;
                entity.EmpJoinCompanyYear = vm.EmpJoinCompanyYear;
                entity.EmpHomePhone = vm.EmpHomePhone;
                entity.EmpLanguageLevel = vm.EmpLanguageLevel;
                entity.EmpSocialSecurityMonth = vm.EmpSocialSecurityMonth;
                entity.EmpNote = vm.EmpNote;
                entity.EmpSortNo = vm.EmpSortNo;
                entity.EmpState = vm.EmpState;

                var rel = _employeeService.Update(entity);

                if (!rel.IsSuccess)
                {
                    return rel.ErrorCode;
                }
                var oldObjid = string.Empty;
                if (oldDeptID != vm.EmpDeptID)
                {
                    oldObjid = oldDeptID.ToString();
                }
                if (oldJobId != vm.EmpJobID)
                {
                    oldObjid = oldJobId.ToString();
                }
                _employeeService.CreateExtendMapping(rel.Entity.Id, rel.Entity.EmpDeptID, 1,oldObjid);
                _employeeService.CreateExtendMapping(rel.Entity.Id, rel.Entity.EmpJobID, 2, oldObjid);
                return new BaseResponse();
            });
        }
        /// <summary>
        /// 移除
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [Route("remove")]
        [HttpPost]
        public BaseResponse RemoveEmployee(Human_EmployeeBaseVM vm)
        {
            if (vm == null)
            {
                return ErrorCode.Unauthorized;
            }

            if (vm.Id == Guid.Empty)
            {
                return ErrorCode.DatabaseError;
            }

            var rel = _employeeService.GetById(vm.Id);

            if (!rel.IsSuccess)
            {
                return rel.ErrorCode;
            }

            if (rel.Entity == null)
            {
                return ErrorCode.None;
            }

            rel = _employeeService.Delete(rel.Entity);

            return RelResponse(rel);
        }
        [Route("list")]
        [HttpPost]
        public EntityResponse<IEnumerable<Human_EmployeeBaseVM>> List(Human_EmployeeBaseFilterVM filter)
        {
            if (filter == null)
            {
                filter = new Human_EmployeeBaseFilterVM
                {
                    PageSize = 20,
                    PageIndex = 1
                };
            }
            if (filter.PageSize == 0)
            {
                filter.PageSize = 20;
            }

          

            var result = _employeeService.QueryPaging(filter);
         
            return new EntityResponse<IEnumerable<Human_EmployeeBaseVM>>(ErrorCode.None, string.Empty)
            {
                Entity = result.Item1,
                Pagination = new ResponsePagination()
                {
                    PageSize = filter.PageSize,
                    PageIndex = filter.PageIndex,
                    TotalRecords = result.Item2,
                    PageCount = (int)Math.Ceiling((double)result.Item2 / filter.PageSize)
                }
            };
        }

        [Route("empExtendMapping"), HttpPost]
        public BaseResponse empExtendMapping(EmpMappingParam param)
        {
            return SystemLogsBaseService.Execute("用户部门/岗位/角色关联", LogTypeCode.WebApi, param, (log) =>
            {
                log.objectId = param.EmpId;
                log.AppendLine(string.Format("关联:{0}",param.ExtType));
                var result = _employeeService.EmpExtendMapping(param);
                return new BaseResponse();
            });
             
        }
        /// <summary>
        /// 获取部门、岗位、
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("getExtendEmpMapping/{objectId}/{type}"), HttpGet]
        public IEnumerable<Guid> getExtendEmpMapping(Guid objectId, int type)
        {
            return _employeeService.GetExtendEmpMapping(objectId, type);
        }
        [Route("info/{id}"), HttpGet]
        public EntityResponse<Human_EmployeeBaseVM> Info(Guid id)
        {

            var rel = _employeeService.GetEmpInfo(id);

            return rel;
        }
    }
}