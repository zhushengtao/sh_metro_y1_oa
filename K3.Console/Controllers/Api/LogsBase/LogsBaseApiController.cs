﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GeneralFramework.BLL;
using GeneralFramework.Common;
using GeneralFramework.ViewModel;
using System.Web.Http;
using GeneralFramework.Model;
namespace GeneralFramework.Console.Controllers.Api
{
    [RoutePrefix("api/logs")]
    public class LogsBaseApiController : ApiController
    {
        private readonly SystemLogsBaseService _logService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="attachService"></param>
        public LogsBaseApiController(SystemLogsBaseService logService)
        {
            _logService = logService;
        }
        /// <summary>
        /// 查询日志接口-目前只是简单的实现查询 后期扩展
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [Route("query"),HttpPost]
        public EntityResponse<IEnumerable<LogsVM>> QueryLogs(LogsQueryParam query)
        {
            var logs = new List<LogsVM>();
            var pagination = new ResponsePagination();
            try
            {
                if (query == null)
                {
                    query = new LogsQueryParam();
                }
                var total = 0;
                var result = SystemLogsBaseService.QueryLogs(query);
                if (result.Entity != null)
                {
                    return result;
                }
                pagination = new ResponsePagination()
                {
                    TotalRecords = total,
                    PageIndex = query.PageIndex,
                    PageSize = query.PageSize,
                    PageCount = (int)Math.Ceiling((decimal)total / query.PageSize)
                };

            }
            catch (Exception ex)
            {
              
            }
            return new EntityResponse<IEnumerable<LogsVM>>(logs, pagination);
        }
        [Route("details/{id}"),HttpGet]
        public EntityResponse<LogsBase> details(Guid id)
        {
            return _logService.GetById(id);
        }
    }
}