﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GeneralFramework.BLL;
using GeneralFramework.Common;
using GeneralFramework.ViewModel;
using System.Web.Http;
using GeneralFramework.Model;
using System.Net;
using GeneralFramework.Security;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Net.Http;
using GeneralFramework.Infrastructure.Utilities;

namespace GeneralFramework.Console.Controllers.Api
{
    [RoutePrefix("api/systemuser")]
    public class System_UserApiController : BaseApiController
    {
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Authentication;
            }
        }
        private readonly SystemUserService _systemUserService;
        private readonly Human_EmployeeBaseService _human_employeeService;
        public System_UserApiController(SystemUserService systemUserService,Human_EmployeeBaseService human_employeeService) {

            _systemUserService = systemUserService;
            _human_employeeService = human_employeeService;
        }
        /// <summary>
        /// 创建插入demo
        /// </summary>
        /// <param name="param">后台需要的对象-前端封装</param>
        /// <returns></returns>
        [Route("add"), HttpPost]
        public BaseResponse add(SystemUserParam vm)
        {
            return SystemLogsBaseService.Execute("userAdd", LogTypeCode.WebApi, "", (log) =>
            {
                var h = new CryptoHelper();
                if (string.IsNullOrEmpty(vm.Username))
                {
                    return new BaseResponse(ErrorCode.DatabaseError,"");
                }

                if (string.IsNullOrEmpty(vm.Password))
                {
                    return new BaseResponse(ErrorCode.DatabaseError, "");
                }

                if (_systemUserService.GetByUsername(vm.Username).Entity != null)
                {
                    return new BaseResponse(ErrorCode.DatabaseError, "");
                }
                if (string.IsNullOrEmpty(vm.UserWorkNumber))
                {
                    return new BaseResponse(ErrorCode.DatabaseError, "工号为空");
                }
                var employee = _human_employeeService.GetAll().FirstOrDefault(c=>c.EmpWorkID==vm.UserWorkNumber);
                if (employee==null)
                {
                    return new BaseResponse(ErrorCode.DatabaseError, "工号错误");
                }
                var user = _systemUserService.GetAll().Any(c => c.UserWorkNumber == vm.UserWorkNumber&&!c.IsLockedOut&&!c.IsDeleted);
                if (user)
                {
                    return new BaseResponse(ErrorCode.DatabaseError, "该工号下已存在账号");
                }
                var entity = new SystemUser();
                entity.Username = vm.Username;
                entity.Password = vm.Password;
                entity.PersonalEmail = vm.PersonalEmail;
                entity.FullName = vm.FullName;
                entity.MobilePhone = vm.MobilePhone;
                entity.NickName = vm.NickName;
                entity.IsLockedOut = vm.IsLockedOut;
                entity.IsApproved = true;
                entity.UserEmpID = employee.Id;
                entity.UserWorkNumber = employee.EmpWorkID;
                entity.PasswordHash = CryptoHelper.EncryptByBase64(vm.Password);
                var rel = _systemUserService.Create(entity);

                if (!rel.IsSuccess)
                {
                    return rel.ErrorCode;
                }
                return new BaseResponse();
            });
        }
        /// <summary>
        /// 创建插入demo
        /// </summary>
        /// <param name="param">后台需要的对象-前端封装</param>
        /// <returns></returns>
        [Route("update"), HttpPost]
        public BaseResponse update(SystemUserParam vm)
        {
            return SystemLogsBaseService.Execute("userUpdate", LogTypeCode.WebApi, "", (log) =>
            {
                var h = new CryptoHelper();
                if (string.IsNullOrEmpty(vm.Username))
                {
                    return new BaseResponse(ErrorCode.DatabaseError, "账号为空");
                }

                if (string.IsNullOrEmpty(vm.Password))
                {
                    return new BaseResponse(ErrorCode.DatabaseError, "密码为空");
                }
                var entity = _systemUserService.GetById(vm.Id).Entity;
                if (entity.Username != vm.Username) { 
                    if (_systemUserService.GetByUsername(vm.Username).Entity != null)
                    {
                        return new BaseResponse(ErrorCode.DatabaseError, "用户名存在");
                    }
                }
                if (string.IsNullOrEmpty(vm.UserWorkNumber))
                {
                    return new BaseResponse(ErrorCode.DatabaseError, "工号为空");
                }
                var employee = _human_employeeService.GetAll().FirstOrDefault(c => c.EmpWorkID == vm.UserWorkNumber);
                if (employee == null)
                {
                    return new BaseResponse(ErrorCode.DatabaseError, "工号错误");
                }
                if (entity.UserWorkNumber != vm.UserWorkNumber)
                {
                    var user = _systemUserService.GetAll().Any(c => c.UserWorkNumber == vm.UserWorkNumber && !c.IsLockedOut && !c.IsDeleted);
                    if (user)
                    {
                        return new BaseResponse(ErrorCode.DatabaseError, "该工号下已存在账号");
                    }
                }
                entity.Username = vm.Username;
                entity.Password = vm.Password;
                entity.PersonalEmail = vm.PersonalEmail;
                entity.FullName = vm.FullName;
                entity.MobilePhone = vm.MobilePhone;
                entity.NickName = vm.NickName;
                entity.IsLockedOut = vm.IsLockedOut;
                entity.IsApproved = true;
                entity.UserEmpID = employee.Id;
                entity.UserWorkNumber = employee.EmpWorkID;
                entity.PasswordHash = CryptoHelper.EncryptByBase64(vm.Password);
                var rel = _systemUserService.Update(entity);

                if (!rel.IsSuccess)
                {
                    return rel.ErrorCode;
                }
                return new BaseResponse();
            });
        }
        /// <summary>
        /// 移除用户
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [Route("remove")]
        [HttpPost]
        public BaseResponse RemoveUser(SystemUserParam vm)
        {
            if (vm == null)
            {
                return ErrorCode.Unauthorized;
            }

            if (vm.Id== Guid.Empty)
            {
                return ErrorCode.DatabaseError;
            }

            var rel = _systemUserService.GetById(vm.Id);

            if (!rel.IsSuccess)
            {
                return rel.ErrorCode;
            }

            if (rel.Entity == null)
            {
                return ErrorCode.None;
            }

            rel = _systemUserService.Delete(rel.Entity);

            return RelResponse(rel);
        }
        /// <summary>
        /// 查询demo
        /// </summary>
        /// <returns></returns>
        [Route("list"), HttpPost]
        public EntityResponse<IEnumerable<SystemUserVM>> List(SystemUserFilterVM filter)
        {
            if (filter == null)
            {
                filter = new SystemUserFilterVM();
            }
            var listEntits = _systemUserService.GetAll();
            if (!string.IsNullOrEmpty(filter.Keywords))
            {
                listEntits = listEntits.Where(c => c.Username == filter.Keywords);
            }
            if (filter.JobId.HasValue)
            {
                listEntits = listEntits.Where(c=>c.UserEmp.EmpJobID==filter.JobId);
            }
            if (filter.DepId.HasValue)
            {
                listEntits = listEntits.Where(c=>c.UserEmp.EmpDeptID==filter.DepId);
            }
           
            var totalRecords = listEntits.Count();
            var pageCount = Math.Ceiling((decimal)totalRecords / filter.PageSize);
            var pagination = new ResponsePagination
            {
                PageCount = (int)pageCount,
                PageIndex = filter.PageIndex,
                PageSize = filter.PageSize,
                TotalRecords = totalRecords
            };

            var list = listEntits
                        .OrderBy(c => c.LineNum)
                        .Skip(filter.PageSize * (filter.PageIndex - 1))
                        .Take(filter.PageSize).ToArray()
                        .Select(f => new SystemUserVM(f))
                        .ToArray();

            return EntityResponse<IEnumerable<SystemUserVM>>.Success(list, pagination);
        }
        /// <summary>
        /// update password
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [Route("resetpwd"), HttpPost]
        public BaseResponse UpdatePassword(PasswordParam vm)
        {
            if (string.IsNullOrEmpty(vm.Password))
            {
                return ErrorCode.DatabaseError;
            }

            if (string.IsNullOrEmpty(vm.NewPassword))
            {
                return ErrorCode.DatabaseError;
            }
            var user = _systemUserService.GetById(CurrentUser.Id).Entity;

            if (_systemUserService.CheckPassword(user, vm.Password))
            {
                var rel = _systemUserService.UpdatePassword(user, vm.NewPassword);
                if (rel.IsSuccess)
                {
                    CurrentUser = new SystemUserVM(_systemUserService.GetById(user.Id).Entity);
                }

                return rel;
            }

            return ErrorCode.DatabaseError;
        }
        [Route("login"), HttpPost, AllowAnonymous]
        public EntityResponse<SystemUserVM> Login(LoginParam vm)
        {
            var h = new CryptoHelper();
            if (string.IsNullOrEmpty(vm.Username))
            {
                return EntityResponse<SystemUserVM>.Error(ErrorCode.DatabaseError, "");
            }

            if (string.IsNullOrEmpty(vm.Password))
            {
                return EntityResponse<SystemUserVM>.Error(ErrorCode.DatabaseError, "");
            }

            var rel = _systemUserService.Validate(vm.Username, vm.Password);

            if (rel.Entity == null)
            {
                return rel.ErrorCode;
            }
            if (!rel.Entity.IsApproved)
            {
                return EntityResponse<SystemUserVM>.Error(ErrorCode.DatabaseError, "");
            }
            if (rel.Entity.IsLockedOut)
            {

                return EntityResponse<SystemUserVM>.Error(ErrorCode.DatabaseError, "");
            }
            SignIn(rel.Entity);

            SystemLogsBaseService.Instance.AddTraceInfo(string.Format("User 【{0}】login IP【{1}】", rel.Entity.Id, GetClientIp(Request)));

            return new SystemUserVM(rel);

        }
        private void SignIn(SystemUser user)
        {
            try
            {
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                var identity = GeneralIdentity.Create(user, DefaultAuthenticationTypes.ApplicationCookie);
                //SystemUserUnitWork.GetUserCatalogs(user.Id, true);
                //SystemUserUnitWork.GetUserContracts(user.Id, true);
                AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = true }, identity);
                CurrentUser = new SystemUserVM(user);
            }
            catch (Exception err)
            {

            }
        }
        /// <summary>
        /// Get Current user info.
        /// </summary>
        /// <returns></returns>
        [Route("info"), HttpGet]
        public EntityResponse<SystemUserVM> CurrentUserInfo()
        {
            if (CurrentUser != null)
            {
                var _cookies = this.Request.Headers.GetCookies(string.Format(".General_AUTH_{0}", "2020.05.20.001".Replace(".", "_")));
                CurrentUser.ClientCookieValue = _cookies[0].Cookies[1].Value;

                return CurrentUser;
            }

            return ErrorCode.Unauthorized;
        }
        [Route("{idOrUsername}")]
        [HttpGet]
        public EntityResponse<SystemUserVM> UserDetail(string idOrUsername)
        {
            if (string.IsNullOrEmpty(idOrUsername))
            {
                return ErrorCode.None;
            }
            Guid id = Guid.Empty;
            EntityResponse<SystemUser> rel = null;
            if (Guid.TryParse(idOrUsername, out id))
            {
                rel = _systemUserService.GetById(id);
            }
            else
            {
                rel = _systemUserService.GetByUsername(idOrUsername);
            };

            if (!rel.IsSuccess)
            {
                return rel.ErrorCode;
            }

            if (rel.Entity == null)
            {
                return ErrorCode.DatabaseError;
            }

            return new SystemUserVM(rel);
        }
        [Route("logout"), HttpPost, HttpGet, AllowAnonymous]
        public BaseResponse LogOut()
        {
            var entity = _systemUserService.GetById(CurrentUser.Id).Entity;
            AuthenticationManager.SignOut();
            CurrentUser = null;
            SessionHelper.Abandon();
            return BaseResponse.Success();
        }

        [Route("createAdmin/{token}"), HttpGet]
        public BaseResponse createAdminUser(string token)
        {
            var h = new CryptoHelper();
            if (token != "18516291685")
            {
                return new BaseResponse(ErrorCode.DatabaseError, "token失败");
            }
            var isExt = _systemUserService.GetByUsername("admin");
            if (isExt.Entity != null)
            {
                return new BaseResponse(ErrorCode.DatabaseError, "账号已存在");
            }
            var paramUser = new SystemUser();
            paramUser.Username = "admin";
            paramUser.Password = "china.1";
            paramUser.PasswordHash = CryptoHelper.EncryptByBase64("china.1");
            paramUser.IsApproved = true;
            return _systemUserService.Create(paramUser);
        }

    }
}