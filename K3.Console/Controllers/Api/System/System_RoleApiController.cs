﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GeneralFramework.BLL;
using GeneralFramework.Common;
using GeneralFramework.ViewModel;
using System.Web.Http;
using GeneralFramework.Model;
using System.Net;
using GeneralFramework.Security;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Net.Http;
using GeneralFramework.Infrastructure.Utilities;

namespace GeneralFramework.Console.Controllers.Api.System
{
    public class System_RoleApiController : BaseApiController
    {
        /// <summary>
        /// 角色
        /// </summary>
        [RoutePrefix("api/role")]
        public class RoleApiController : BaseApiController
        {
            private readonly System_RoleBaseService _roleService;
            public RoleApiController(System_RoleBaseService roleService)
            {
                _roleService = roleService;
             
            }
            [Route("add")]
            [HttpPost]
            public BaseResponse Add(RoleParam vm)
            {
                var entity = new System_RolesBase
                {
                    RoleName = vm.RoleName.Trim(),
                    Organization = vm.Organization,
                    LoweredRoleName = vm.RoleName.ToLower(),
                    LineNum = vm.LineNum,
                    Remarks = vm.Remarks,
                    IsLockedOut = vm.IsLockout,
                    IsProtected = vm.IsProtected
                };
                var rel = _roleService.Create(entity);

                if (!rel.IsSuccess)
                {
                    return rel.ErrorCode;
                }

                return rel;
            }

            /// <summary>
            /// update function category
            /// </summary>
            /// <param name="vm">The param entity FunctionCategoryParam</param>
            /// <returns>Role entity</returns>
            [Route("update")]
            [HttpPost]
            public BaseResponse Update(RoleParam vm)
            {
               
                var id = vm.Id;
                if (_roleService.IsExists(vm.RoleName, id))
                {
                    return ErrorCode.DatabaseError;
                }

                var entity = _roleService.GetById(id).Entity;

                if (entity == null)
                {
                    return ErrorCode.DatabaseError;
                }

                entity.RoleName = vm.RoleName.Trim();
                entity.Organization = vm.Organization;
                entity.LoweredRoleName = vm.RoleName.ToLower();
                entity.LineNum = vm.LineNum;
                entity.Remarks = vm.Remarks;
                entity.IsLockedOut = vm.IsLockout;
                entity.IsProtected = vm.IsProtected;
                var rel = _roleService.Update(entity);

                if (!rel.IsSuccess)
                {
                    return EntityResponse<System_RoleBaseVM>.Error(rel.ErrorCode);
                }

                return rel;
            }

            /// <summary>
            /// 获取角色详情
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            [Route("detail/{id}")]
            [HttpGet]
            public EntityResponse<System_RoleBaseVM> Detail(Guid id)
            {
                var rel = _roleService.GetById(id);

                if (!rel.IsSuccess)
                {
                    return rel.ErrorCode;
                }

                if (rel.Entity == null)
                {
                    return ErrorCode.DatabaseError;
                }
                return new System_RoleBaseVM(rel);
            }

            /// <summary>
            /// 移除角色
            /// </summary>
            /// <param name="vm"></param>
            /// <returns></returns>
            [Route("delete")]
            [HttpPost]
            public BaseResponse Delete(RoleParam vm)
            {
             
                var rel = _roleService.GetById(vm.Id);

                if (!rel.IsSuccess || rel.Entity == null)
                {
                    return ErrorCode.DatabaseError;
                }

                rel = _roleService.Delete(rel.Entity);

                return RelResponse(rel);
            }

            /// <summary>
            /// 查询角色信息
            /// </summary>
            /// <param name="filter"></param>
            /// <returns></returns>
            [Route("list")]
            [HttpPost]
            public EntityResponse<IEnumerable<System_RoleBaseVM>> RoleList(RoleFilter filter)
            {
                if (filter == null)
                {
                    filter = new RoleFilter();
                }

                var totalRecords = _roleService.GetAll().Count(f => f.RoleName.Contains(filter.Keywords));
                var pageCount = Math.Ceiling((decimal)totalRecords / filter.PageSize);
                var pagination = new ResponsePagination
                {
                    PageCount = (int)pageCount,
                    PageIndex = filter.PageIndex,
                    PageSize = filter.PageSize,
                    TotalRecords = totalRecords
                };

                var list = _roleService
                            .GetAll()
                            .Where(f => f.RoleName.Contains(filter.Keywords))
                            .OrderBy(c => c.RoleName)
                            .ThenBy(c => c.CreatedOn)
                            .Skip(filter.PageSize * (filter.PageIndex - 1))
                            .Take(filter.PageSize).ToArray()
                            .Select(f => new System_RoleBaseVM(f))
                            .ToArray();

                return EntityResponse<IEnumerable<System_RoleBaseVM>>.Success(list, pagination);
            }


         
        }
    }
}