﻿using GeneralFramework.BLL;
using GeneralFramework.Common;
using GeneralFramework.Model;
using GeneralFramework.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace GeneralFramework.Console.Controllers.Api.System
{
    [RoutePrefix("api/systemJob")]
    public class System_JobBaseApiController : BaseApiController
    {
        private readonly System_JobBaseService _system_JobBaseService;

        public System_JobBaseApiController(System_JobBaseService system_JobBaseService)
        {
            _system_JobBaseService = system_JobBaseService;
        } /// <summary>
          /// 增加
          /// </summary>
          /// <param name="vm">The param entity ..</param>
        [Route("add")]
        [HttpPost]
        public BaseResponse add(System_JobBaseParam param)
        {
            var entity = new System_JobBase();
            entity.JobName = param.JobName;
            entity.JobNumber = param.JobNumber;
            entity.JobParentID = param.JobParentID;
            entity.JobDeptID = param.JobDeptID;
            entity.JobCoID = param.JobCoID;
            entity.JobLevel = param.JobLevel;
            entity.JobType = param.JobType;
            entity.JobNote = param.JobNote;
            entity.JobTreeCode = param.JobTreeCode;
            entity.JobSortNo = param.JobSortNo;
            entity.JobState = param.JobState;
            entity.JobtIsShow = param.JobtIsShow;
            entity.JobClientIP = param.JobClientIP;


            if (param.JobParentID.HasValue)
            {
                var parent = _system_JobBaseService.GetById(param.JobParentID.Value);
                entity.JobParent = parent.Entity;
            }
            var rel = _system_JobBaseService.Create(entity);

            if (!rel.IsSuccess)
            {
                return BaseResponse.Error(ErrorCode.DatabaseError, "失败");
            }

            return new BaseResponse();
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="vm">The param entity ..</param>
        [Route("update")]
        [HttpPost]
        public BaseResponse update(System_JobBaseParam param)
        {
            var entity = _system_JobBaseService.GetById(param.Id).Entity;
            if (entity == null)
            {
                return BaseResponse.Error(ErrorCode.Existed, "数据不存在");
            }
            if (_system_JobBaseService.IsExists(param.JobName, param.Id, param.JobParentID))
            {
                return BaseResponse.Error(ErrorCode.DatabaseError, "");
            }

            if (param.Id == param.JobParentID)
            {
                return ErrorCode.DatabaseError;
            }
            entity.JobName = param.JobName;
            entity.JobNumber = param.JobNumber;
            entity.JobParentID = param.JobParentID;
            entity.JobDeptID = param.JobDeptID;
            entity.JobCoID = param.JobCoID;
            entity.JobLevel = param.JobLevel;
            entity.JobType = param.JobType;
            entity.JobNote = param.JobNote;
            entity.JobTreeCode = param.JobTreeCode;
            entity.JobSortNo = param.JobSortNo;
            entity.JobState = param.JobState;
            entity.JobtIsShow = param.JobtIsShow;
            entity.JobClientIP = param.JobClientIP;

            if (param.JobParentID.HasValue)
            {
                var parent = _system_JobBaseService.GetById(param.JobParentID.Value);
                entity.JobParent = parent.Entity;
            }
            var rel = _system_JobBaseService.Update(entity);

            if (!rel.IsSuccess)
            {
                return BaseResponse.Error(ErrorCode.DatabaseError, "失败");
            }

            return new BaseResponse();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [Route("list")]
        [HttpPost]
        public EntityResponse<IEnumerable<System_JobBaseVM>> list(System_JobFilter filter)
        {
            if (filter == null)
            {
                filter = new System_JobFilter();
            }

            var totalRecords = _system_JobBaseService.GetAll().Count(f => f.JobName.Contains(filter.Keywords));
            var pageCount = Math.Ceiling((decimal)totalRecords / filter.PageSize);
            var pagination = new ResponsePagination
            {
                PageCount = (int)pageCount,
                PageIndex = filter.PageIndex,
                PageSize = filter.PageSize,
                TotalRecords = totalRecords
            };

            var list = _system_JobBaseService
                        .GetAll()
                        .Where(f => f.JobName.Contains(filter.Keywords))
                        .OrderBy(c => c.LineNum)
                        .Skip(filter.PageSize * (filter.PageIndex - 1))
                        .Take(filter.PageSize).ToArray()
                        .Select(f => new System_JobBaseVM(f, withChildren: false))
                        .ToArray();

            return EntityResponse<IEnumerable<System_JobBaseVM>>.Success(list, pagination);
        }


        /// <summary>
        /// 所有节点
        /// </summary>
        /// <returns></returns>
        [Route("jobTree")]
        [HttpGet]
        public IEnumerable<JsTreeVM> CatalogTree()
        {
            var list = _system_JobBaseService.GetByParentId(null);

            JsTreeVM[] _list = new JsTreeVM[list.Entity.Count];
            var i = 0;
            foreach (System_JobBase _item in list.Entity)
            {
                _list[i] = GetChildren(_item);
                i++;
            }
            return _list;
        }
        private JsTreeVM GetChildren(System_JobBase job)
        {
            var jsTreeVm = new JsTreeVM(job.Id.ToString(), job.JobName, null, "root", "","");
            if (job.Children.Any(c => !c.IsDeleted))
            {
                jsTreeVm.children = new List<JsTreeVM>();
                var children = new List<JsTreeVM>();
                foreach (var child in job.Children.Where(c => !c.IsDeleted))
                {
                    children.Add(GetChildren(child));
                }
                jsTreeVm.children = children;
            }

            return jsTreeVm;
        }
        /// <summary>
        /// GetInternalNumber
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        private string BuildJobInternalNumber(Guid? parentId)
        {
            string billnumber = "";

            if (parentId.HasValue)
            {
                var NumberbyParent = "";

                var ChildrenCount = 0;
                NumberbyParent = _system_JobBaseService.GetByJobId(parentId).Entity.JobSortNo;

                ChildrenCount = _system_JobBaseService.GetByParentId(parentId).Entity.Count;

                billnumber = NumberbyParent + "000" + (ChildrenCount + 1);
            }
            else
            {
                int counter = 0;

                List<System_JobBase> _items = _system_JobBaseService.GetAll().Where(m => !parentId.HasValue && !m.IsDeleted).ToList<System_JobBase>();

                if (_items != null)
                {
                    counter = _items.Count;
                }
                counter++;

                billnumber = "000" + counter;
            }

            return billnumber;
        }

        [Route("info/{id}"),HttpGet]
        public EntityResponse<System_JobBaseVM> Info(Guid id)
        {

            var rel = _system_JobBaseService.GetJobInfo(id);

            return rel;
        }
        [Route("delete")]
        [HttpPost]
        public BaseResponse Delete(System_JobBaseParam vm)
        {
            if (vm.Id == Guid.Empty)
            {
                return ErrorCode.DatabaseError;
            }

            var rel = _system_JobBaseService.GetById(vm.Id);
            if (rel.Entity == null)
            {
                return ErrorCode.DatabaseError;
            }

            if (rel.Entity.Children.Count() > 0)
            {
                return ErrorCode.DatabaseError;
            }


            rel = _system_JobBaseService.Delete(rel.Entity);

            return rel;
        }
  
    }
}