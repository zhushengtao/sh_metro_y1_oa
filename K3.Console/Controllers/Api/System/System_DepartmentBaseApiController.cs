﻿using GeneralFramework.BLL;
using GeneralFramework.Common;
using GeneralFramework.Model;
using GeneralFramework.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace GeneralFramework.Console.Controllers.Api
{
    [RoutePrefix("api/systemDept")]
    public class System_DepartmentBaseApiController : BaseApiController
    {
        private readonly System_DepartmentBaseServicec _system_DepartmentBaseServicecs;

        public System_DepartmentBaseApiController(System_DepartmentBaseServicec system_DepartmentBaseServicecs)
        {
            _system_DepartmentBaseServicecs = system_DepartmentBaseServicecs;
        } /// <summary>
          /// 增加
          /// </summary>
          /// <param name="vm">The param entity ..</param>
        [Route("add")]
        [HttpPost]
        public BaseResponse add(System_DepartmentBaseParam param)
        {
            var entity = new System_DepartmentBase();
            entity.DeptName = param.DeptName;
            entity.DeptNumber = param.DeptNumber;
            entity.DeptParentID = param.DeptParentID;
            entity.DeptGroupStID = param.DeptGroupStID;
            entity.DeptGroupStName = param.DeptGroupStName;
            entity.DeptOperManID = param.DeptOperManID;
            entity.DeptOperManName = param.DeptOperManName;
            entity.DeptEmail = param.DeptEmail;
            entity.DeptTelePhone = param.DeptTelePhone;
            entity.DeptFax = param.DeptFax;
            entity.DeptCoID = param.DeptCoID;
            entity.DeptType = param.DeptType;
            entity.DeptNote = param.DeptNote;
            entity.DeptTreeCode = param.DeptTreeCode;
            entity.DeptSortNo = BuildCatalogInternalNumber(param.DeptParentID);
            entity.DeptState = param.DeptState;
            entity.DeptIsShow = param.DeptIsShow;
            entity.DeptClientIP = param.DeptClientIP;

            if (param.DeptParentID.HasValue)
            {
                var parent = _system_DepartmentBaseServicecs.GetById(param.DeptParentID.Value);
                entity.DeptParent = parent.Entity;
            }
            var rel = _system_DepartmentBaseServicecs.Create(entity);

            if (!rel.IsSuccess)
            {
                return EntityResponse<System_MenuBaseVM>.Error(rel.ErrorCode, rel.ErrorMessage);
            }

            return new BaseResponse();
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="vm">The param entity ..</param>
        [Route("update")]
        [HttpPost]
        public BaseResponse update(System_DepartmentBaseParam param)
        {
            var entity = _system_DepartmentBaseServicecs.GetById(param.Id).Entity;
            if (entity == null)
            {
                return BaseResponse.Error(ErrorCode.Existed, "数据不存在");
            }
            if (_system_DepartmentBaseServicecs.IsExists(param.DeptName, param.Id, param.DeptParentID))
            {
                return BaseResponse.Error(ErrorCode.DatabaseError, "");
            }

            if (param.Id == param.DeptParentID)
            {
                return ErrorCode.DatabaseError;
            }
            entity.DeptName = param.DeptName;
            entity.DeptNumber = param.DeptNumber;
            entity.DeptParentID = param.DeptParentID;
            entity.DeptGroupStID = param.DeptGroupStID;
            entity.DeptGroupStName = param.DeptGroupStName;
            entity.DeptOperManID = param.DeptOperManID;
            entity.DeptOperManName = param.DeptOperManName;
            entity.DeptEmail = param.DeptEmail;
            entity.DeptTelePhone = param.DeptTelePhone;
            entity.DeptFax = param.DeptFax;
            entity.DeptCoID = param.DeptCoID;
            entity.DeptType = param.DeptType;
            entity.DeptNote = param.DeptNote;
            entity.DeptTreeCode = param.DeptTreeCode;
            entity.DeptSortNo = param.DeptSortNo;
            entity.DeptState = param.DeptState;
            entity.DeptIsShow = param.DeptIsShow;
            entity.DeptClientIP = param.DeptClientIP;

            if (param.DeptParentID.HasValue)
            {
                var parent = _system_DepartmentBaseServicecs.GetById(param.DeptParentID.Value);
                entity.DeptParent = parent.Entity;
            }
            var rel = _system_DepartmentBaseServicecs.Update(entity);

            if (!rel.IsSuccess)
            {
                return EntityResponse<System_MenuBaseVM>.Error(rel.ErrorCode, rel.ErrorMessage);
            }

            return new BaseResponse();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [Route("list")]
        [HttpPost]
        public EntityResponse<IEnumerable<System_DepartmentBaseVM>> list(System_DepartmentBaseFilter filter)
        {
            if (filter == null)
            {
                filter = new System_DepartmentBaseFilter();
            }

            var totalRecords = _system_DepartmentBaseServicecs.GetAll().Count(f => f.DeptName.Contains(filter.Keywords));
            var pageCount = Math.Ceiling((decimal)totalRecords / filter.PageSize);
            var pagination = new ResponsePagination
            {
                PageCount = (int)pageCount,
                PageIndex = filter.PageIndex,
                PageSize = filter.PageSize,
                TotalRecords = totalRecords
            };

            var list = _system_DepartmentBaseServicecs
                        .GetAll()
                        .Where(f => f.DeptName.Contains(filter.Keywords))
                        .OrderBy(c => c.LineNum)
                        .Skip(filter.PageSize * (filter.PageIndex - 1))
                        .Take(filter.PageSize).ToArray()
                        .Select(f => new System_DepartmentBaseVM(f, withChildren: false))
                        .ToArray();

            return EntityResponse<IEnumerable<System_DepartmentBaseVM>>.Success(list, pagination);
        }
        [Route("delete")]
        [HttpPost]
        public BaseResponse Delete(System_DepartmentBaseParam vm)
        {
            if (vm.Id == Guid.Empty)
            {
                return ErrorCode.DatabaseError;
            }

            var rel = _system_DepartmentBaseServicecs.GetById(vm.Id);
            if (rel.Entity == null)
            {
                return ErrorCode.DatabaseError;
            }

            if (rel.Entity.Children.Count() > 0)
            {
                return ErrorCode.DatabaseError;
            }


            rel = _system_DepartmentBaseServicecs.Delete(rel.Entity);

            return rel;
        }


        /// <summary>
        /// 所有节点
        /// </summary>
        /// <returns></returns>
        [Route("deptTree")]
        [HttpGet]
        public IEnumerable<JsTreeVM> CatalogTree()
        {
            var list = _system_DepartmentBaseServicecs.GetByParentId(null);

            JsTreeVM[] _list = new JsTreeVM[list.Entity.Count];
            var i = 0;
            foreach (System_DepartmentBase _item in list.Entity)
            {
                _list[i] = GetChildren(_item);
                i++;
            }
            return _list;
        }
        private JsTreeVM GetChildren(System_DepartmentBase catalog)
        {
            var jsTreeVm = new JsTreeVM(catalog.Id.ToString(), catalog.DeptName, null, "root", "","");
            if (catalog.Children.Any(c => !c.IsDeleted))
            {
                jsTreeVm.children = new List<JsTreeVM>();
                var children = new List<JsTreeVM>();
                foreach (var child in catalog.Children.Where(c => !c.IsDeleted))
                {
                    children.Add(GetChildren(child));
                }
                jsTreeVm.children = children;
            }

            return jsTreeVm;
        }
        /// <summary>
        /// GetInternalNumber
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        private string BuildCatalogInternalNumber(Guid? parentId)
        {
            string billnumber = "";

            if (parentId.HasValue)
            {
                var NumberbyParent = "";

                var ChildrenCount = 0;
                NumberbyParent = _system_DepartmentBaseServicecs.GetByDepartmentId(parentId).Entity.DeptSortNo;

                ChildrenCount = _system_DepartmentBaseServicecs.GetByParentId(parentId).Entity.Count;

                billnumber = NumberbyParent + "000" + (ChildrenCount + 1);
            }
            else
            {
                int counter = 0;

                List<System_DepartmentBase> _items = _system_DepartmentBaseServicecs.GetAll().Where(m => !parentId.HasValue && !m.IsDeleted).ToList<System_DepartmentBase>();

                if (_items != null)
                {
                    counter = _items.Count;
                }
                counter++;

                billnumber = "000" + counter;
            }

            return billnumber;
        }
        [Route("info/{id}"), HttpGet]
        public EntityResponse<System_DepartmentBase> Info(Guid id)
        {

            var rel = _system_DepartmentBaseServicecs.GetById(id);

            return rel;
        }
    }
}