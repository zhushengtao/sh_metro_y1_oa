﻿using GeneralFramework.BLL;
using GeneralFramework.Common;
using GeneralFramework.Model;
using GeneralFramework.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace GeneralFramework.Console.Controllers.Api
{
    [RoutePrefix("api/systemMenu")]

    public class System_MenuBaseApiController : BaseApiController
    {
        private readonly System_MenuBaseService _system_MenuBaseService;

        public System_MenuBaseApiController(System_MenuBaseService system_MenuBaseService)
        {
            _system_MenuBaseService = system_MenuBaseService;
        }
        /// <summary>
        /// 增加一个目录
        /// </summary>
        /// <param name="vm">The param entity ..</param>
        /// <returns>Role entity</returns>
        [Route("add")]
        [HttpPost]
        public BaseResponse add(System_MenuBaseParam param)
        {
            var entity = new System_MenuBase();
            entity.MenuClientIP = string.Empty;
            entity.MenuDescription = param.MenuDescription;
            entity.MenuIcoUrl = param.MenuIcoUrl;
            entity.MenuIsShow = param.MenuIsShow;
            entity.MenuName = param.MenuName;
            entity.MenuNote = param.MenuNote;
            entity.MenuNumber = param.MenuNumber;
            entity.MenuParentID = param.MenuParentID;
            entity.MenuSortNo = param.MenuSortNo;
            entity.MenuState = param.MenuState;
            entity.MenuUrl = param.MenuUrl;

            if (param.MenuParentID.HasValue)
            {
                var parent = _system_MenuBaseService.GetById(param.MenuParentID.Value);
                entity.MenuParent = parent.Entity;
                entity.Layer = 2;
            }
            else
            {
                entity.Layer = 1;
            }
            var rel = _system_MenuBaseService.Create(entity);

            if (!rel.IsSuccess)
            {
                return EntityResponse<System_MenuBaseVM>.Error(rel.ErrorCode, rel.ErrorMessage);
            }

            return new BaseResponse();
        }
        /// <summary>
        /// 更新一个目录
        /// </summary>
        /// <param name="vm">The param entity ..</param>
        /// <returns>Role entity</returns>
        [Route("update")]
        [HttpPost]
        public BaseResponse update(System_MenuBaseParam param)
        {
            var entity = _system_MenuBaseService.GetById(param.Id).Entity;
            if (entity == null)
            {
                return BaseResponse.Error(ErrorCode.Existed,"数据不存在");
            }
            if (_system_MenuBaseService.IsExists(param.MenuName, param.Id, param.MenuParentID))
            {
                return BaseResponse.Error(ErrorCode.DatabaseError,"");
            }

            if (param.Id == param.MenuParentID)
            {
                return ErrorCode.DatabaseError;
            }
            entity.MenuClientIP = string.Empty;
            entity.MenuDescription = param.MenuDescription;
            entity.MenuIcoUrl = param.MenuIcoUrl;
            entity.MenuIsShow = param.MenuIsShow;
            entity.MenuName = param.MenuName;
            entity.MenuNote = param.MenuNote;
            entity.MenuNumber = param.MenuNumber;
            entity.MenuParentID = param.MenuParentID;
            entity.MenuSortNo = param.MenuSortNo;
            entity.MenuState = param.MenuState;
            entity.MenuUrl = param.MenuUrl;

            if (param.MenuParentID.HasValue)
            {
                var parent = _system_MenuBaseService.GetById(param.MenuParentID.Value);
                entity.MenuParent = parent.Entity;
                entity.Layer = 2;
            }
            else
            {
                entity.Layer = 1;
            }
            var rel = _system_MenuBaseService.Update(entity);

            if (!rel.IsSuccess)
            {
                return EntityResponse<System_MenuBaseVM>.Error(rel.ErrorCode, rel.ErrorMessage);
            }

            return new BaseResponse();
        }
        [Route("remove")]
        [HttpPost]
        public BaseResponse DeleteCatalog(System_MenuBaseParam vm)
        {
            if (vm.Id == Guid.Empty)
            {
                return ErrorCode.DatabaseError;
            }

            var rel = _system_MenuBaseService.GetById(vm.Id);
            if (rel.Entity == null)
            {
                return ErrorCode.DatabaseError;
            }

            if (rel.Entity.Children.Count() > 0)
            {
                return BaseResponse.Error(ErrorCode.DatabaseError,"存在子集") ;
            }


            rel = _system_MenuBaseService.Delete(rel.Entity);

            return rel;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [Route("list")]
        [HttpPost]
        public EntityResponse<IEnumerable<System_MenuBaseVM>> list(System_MenuBaseFilter filter)
        {
            if (filter == null)
            {
                filter = new System_MenuBaseFilter();
            }
            var listEntits = _system_MenuBaseService.GetAll();
            if (filter.MenuSortNo.HasValue)
            {
                listEntits = listEntits.Where(c=>c.MenuSortNo==filter.MenuSortNo);
            }
            var totalRecords = listEntits.Count();
            var pageCount = Math.Ceiling((decimal)totalRecords / filter.PageSize);
            var pagination = new ResponsePagination
            {
                PageCount = (int)pageCount,
                PageIndex = filter.PageIndex,
                PageSize = filter.PageSize,
                TotalRecords = totalRecords
            };

            var list = listEntits
                        .OrderBy(c => c.LineNum)
                        .Skip(filter.PageSize * (filter.PageIndex - 1))
                        .Take(filter.PageSize).ToArray()
                        .Select(f => new System_MenuBaseVM(f, withChildren: false))
                        .ToArray();

            return EntityResponse<IEnumerable<System_MenuBaseVM>>.Success(list, pagination);
        }
        /// <summary>
        /// 所有节点
        /// </summary>
        /// <returns></returns>
        [Route("catalogTree")]
        [HttpGet]
        public IEnumerable<JsTreeVM> CatalogTree()
        {
            var list = _system_MenuBaseService.GetByParentId(null);

            JsTreeVM[] _list = new JsTreeVM[list.Entity.Count];
            var i = 0;
            foreach (System_MenuBase _item in list.Entity)
            {
                _list[i] = GetChildren(_item);
                i++;
            }
            return _list;
        }
        private JsTreeVM GetChildren(System_MenuBase catalog)
        {
            var jsTreeVm = new JsTreeVM(catalog.Id.ToString(), catalog.MenuName, null, "root",catalog.MenuUrl,"");
            if (catalog.Children.Any(c => !c.IsDeleted))
            {
                jsTreeVm.children = new List<JsTreeVM>();
                var children = new List<JsTreeVM>();
                foreach (var child in catalog.Children.Where(c => !c.IsDeleted))
                {
                    children.Add(GetChildren(child));
                }
                jsTreeVm.children = children;
            }

            return jsTreeVm;
        }
        [Route("tree")]
        [HttpPost]
        public EntityResponse<System_MenuBaseVM> CatalogTree(System_MenuBaseParam vm)
        {
            if (vm.Id == Guid.Empty)
            {
                return ErrorCode.None;
            }

            var rel = _system_MenuBaseService.GetById(vm.Id);

            return new System_MenuBaseVM(rel);
        }

        [Route("menuExtendMapping"), HttpPost]
        public BaseResponse MenuExtendMapping(MenuMappingParam param)
        {
            var result = _system_MenuBaseService.MenuMapping(param);
            return new BaseResponse();
        }

        /// <summary>
        /// 获取部门、岗位、
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("getExtendMenuMapping/{objectId}/{type}"), HttpGet]
        public IEnumerable<Guid> GetExtendMenuMapping(Guid objectId,int type)
        {
            return _system_MenuBaseService.GetExtendMenuMapping(objectId,type);
        }

        //[Route("test/{id}"), HttpGet]
        //public bool test(Guid id)
        //{
        //    var s = _system_MenuBaseService.MenuIsExistend(id);
        //    return s;
        //}
    }
}