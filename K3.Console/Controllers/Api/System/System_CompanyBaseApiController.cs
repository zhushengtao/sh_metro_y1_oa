﻿using GeneralFramework.BLL;
using GeneralFramework.Common;
using GeneralFramework.Model;
using GeneralFramework.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace GeneralFramework.Console.Controllers.Api
{
    [RoutePrefix("api/systemCompany")]

    public class System_CompanyBaseApiController : BaseApiController
    {
        private readonly System_CompanyBaseService _system_CompanyBaseService;
        public System_CompanyBaseApiController(System_CompanyBaseService system_CompanyBaseService)
        {
            _system_CompanyBaseService = system_CompanyBaseService;
        }
        /// <summary>
        /// 创建插入demo
        /// </summary>
        /// <param name="param">后台需要的对象-前端封装</param>
        /// <returns></returns>
        [Route("add"), HttpPost]
        public BaseResponse add(System_CompanyBaseParam param)
        {
            return SystemLogsBaseService.Execute("company Add", LogTypeCode.WebApi, "", (log) =>
            {
                log.AppendLine("创建公司开始");
                var entity = new System_CompanyBase();
                entity.CoAddress = param.CoAddress;
                entity.CoClientIP = string.Empty;
                entity.CoEmail = param.CoEmail;
                entity.CoFax = param.CoFax;
                entity.CoIsShow = param.CoIsShow;
                entity.CoName = param.CoName;
                entity.CoNote = param.CoNote;
                entity.CoNumber = param.CoNumber;
                entity.CoOtherName = param.CoOtherName;
                entity.CoOtherNumber = param.CoOtherNumber;
                entity.CoSortNo = param.CoSortNo;
                entity.CoState = param.CoState;
                entity.CoTelephone = param.CoTelephone;
                entity.CoTreeCode = param.CoTreeCode;
                entity.CreatedOn = param.CreatedOn;
                //to do - 其他业务逻辑处理 -异常处理
                var result = _system_CompanyBaseService.Create(entity);
                if (!result.IsSuccess)
                {
                    log.AppendLine("创建失败");
                }
                //remark log.AppendLine()-可用于任何需要记录日志的地方
                return result;
            });
        }
        /// <summary>
        /// 查询demo
        /// </summary>
        /// <returns></returns>
        [Route("list"), HttpPost]
        public EntityResponse<IEnumerable<System_CompanyBaseVM>> list(System_CompanyBaseFilter filter)
        {
            if (filter == null)
            {
                filter = new System_CompanyBaseFilter();
            }
            var listEntits = _system_CompanyBaseService.GetAll();
           
            var totalRecords = listEntits.Count();
            var pageCount = Math.Ceiling((decimal)totalRecords / filter.PageSize);
            var pagination = new ResponsePagination
            {
                PageCount = (int)pageCount,
                PageIndex = filter.PageIndex,
                PageSize = filter.PageSize,
                TotalRecords = totalRecords
            };

            var list = listEntits
                        .OrderBy(c => c.LineNum)
                        .Skip(filter.PageSize * (filter.PageIndex - 1))
                        .Take(filter.PageSize).ToArray()
                        .Select(f => new System_CompanyBaseVM(f))
                        .ToArray();

            return EntityResponse<IEnumerable<System_CompanyBaseVM>>.Success(list, pagination);
        }
        /// <summary>
        /// 创建插入demo
        /// </summary>
        /// <param name="param">后台需要的对象-前端封装</param>
        /// <returns></returns>
        [Route("update"), HttpPost]
        public BaseResponse update(System_CompanyBaseParam param)
        {
            return SystemLogsBaseService.Execute("company Update", LogTypeCode.WebApi, "", (log) =>
            {
                log.AppendLine("编辑开始");
                var entity = _system_CompanyBaseService.GetById(param.Id).Entity;
                if (entity == null)
                {
                    return BaseResponse.Error(ErrorCode.Existed, "数据不存在");
                }
                entity.CoAddress = param.CoAddress;
                entity.CoClientIP = string.Empty;
                entity.CoEmail = param.CoEmail;
                entity.CoFax = param.CoFax;
                entity.CoIsShow = param.CoIsShow;
                entity.CoName = param.CoName;
                entity.CoNote = param.CoNote;
                entity.CoNumber = param.CoNumber;
                entity.CoOtherName = param.CoOtherName;
                entity.CoOtherNumber = param.CoOtherNumber;
                entity.CoSortNo = param.CoSortNo;
                entity.CoState = param.CoState;
                entity.CoTelephone = param.CoTelephone;
                entity.CoTreeCode = param.CoTreeCode;
                entity.CreatedOn = param.CreatedOn;
                //to do - 其他业务逻辑处理 -异常处理
                var result = _system_CompanyBaseService.Update(entity);
                if (!result.IsSuccess)
                {
                    log.AppendLine("更新失败");
                }
                //remark log.AppendLine()-可用于任何需要记录日志的地方
                return result;
            });
        }
        /// <summary>
        /// 创建插入demo
        /// </summary>
        /// <param name="param">后台需要的对象-前端封装</param>
        /// <returns></returns>
        [Route("remove"), HttpPost]
        public BaseResponse remove(System_CompanyBaseParam param)
        {
            return SystemLogsBaseService.Execute("company Remove", LogTypeCode.WebApi, "", (log) =>
            {
                log.AppendLine("删除开始");
                var entity = _system_CompanyBaseService.GetById(param.Id).Entity;
                if (entity == null)
                {
                    return BaseResponse.Error(ErrorCode.Existed, "数据不存在");
                }
                var result = _system_CompanyBaseService.Delete(entity);
                if (!result.IsSuccess)
                {
                    log.AppendLine("操作失败");
                }
                //remark log.AppendLine()-可用于任何需要记录日志的地方
                return result;
            });
        } 
        /// <summary>
          /// 获取角色详情
          /// </summary>
          /// <param name="id"></param>
          /// <returns></returns>
        [Route("detail/{id}")]
        [HttpGet]
        public EntityResponse<System_CompanyBaseVM> Detail(Guid id)
        {
            var rel = _system_CompanyBaseService.GetById(id);

            if (!rel.IsSuccess)
            {
                return rel.ErrorCode;
            }

            if (rel.Entity == null)
            {
                return ErrorCode.DatabaseError;
            }
            return new System_CompanyBaseVM(rel);
        }
    }
}