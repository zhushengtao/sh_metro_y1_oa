﻿var employeeId = null;
$(function () {
    listObj.initLoadFun(); listObj.loadDataDeptTreeFun(); listObj.loadDataJobTreeFun();
});
$(".wrapper-tree").niceScroll({ cursorcolor: "#ccc", horizrailenabled: "false" });
var listObj = {
    //查询条件
    filter: {
        pageSize: 15,
        pageIndex: 1
    },
    //页面初始化加载
    initLoadFun: function () {
        //加载列表
        listObj.loadDataFun();
        //查询
        $('.buttonSearch').click(function () {
            listObj.filter.pageIndex = 1;
            listObj.loadDataFun();
        });
        //添加
        $(document).on('click', '#removeUser', function () {
            listObj.removeFun(this);
        }).on('click', "#deptMapp", function () {
            employeeId= $(this).parents('tr').attr('empid');
            $('#deptModal').modal('show');
            listObj.showDepartMapp();
        }).on('click', "#save_dept", function () {
            listObj.saveDeptMapping();
        }).on('click', "#jobMapp", function () {
            employeeId = $(this).parents('tr').attr('empid');
            $('#jobModal').modal('show');
            listObj.showJobMapp();
        }).on('click', "#save_job", function () {
            listObj.saveJobMapping();
        }).on('click', "#add", function ()
        {
            employeeId = null;
            listObj.clearField();
            $('#empModal').modal('show');
        }).on('click', "#btn_add", function ()
        {
            
            listObj.saveEmployee();
        }).on('click', "#updateEmp", function () {
            employeeId = $(this).parents('tr').attr('empid');
            $('#empModal').modal('show');
            listObj.showEmp();
        });
        $(".deptSelect").autocomplete({
            source: function (request, response) {
                var textV = request.term.toLowerCase();

                var result = deptOptions.filter(function (item) {
                    return item.deptName.toLowerCase().indexOf(textV) >= 0;
                });
                response(result.map(function (item) {
                    return {
                        label: item.deptName,
                        value: item.deptName,
                        entity: item,
                    };
                }));
            },
            autoFocus: true,
            change: function (event, ui) {
                if ($(event.target).val() == "") {
                    $(event.target).attr("data-deptid", "");
                    return;
                }
                if (ui.item) {
                    for (var i = 0; i < deptOptions.length; i++) {
                        var item = deptOptions[i];

                    }
                }
            },
            select: function (event, ui) {
                $(event.target).attr("data-deptid", ui.item.entity.id);
            },
            close: function (event, ui) {
                if ($(event.target).val() == "") {
                    $(event.target).attr("data-deptid", "");
                    return;
                }
                if (!$(event.target).attr("data-deptid")) {
                    $(event.target).attr("data-deptid", "").attr("data-deptid", "").val("");
                    return;
                } else {
                    for (var i = 0; i < deptOptions.length; i++) {
                        var item = deptOptions[i];
                        if (item.id == $(event.target).attr("data-deptid")) {
                            $(event.target).val(item.deptName);
                            break;
                        }
                    }
                }
            },
            minLength: 0
        });
        $(".jobSelect").autocomplete({
            source: function (request, response) {
                var textV = request.term.toLowerCase();

                var result = jobOptions.filter(function (item) {
                    return item.jobName.toLowerCase().indexOf(textV) >= 0;
                });
                response(result.map(function (item) {
                    return {
                        label: item.jobName,
                        value: item.jobName,
                        entity: item,
                    };
                }));
            },
            autoFocus: true,
            change: function (event, ui) {
                if ($(event.target).val() == "") {
                    $(event.target).attr("data-jobid", "");
                    return;
                }
                if (ui.item) {
                    for (var i = 0; i < jobOptions.length; i++) {
                        var item = jobOptions[i];

                    }
                }
            },
            select: function (event, ui) {
                $(event.target).attr("data-jobid", ui.item.entity.id);
            },
            close: function (event, ui) {
                if ($(event.target).val() == "") {
                    $(event.target).attr("data-jobid", "");
                    return;
                }
                if (!$(event.target).attr("data-jobid")) {
                    $(event.target).attr("data-jobid", "").attr("data-jobid", "").val("");
                    return;
                } else {
                    for (var i = 0; i < jobOptions.length; i++) {
                        var item = jobOptions[i];
                        if (item.id == $(event.target).attr("data-jobid")) {
                            $(event.target).val(item.jobName);
                            break;
                        }
                    }
                }
            },
            minLength: 0
        });
        $(".fromDeptSelect").autocomplete({
            source: function (request, response) {
                var textV = request.term.toLowerCase();

                var result = deptOptions.filter(function (item) {
                    return item.deptName.toLowerCase().indexOf(textV) >= 0;
                });
                response(result.map(function (item) {
                    return {
                        label: item.deptName,
                        value: item.deptName,
                        entity: item,
                    };
                }));
            },
            autoFocus: true,
            change: function (event, ui) {
                if ($(event.target).val() == "") {
                    $(event.target).attr("data-deptid", "");
                    return;
                }
                if (ui.item) {
                    for (var i = 0; i < deptOptions.length; i++) {
                        var item = deptOptions[i];

                    }
                }
            },
            select: function (event, ui) {
                $(event.target).attr("data-deptid", ui.item.entity.id);
            },
            close: function (event, ui) {
                if ($(event.target).val() == "") {
                    $(event.target).attr("data-deptid", "");
                    return;
                }
                if (!$(event.target).attr("data-deptid")) {
                    $(event.target).attr("data-deptid", "").attr("data-deptid", "").val("");
                    return;
                } else {
                    for (var i = 0; i < deptOptions.length; i++) {
                        var item = deptOptions[i];
                        if (item.id == $(event.target).attr("data-deptid")) {
                            $(event.target).val(item.deptName);
                            break;
                        }
                    }
                }
            },
            minLength: 0
        });
        $(".fromJobSelect").autocomplete({
            source: function (request, response) {
                var textV = request.term.toLowerCase();

                var result = jobOptions.filter(function (item) {
                    return item.jobName.toLowerCase().indexOf(textV) >= 0;
                });
                response(result.map(function (item) {
                    return {
                        label: item.jobName,
                        value: item.jobName,
                        entity: item,
                    };
                }));
            },
            autoFocus: true,
            change: function (event, ui) {
                if ($(event.target).val() == "") {
                    $(event.target).attr("data-jobid", "");
                    return;
                }
                if (ui.item) {
                    for (var i = 0; i < jobOptions.length; i++) {
                        var item = jobOptions[i];

                    }
                }
            },
            select: function (event, ui) {
                $(event.target).attr("data-jobid", ui.item.entity.id);
            },
            close: function (event, ui) {
                if ($(event.target).val() == "") {
                    $(event.target).attr("data-jobid", "");
                    return;
                }
                if (!$(event.target).attr("data-jobid")) {
                    $(event.target).attr("data-jobid", "").attr("data-jobid", "").val("");
                    return;
                } else {
                    for (var i = 0; i < jobOptions.length; i++) {
                        var item = jobOptions[i];
                        if (item.id == $(event.target).attr("data-jobid")) {
                            $(event.target).val(item.jobName);
                            break;
                        }
                    }
                }
            },
            minLength: 0
        });

    },
    //分页加载列表数据
    loadDataFun: function () {
        listObj.filter.Keywords = $('#keyWords').val();
        listObj.filter.EmpJobID = $('#txtJob').attr("data-jobid");
        listObj.filter.EmpDeptID = $('#txtDept').attr("data-deptid");
        $.post("/api/employee/list", listObj.filter, function (data) {
            $('.gtco-loader').hide();
            if (data) {
                if (data.isSuccess) {

                    listObj.createPaginationFun(data.pagination.pageCount);
                    var template = $("#template").html();
                    Mustache.parse(template); //
                    var rendered = Mustache.render(template, data);
                    $("#gridList").html(rendered);
                    $("#totalPagesCounter").html(data.pagination.pageCount);
                    $("#totalRecords").html(data.pagination.totalRecords);
                }
                else
                    alert(rel.errorMessage)
            } else
                alert("unknown error!")

        }).fail(function (ex) { $('.gtco-loader').hide(); });
    },
    //生成分页码
    createPaginationFun: function (totalPage) {

        $('#number-page').off().empty();
        $('#number-page').bootpag({
            total: totalPage,
            maxVisible: 10,
            page: listObj.filter.pageIndex,
            firstLastUse: true,
            next: '>',
            prev: '<',
            first: '<<',
            last: '>>'
        })
        .on("page", function (event, num) {
            listObj.filter.pageIndex = num;
            listObj.loadDataFun();
        });
        $('#number-page').find("li").removeClass("active");
        if (listObj.filter.pageIndex != 1) {
            $('#number-page').find("li[data-lp='" + listObj.filter.pageIndex + "']").first().addClass("active");
        } else {
            $('#number-page').find("li[data-lp='" + listObj.filter.pageIndex + "']").last().addClass("active");
        }
    },
   
    removeFun: function (obj) {
        if (!confirm('请确认是否要执行操作？')) return false;
        var param = {};
        param.id = $(obj).parents('tr').attr('db_id');

        $.post("/api/systemuser/remove", param, function (rel) {

            if (rel) {
                if (rel.isSuccess) {
                    listObj.loadDataFun();
                    alert('删除成功');
                } else {
                    alert(rel.errorMessage)
                }
            } else {
                alert("unknown error!")
            }
        }).fail(function (ex) { });
    },
    updateFun: function (obj) {
        location.href = "/Web/System/UserEditor?id=" + obj;
    },
    //加载部门数
    loadDataDeptTreeFun: function () {
        $.ajax({
            url: "/api/systemDept/deptTree",
            type: "GET",
            success: function (result) {
                $('#dept_tree').jstree({
                    "core": {
                        "animation": 0,
                        "check_callback": true,
                        "themes": {
                            "name": "default",
                            "dots": true,
                            "icons": true
                        },
                        'data': result,
                    },
                    'contextmenu': {
                        items: {
                            "ccp": false
                        }
                    },
                    "types": {
                        "#": { "max_children": 1, "max_depth": 4, "valid_children": ["root"] },
                        "root": { "icon": "ion-social-buffer-outline", "valid_children": ["default"] },
                        "default": { "valid_children": ["default", "file"] },
                        "file": { "icon": "glyphicon glyphicon-map-marker", "valid_children": [] }
                    },
                    "checkbox": {
                        "keep_selected_style": true
                    },

                    "plugins": ["contextmenu", "dnd", "search", "state", "types", "wholerow", "checkbox"]
                });
            }
        });
      
    },
    //加载岗位
    loadDataJobTreeFun: function () {
        $.ajax({
            url: "/api/systemJob/jobTree",
            type: "GET",
            success: function (result) {
                $('#job_tree').jstree({
                    "core": {
                        "animation": 0,
                        "check_callback": true,
                        "themes": {
                            "name": "default",
                            "dots": true,
                            "icons": true
                        },
                        'data': result,
                    },
                    'contextmenu': {
                        items: {
                            "ccp": false
                        }
                    },
                    "types": {
                        "#": { "max_children": 1, "max_depth": 4, "valid_children": ["root"] },
                        "root": { "icon": "ion-social-buffer-outline", "valid_children": ["default"] },
                        "default": { "valid_children": ["default", "file"] },
                        "file": { "icon": "glyphicon glyphicon-map-marker", "valid_children": [] }
                    },
                    "checkbox": {
                        "keep_selected_style": true
                    },

                    "plugins": ["contextmenu", "dnd", "search", "state", "types", "wholerow", "checkbox"]
                });
              
            }
        });
    },
    saveDeptMapping:function()
    {
        var depts = $("#dept_tree").jstree().get_checked(0, true);
        if (depts == null || depts == '')
            depts = [];
        $.ajax({
            url: "/api/employee/empExtendMapping",
            type: "POST",
            data: {
                EmpId: employeeId,
                ExtendIds: depts,
                ExtType:1
            },
            success: function (result) {

                alert('操作成功');
            }
        });
    },
    saveJobMapping: function () {
        var depts = $("#job_tree").jstree().get_checked(0, true);
        if (depts == null || depts == '')
            depts = [];
        $.ajax({
            url: "/api/employee/empExtendMapping",
            type: "POST",
            data: {
                EmpId: employeeId,
                ExtendIds: depts,
                ExtType: 2
            },
            success: function (result) {

                alert('操作成功');
            }
        });
    },
    showDepartMapp: function ()
    {
        var jstree = $("#dept_tree").jstree();
        jstree.uncheck_all();//清空
        $.ajax({
            url: "/api/employee/getExtendEmpMapping/" + employeeId + "/1",
            type: "GET",
            success: function (result) {
                function expandNode(node) {
                    if (!jstree.is_open(node)) {
                        jstree.open_node(node);
                        if (node.parents.length) {
                            node.parents.map(function (id) {
                                var n = jstree.get_node(id);
                                expandNode(n);
                            });
                        }
                    }
                }
                result.map(function (id) {
                    expandNode(jstree.get_node(id));
                });
                var regionsIds = result;
                for (var i = 0; i < result.length; i++) {
                    jstree.check_node(result[i] + '_anchor');

                }
            }
        });
    },
    showJobMapp: function () {
        var jstree = $("#job_tree").jstree();
        jstree.uncheck_all();//清空
        $.ajax({
            url: "/api/employee/getExtendEmpMapping/" + employeeId + "/2",
            type: "GET",
            success: function (result) {
                function expandNode(node) {
                    if (!jstree.is_open(node)) {
                        jstree.open_node(node);
                        if (node.parents.length) {
                            node.parents.map(function (id) {
                                var n = jstree.get_node(id);
                                expandNode(n);
                            });
                        }
                    }
                }
                result.map(function (id) {
                    expandNode(jstree.get_node(id));
                });
                var regionsIds = result;
                for (var i = 0; i < result.length; i++) {
                    jstree.check_node(result[i] + '_anchor');

                }
            }
        });
    },
    saveEmployee: function ()
    {
        if ($("#EmpName").val() == "")
        {
            alert("请输入员工姓名");
            return;
        }
        if ($("#EmpWorkID").val() == "") {
            alert("请输入员工工号");
            return;
        }
        if ($("#EmpDeptID").val() == "") {
            alert("请选择员工部门");
            return;
        }
        if ($("#EmpJobID").val() == "") {
            alert("请选择员工岗位");
            return;
        }
        var param={};
        param.EmpName=$("#EmpName").val(),
        param.EmpSex=$("#EmpSex").val(),
        param.EmpBirthday=$("#EmpBirthday").val(),
        param.EmpWorkID=$("#EmpWorkID").val(),
        param.EmpEmail=$("#EmpEmail").val(),
        param.EmpTelePhone=$("#EmpTelePhone").val(),
        param.EmpOfficePhone=$("#EmpOfficePhone").val(),
        param.EmpFax=$("#EmpFax").val(),
        param.EmpCoID=$("#EmpCoID").val(),
        param.EmpDeptID=$("#EmpDeptID").attr("data-deptid"),
        param.EmpJobID=$("#EmpJobID").attr("data-jobid"),
        param.EmpJobNote=$("#EmpJobNote").val(),
        param.EmpAddress=$("#EmpAddress").val(),
        param.EmpEducation=$("#EmpEducation").val(),
        param.EmpBankCardNumber=$("#EmpBankCardNumber").val(),
        param.EmpJoinWorkTime=$("#EmpJoinWorkTime").val(),
        param.EmpHomePhone=$("#EmpHomePhone").val(),
        param.EmpLanguageLevel=$("#EmpLanguageLevel").val()
        var url = "/api/employee/add";
        if (employeeId != null)
        {
            url = "/api/employee/update";
            param.id = employeeId;
        }
      
        $.ajax({
            url: url,
            type: "POST",
            data: param,
            success: function (result) {
                employeeId = null;
                listObj.loadDataFun();
                $('#empModal').modal('hide');
                alert('操作成功');
            }
        });
    },
    clearField: function ()
    {
        $("#EmpName").val('');
        $("#EmpSex").val('');
        $("#EmpBirthday").val('');
        $("#EmpWorkID").val('');
        $("#EmpEmail").val('');
        $("#EmpTelePhone").val('');
        $("#EmpOfficePhone").val('');
        $("#EmpFax").val('');
        $("#EmpCoID").val('');
        $("#EmpDeptID").attr("data-deptid",'');
        $("#EmpJobID").attr("data-jobid",'');
        $("#EmpDeptID").val('');
        $("#EmpJobID").val('');
        $("#EmpJobNote").val('');
        $("#EmpAddress").val('');
        $("#EmpEducation").val('');
        $("#EmpBankCardNumber").val('');
        $("#EmpJoinWorkTime").val('');
        $("#EmpHomePhone").val('');
        $("#EmpLanguageLevel").val('');
    },
    showEmp: function ()
    {
        var url = "/api/employee/info/" + employeeId;
        $.get(url, null, function (rel) {
            if (rel) {
                if (rel.isSuccess) {
                    if (rel.entity) {
                       
                        $("#EmpName").val(rel.entity.empName);
                        $("#EmpSex").val(rel.entity.empSex);
                        $("#EmpBirthday").val(rel.entity.empBirthday);
                        $("#EmpWorkID").val(rel.entity.empWorkID);
                        $("#EmpEmail").val(rel.entity.empEmail);
                        $("#EmpTelePhone").val(rel.entity.empTelePhone);
                        $("#EmpOfficePhone").val(rel.entity.empOfficePhone);
                        $("#EmpFax").val(rel.entity.empFax);
                        $("#EmpCoID").val(rel.entity.empCoID);
                        $("#EmpDeptID").attr("data-deptid", rel.entity.empDeptID);
                        $("#EmpJobID").attr("data-jobid", rel.entity.empJobID);
                        $("#EmpDeptID").val(rel.entity.deptName);
                        $("#EmpJobID").val(rel.entity.jobName);
                        $("#EmpJobNote").val(rel.entity.empJobNote);
                        $("#EmpAddress").val(rel.entity.empAddress);
                        $("#EmpEducation").val(rel.entity.empEducation);
                        $("#EmpBankCardNumber").val(rel.entity.empBankCardNumber);
                        $("#EmpJoinWorkTime").val(rel.entity.empJoinWorkTime);
                        $("#EmpHomePhone").val(rel.entity.empHomePhone);
                        $("#EmpLanguageLevel").val(rel.entity.empLanguageLevel);
                    }
                } else {
                    alert(rel.errorMessage)
                }
            } else {
                alert("unknown error!")
            }

        }).done(function (data) { })
            .fail(function (ex) { });
    }
};