﻿var userId = null;
var employeeId = null;
$(function () {
    listObj.initLoadFun();
    listObj.loadRoles();
});
var listObj = {
    //查询条件
    filter: {
        pageSize: 15,
        pageIndex: 1
    },
    //页面初始化加载
    initLoadFun: function () {
        //加载列表
        listObj.loadDataFun();
        //查询
        $('.buttonSearch').click(function () {
            listObj.filter.pageIndex = 1;
            listObj.loadDataFun();
        });
        //添加
        $(document).on('click', '#removeUser', function () {
            listObj.removeFun(this);
        }).on('click', '#create', function () {
            listObj.clearField();
            userId = null;
            $("#add").modal('show');
        }).on('click', "#btnCreate", function () {
            listObj.saveFun();
        });
        $(".deptSelect").autocomplete({
            source: function (request, response) {
                var textV = request.term.toLowerCase();

                var result = deptOptions.filter(function (item) {
                    return item.deptName.toLowerCase().indexOf(textV) >= 0;
                });
                response(result.map(function (item) {
                    return {
                        label: item.deptName,
                        value: item.deptName,
                        entity: item,
                    };
                }));
            },
            autoFocus: true,
            change: function (event, ui) {
                if ($(event.target).val() == "") {
                    $(event.target).attr("data-deptid", "");
                    return;
                }
                if (ui.item) {
                    for (var i = 0; i < deptOptions.length; i++) {
                        var item = deptOptions[i];

                    }
                }
            },
            select: function (event, ui) {
                $(event.target).attr("data-deptid", ui.item.entity.id).attr("data-deptid", ui.item.entity.id);
            },
            close: function (event, ui) {
                if ($(event.target).val() == "") {
                    $(event.target).attr("data-deptid", "");
                    return;
                }
                if (!$(event.target).attr("data-deptid")) {
                    $(event.target).attr("data-deptid", "").attr("data-deptid", "").val("");
                    return;
                } else {
                    for (var i = 0; i < deptOptions.length; i++) {
                        var item = deptOptions[i];
                        if (item.id == $(event.target).attr("data-deptid")) {
                            $(event.target).val(item.deptName);
                            break;
                        }
                    }
                }
            },
            minLength: 0
        });
        $(".workIdSelect").autocomplete({
            source: function (request, response) {
                var textV = request.term.toLowerCase();

                var result = empOptions.filter(function (item) {
                    return item.workId.toLowerCase().indexOf(textV) >= 0;
                });
                response(result.map(function (item) {
                    return {
                        label: item.workId,
                        value: item.workId,
                        entity: item,
                    };
                }));
            },
            autoFocus: true,
            change: function (event, ui) {
                if ($(event.target).val() == "") {
                    $(event.target).attr("data-empid", "");
                    return;
                }
                if (ui.item) {
                    for (var i = 0; i < empOptions.length; i++) {
                        var item = empOptions[i];

                    }
                }
            },
            select: function (event, ui) {
                $(event.target).attr("data-empid", ui.item.entity.id);
            },
            close: function (event, ui) {
                if ($(event.target).val() == "") {
                    $(event.target).attr("data-empid", "");
                    return;
                }
                if (!$(event.target).attr("data-empid")) {
                    $(event.target).attr("data-empid", "").attr("data-empid", "").val("");
                    return;
                } else {
                    for (var i = 0; i < empOptions.length; i++) {
                        var item = empOptions[i];
                        if (item.id == $(event.target).attr("data-empid")) {
                            $(event.target).val(item.workId);
                            break;
                        }
                    }
                }
            },
            minLength: 0
        });
        $(".jobSelect").autocomplete({
            source: function (request, response) {
                var textV = request.term.toLowerCase();

                var result = jobOptions.filter(function (item) {
                    return item.jobName.toLowerCase().indexOf(textV) >= 0;
                });
                response(result.map(function (item) {
                    return {
                        label: item.jobName,
                        value: item.jobName,
                        entity: item,
                    };
                }));
            },
            autoFocus: true,
            change: function (event, ui) {
                if ($(event.target).val() == "") {
                    $(event.target).attr("data-jobid", "");
                    return;
                }
                if (ui.item) {
                    for (var i = 0; i < jobOptions.length; i++) {
                        var item = jobOptions[i];

                    }
                }
            },
            select: function (event, ui) {
                $(event.target).attr("data-jobid", ui.item.entity.id);
            },
            close: function (event, ui) {
                if ($(event.target).val() == "") {
                    $(event.target).attr("data-jobid", "");
                    return;
                }
                if (!$(event.target).attr("data-jobid")) {
                    $(event.target).attr("data-jobid", "").attr("data-jobid", "").val("");
                    return;
                } else {
                    for (var i = 0; i < jobOptions.length; i++) {
                        var item = jobOptions[i];
                        if (item.id == $(event.target).attr("data-jobid")) {
                            $(event.target).val(item.jobName);
                            break;
                        }
                    }
                }
            },
            minLength: 0
        });
    },
    //分页加载列表数据
    loadDataFun: function () {
        listObj.filter.Keywords = $('#keyWords').val();
        listObj.filter.JobId = $('#txtJob').attr("data-jobid");
        listObj.filter.DepId = $('#txtDept').attr("data-deptid");
        $.post("/api/systemuser/list", listObj.filter, function (data) {
            $('.gtco-loader').hide();
            if (data) {
                if (data.isSuccess) {

                    listObj.createPaginationFun(data.pagination.pageCount);
                    var template = $("#template").html();
                    Mustache.parse(template); //
                    var rendered = Mustache.render(template, data);
                    $("#gridList").html(rendered);
                    $("#totalPagesCounter").html(data.pagination.pageCount);
                    $("#totalRecords").html(data.pagination.totalRecords);
                }
                else
                    alert(rel.errorMessage)
            } else
                alert("unknown error!")

        }).fail(function (ex) { $('.gtco-loader').hide(); });
    },
    //生成分页码
    createPaginationFun: function (totalPage) {

        $('#number-page').off().empty();
        $('#number-page').bootpag({
            total: totalPage,
            maxVisible: 10,
            page: listObj.filter.pageIndex,
            firstLastUse: true,
            next: '>',
            prev: '<',
            first: '<<',
            last: '>>'
        })
        .on("page", function (event, num) {
            listObj.filter.pageIndex = num;
            listObj.loadDataFun();
        });
        $('#number-page').find("li").removeClass("active");
        if (listObj.filter.pageIndex != 1) {
            $('#number-page').find("li[data-lp='" + listObj.filter.pageIndex + "']").first().addClass("active");
        } else {
            $('#number-page').find("li[data-lp='" + listObj.filter.pageIndex + "']").last().addClass("active");
        }
    },
    //保存
    saveFun: function () {
        if ($("#u_workId").val() == "")
        {
            alert("请输入工号");
            return;
        }
        if ($("#u_name").val() == "") {
            alert("请输入账号");
            return;
        }
        if ($("#u_password").val() == "") {
            alert("请输入密码");
            return;
        }

        var url = "/api/systemuser/add";
        var param = {};
        param.Username = $("#u_name").val();
        param.Password = $("#u_password").val();
    
        param.isLockedout = $('input[name="locked"]:checked').val();
        param.UserEmpID = $("#u_workId").attr("data-empid");
        param.UserWorkNumber = $("#u_workId").val();
        if (userId != null) {
            url = "/api/systemuser/update";
            param.id = userId;
        }
        $.post(url, param, function (rel) {

            if (rel) {
                if (rel.isSuccess) {
                    alert("操作成功");
                    listObj.loadDataFun();
                } else {
                    alert(rel.errorMessage)
                }
            } else {
                alert("unknown error!")
            }

        }).done(function (data) { location.reload(); })
            .fail(function (ex) { });
    },
    removeFun: function (obj)
    {
        if (!confirm('请确认是否要执行操作？')) return false;
        var param = {};
        param.id =  $(obj).parents('tr').attr('db_id');

        $.post("/api/systemuser/remove", param, function (rel) {

            if (rel) {
                if (rel.isSuccess) {
                    listObj.loadDataFun();
                    alert('删除成功');
                } else {
                    alert(rel.errorMessage)
                }
            } else {
                alert("unknown error!")
            }
        }).fail(function (ex) { });
    },
    updateFun: function (obj)
    {
        $("#add").modal('show');
        userId = obj;
        var url = "/api/systemuser/" + userId;
        $.get(url, function (rel) {
            if (rel) {
                if (rel.isSuccess) {
                    if (rel.entity) {
                        $("#u_name").val(rel.entity.username);

                      
                        $("#u_password").val(rel.entity.password);
                     
                        $("#u_workId").val(rel.entity.userWorkNumber);
                        var $radios2 = $('input:radio[name=locked]');
                        if (rel.entity.isLockedOut) {
                            $radios2.filter('[value=true]').prop('checked', true);
                        } else {
                            $radios2.filter('[value=false]').prop('checked', true);
                        }
                    }
                } else {
                    alert(rel.errorMessage)
                }
            } else {
                alert("unknown error!")
            }

        }).done(function (data) { })
            .fail(function (ex) { });
    },
    clearField: function ()
    {
        $("#u_name").val('');
     
        $("#u_password").val('');
      
        $("#u_workId").val('');
    },
    loadRoles: function ()
    {
       
        var filterRole = {};
        filterRole.keywords = "";
        filterRole.pageSize = 5000000;
        filterRole.pageIndex = 1;

        $.post("/api/role/list", filterRole, function (data) {
            if (data) {
                if (data.isSuccess) {
                    var html = '';
                    $.each(data.entity, function (i, item) {
                        html += '<label class="checkbox-inline" style="margin-left: 20px;min-width: 220px;"><input type="checkbox" name="role" value="' + item.id + '" id="role' + item.id + '">' + item.roleName + '</label>';
                    });
                    $('#rolesList').html(html);
                  
                } else {
                    alert(data.errorMessage);
                }
            } else {
                alert("unknown error!");
            }

        }).fail(function (ex) { });
    },
    saveRole: function ()
    {
        if ($('#rolesList input[name="role"]:checked').length == 0) {
            alert('请选择角色!');
            return;
        }
        var roleList = [];
        $('#rolesList input[name="role"]:checked').each(function () {
            roleList.push($(this).val());
        });
        var param = {
            EmpId: employeeId,
            ExtendIds: roleList,
            ExtType: 3
        };
        $.post('/api/employee/empExtendMapping', param, function (rel) {

            if (rel) {
                if (rel.isSuccess) {
                    alert("success");
                    $('#roleModal').modal('hide');
                } else {
                    alert(rel.errorMessage)
                }
            } else {
                alert("unknown error!")
            }

        }).done(function (data) {
        }).fail(function (ex) { });
    },
    showRoleFun: function (obj)
    {
        employeeId = obj;
        $.ajax({
            url: "/api/employee/getExtendEmpMapping/" + obj + "/3",
            type: "GET",
            success: function (roles) {
             
                for (var i = 0; i < roles.length; i++) {
                    //var currentId = roles[i];
                    //var result = $.grep(roles, function (e) { return e.id == currentId; });
                    //if (result.length > 0) {
                    //    $('#role' + currentId).prop('checked', true);
                    //} else {
                    //    $('#role' + currentId).prop('checked', false);
                    //}
                    $('#role' + roles[i]).prop('checked', true);
                }
            } 
        });
    },
    clearSerach: function ()
    {
        $("#keyWords").val('');
        $("#txtJob").attr("data-jobid","");
        $("#txtDept").val("data-depid","");
        listObj.loadDataFun();
    }

};