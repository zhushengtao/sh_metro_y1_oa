﻿var observeId;
function RefreshTree() {
    $('#jstree_catalog').jstree("refresh");
}
function modify() {
    var ref = $('#jstree_catalog').jstree(true),
    sel = ref.get_selected();
    if (!sel.length) { return false; }
    sel = sel[0];
    observeId = sel;
    if (sel) {
        update(sel);
    }
    else {
        alert("Please select a node!");
    }
};
function removed() {
    if (!confirm('请确认是否要执行操作？')) return false;
    var treeNode = $('#jstree_catalog').jstree(true).get_selected(true);

    if (!treeNode.length) { return false; }
    var id = treeNode[0].id;
    var param = {};
    param.id = id;
    $.post("/api/systemMenu/remove", param, function (rel)
    {
        if (rel) {
            if (rel.isSuccess) {
                alert("操作成功");
                RefreshTree();
            } else {
                alert(rel.errorMessage)
            }
        } else {
            alert("unknown error!")
        }
    }).fail(function (ex) { });
};
$(".wrapper-tree").niceScroll({ cursorcolor: "#ccc", horizrailenabled: "false" });
$(function () {
    var to = false;
    $.ajax({
        url: "/api/systemMenu/catalogTree",
        type: "GET",
        success: function (result) {
            $('#jstree_catalog').jstree({
                "core": {
                    "animation": 0,
                    "check_callback": true,
                    "themes": {
                        "name": "default",
                        "dots": true,
                        "icons": true
                    },
                    'data': result
                },
                'contextmenu': {
                    items: {
                        "ccp": false
                    }
                },
                "types": {
                    "#": { "max_children": 1, "max_depth": 4, "valid_children": ["root"] },
                    "root": { "icon": "ion-android-folder-open", "valid_children": ["default"] },
                    "default": { "valid_children": ["default", "file"] },
                    "file": { "icon": "ion-android-folder-open", "valid_children": [] }
                },
                "plugins": ["contextmenu", "dnd", "search", "state", "types", "wholerow"]
            });
        }
    });
    $("#create").on("click", function () {
        clearValues();
        observeId = null;
        $("#add").modal('show');
    });
});

var selectedId = null;
function update(id) {

    observeId = id;
    var url = "/api/systemMenu/tree";
    var param = {};
    param.Id = id;
    $.post(url, param, function (rel) {
        if (rel) {
            if (rel.isSuccess) {
                if (rel.entity) {
                    $("#ParentId").val(rel.entity.menuParentID);
                  
                    $("#CatalogName").val(rel.entity.menuName);
                    $("#LinkUrl").val(rel.entity.menuUrl);
                    $("#add").modal('show');
                }
            } else {
                alert(rel.errorMessage)
            }
        } else {
            alert("unknown error!")
        }

    }).done(function (data) { })
        .fail(function (ex) { });

}
function addNew() {
    var url = "/api/systemMenu/add";
    var param = {};
    if ($("#CatalogName").val() == "")
    {
        alert('请输入目录名称');
        return;
    }
    if ($("#LinkUrl").val() == "") {
        alert('请输入目录地址');
        return;
    }
    param.MenuName =$("#CatalogName").val();
    param.MenuParentID =  $("#ParentId").val();
    param.MenuUrl = $("#LinkUrl").val();
    param.MenuIcoUrl = $("#Icon").val();
    if (observeId != null) {
        url = "/api/systemMenu/update";
        param.id = observeId;
    }
    $.post(url, param, function (rel) {

        if (rel) {
            if (rel.isSuccess) {

                alert("操作成功");

                clearValues();

                RefreshTree();

            } else {
                alert(rel.errorMessage)
            }
        } else {
            alert("unknown error!")
        }

    }).done(function (data) { location.reload(); })
        .fail(function (ex) { });
}
function clearValues() {
    $("#CatalogName").val("");
    $("#LinkUrl").val("");
    $("#ParentId").val("");
}
//close the parent nav menu in the index when click in all window
function closeMenu() {
    parent.closeMenu();
}