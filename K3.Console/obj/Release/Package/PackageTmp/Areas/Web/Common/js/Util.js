﻿function getUrlParam(param)
{
   var url = window.location.search.substring(1);
   var paramArray = url.split("&");
   if(paramArray.length != 0)
       for(var i = 0;i < paramArray.length;i++)
       {
            var pramArraySub = paramArray[i].split("=");
            if(pramArraySub[0] == param && pramArraySub[1] != "undefined")
            return pramArraySub[1];
       }
   else
   {
        paramArray = url.split("=");
        if(paramArray[0] == param && paramArray[1] != "undefined")
            return paramArray[1];
   }
   return "";
}
function getUrlPageName() { 
    var url = window.location.href.substring(0);

    url = url.split("?")[0];

    url = url.substring(url.lastIndexOf("/") + 1, url.length);

    return url;
}
function getUrlString(url, param) {
    url = url.split("?")[1];
    var paramArray = url.split("&");
    if (paramArray.length != 0) {
        for (var i = 0; i < paramArray.length; i++) {
            var pramArraySub = paramArray[i].split("=");
            if (pramArraySub[0] == param && pramArraySub[1] != "undefined")
                return pramArraySub[1];
        } 
    }
    else {
        paramArray = url.split("=");
        if (paramArray[0] == param && paramArray[1] != "undefined") {
            return paramArray[1];
        }
    }
    return "";
}
function ClosePage()
{
     window.opener = new Object();
     self.close();
 }

function ValidateChar(para)
{
    var strTmp = new String();   
    strTmp = para;   

    for(var i = 0; i < strTmp.length; i++){   
        if(strTmp.charCodeAt(i) > 128)   
            window.alert("全角字符：" + strTmp.charAt(i));
        else   
            window.alert("半角字符：" + strTmp.charAt(i));
    }
}
function IsNull(o)
{
    return ("undefined" == typeof(o) || "unknown" == typeof(o) || null == o);
}
function Trim(s) { 
    s = s.replace(/^\s+|\s+$/g, ''); 
    var chars = String.fromCharCode(0x0085, 0x00A0, 0x2028, 0x2029, 0x1680, 0x180E, 0x2000, 0x2001, 0x2002, 0x2003, 0x2004, 0x2005, 0x2006, 0x2007, 0x2008, 0x2009, 0x200A, 0x202F, 0x205F, 0x3000);
    for (var i = 0; i < chars.length; i++)
    {
        var iChar = chars.charCodeAt(i);
        var sChar = "";
        if (iChar < 0x100)
        {
            sChar += "00";
        }
        else if (iChar < 0x1000)
        {
            sChar += "0";
        }

        sChar += iChar.toString(16);

        s = s.replace(new RegExp("^\\u" + sChar + "+|\\u" + sChar + "+$", "g"), '');
    }

    return s;
}
function TrimSpaces(s)
{
    if (s != null)
    {
        s = Trim(s);
        return s.replace(/\s+/g,' ');
    }

    return null;
}
var LOCID_DIALOG_OFFSET_WIDTH = "0";
var LOCID_DIALOG_OFFSET_HEIGHT = "0";
var LOCID_UI_DIR = "LTR";
var DialogRetryReturnValue = "___RETRY___";
function openStdDlg(sPath, oArgs, iWidth, iHeight, bResizable, bModeless, sCustomWinParams)
{
    if (IsNull(sCustomWinParams))
    {
        sCustomWinParams = "";
    }

    // 	By default all modal dialog is resizable
    if(IsNull(bResizable))
    {
        bResizable = true;
    }
    // Take into consideration any global increases in dialog size.  Used by localization teams.
    // Only use these increased sizes if the dialog is already resizeable.  If it is not resizeable then the increased sizes will
    // most likely not help much because the dialog probably has a fixed layout.
    if (bResizable)
    {
        iWidth	+= parseInt(LOCID_DIALOG_OFFSET_WIDTH, 10);
        iHeight += parseInt(LOCID_DIALOG_OFFSET_HEIGHT, 10);
    }
    // Adjust height for screen size.
    iWidth	= getAdjustedWidthForScreen(iWidth);
    iHeight	= getAdjustedHeightForScreen(iHeight);
    bResizable = (!IsNull(bResizable) && bResizable) ? "yes" : "no";
    // Get the left and top margin.
    var iLeft = getLeftMargin(iWidth);
    var iTop = getTopMargin(iHeight);
    if (LOCID_UI_DIR == "RTL")
        iLeft = screen.availWidth-iLeft-iWidth;
    sCustomWinParams = "dialogWidth:" + iWidth + "px;dialogHeight:" + iHeight + "px;dialogLeft=" + iLeft + "px;dialogTop=" + iTop + "px;help:0;status:1;scroll:0;center:1;resizable:" + bResizable + ";" + sCustomWinParams;
    if (IsNull(bModeless) || !bModeless)
    {
        return safeWindowShowModalDialog(sPath, oArgs, sCustomWinParams);
    }
    else
    {
        return safeWindowShowModelessDialog(sPath, oArgs, sCustomWinParams);
    }
}
function safeWindowShowModalDialog(sUrl, vArguments, sFeatures)
{
    // Make sure the dialog arguments are set.
    if (vArguments === null)
    {
        vArguments = "";
    }
    var returnVar = null;
    try
    {
        do
        {
            returnVar = window.showModalDialog(sUrl, vArguments, sFeatures);
        }
        while (returnVar === DialogRetryReturnValue);
    }
    catch (e)
    {
        handlePopupBlockerError();
    }
    return returnVar;
}
function safeWindowShowModelessDialog(sUrl, vArguments, sFeatures)
{
    var windowOpened = null;
    try
    {
        windowOpened = window.showModelessDialog(sUrl, vArguments, sFeatures);
    }
    catch (e)
    {
    }
    if (IsNull(windowOpened))
    {
        handlePopupBlockerError();
    }
    return windowOpened;
}
//This variable is set as a window is about to open, then can be checked in the window.onload (or similar).  Don't use it in any
// other event handlers as it is global and may have changed due to other windows opening.
// Also, there is a chance that two separate clicks could cause windows to open and the first value set gets overriden
// by the second click.  This would be a rare occurance, but useage of this variable should take this into account.
var _bWindowSizeAdjusted = false;
// Given a screen width will adjust it to fit in the current screen.  If it is empty it will default it to the application default.
function getAdjustedWidthForScreen(iX)
{
    var iWidth = iX;
    if (IsNull(iX) || iX == 0)
    {
        iX = (screen.availWidth >= 1000) ? 820 : 750;
    }
    iX = (iX >= screen.availWidth) ? screen.availWidth : iX;
    _bWindowSizeAdjusted = (iX != iWidth);
    return iX;
}
// Given a screen height will adjust it to fit in the current screen.  If it is empty it will default it to the application default.
function getAdjustedHeightForScreen(iY)
{
    if (IsNull(iY) || iY == 0)
    {
        return (screen.availHeight >= 600) ? 560 : 510;
    }
    else
    {
        return (iY >= screen.availHeight) ? screen.availHeight : iY;
    }
}
// Calculate the left and top margin to place the form in center.
function getLeftMargin(iX)
{
    var iLeft = 0;
    if ((screen.availWidth - iX) > 0)
    {
        iLeft = (screen.availWidth - iX)/2;
    }
    return iLeft;
}
function getTopMargin(iY)
{
    var iTop = 0;
    if ((screen.availHeight - iY) > 0)
    {
        iTop = (screen.availHeight - iY)/2;
    }
    return iTop;
}
function buildWinName(s)
{
    if (s) return s.toLowerCase().replace(/[-\{\}:]/g, "");
    var d = new Date();
    return d.getTime();
}
var IS_PATHBASEDURLS = true;
var ORG_UNIQUE_NAME = "";
function appendOrgName(sUrl)
{
    var sNewUrl = sUrl;	
    if (IS_PATHBASEDURLS) 
    {
        if(sUrl.substring(sUrl.length - 1) == "/")
        {
            sNewUrl = sUrl + ORG_UNIQUE_NAME;
        }	
        else
        {
            sNewUrl = sUrl + "/" + ORG_UNIQUE_NAME;
        }
    }
    return sNewUrl;
}
function prependOrgName(sUrl)
{	
    var sNewUrl = sUrl;	
    if (IS_PATHBASEDURLS && ORG_UNIQUE_NAME.length > 0) 
    {
        sNewUrl = "/" + ORG_UNIQUE_NAME + sUrl;
    }
    return sNewUrl;
}
function stripOrgName(sUrl)
{
    stripOrgNameWithForce(sUrl, false);
}
function stripOrgNameWithForce(sUrl, bForce)
{
    var sNewUrl = sUrl ;
    if (IS_PATHBASEDURLS || bForce && ORG_UNIQUE_NAME.length > 0)
    {
        if (sNewUrl.substr(0, 1) != "/")
        {
            sNewUrl = "/" + sNewUrl ;
        }

        sNewUrl = sNewUrl.replace("/" + ORG_UNIQUE_NAME,"");
    }

    return sNewUrl ;
}
function handlePopupBlockerError()
{
    alert(LOCID_POPUP_BLOCKER_ERROR +  window.location.hostname);
}
var LOCID_POPUP_BLOCKER_ERROR = "";

function openStdWin(sPath, sName, iX, iY, sCustomWinFeatures) {
    iX = getAdjustedWidthForScreen(iX);
    iY = getAdjustedHeightForScreen(iY);
    // Get the left and top margin.
    var iLeft = getLeftMargin(iX);
    var iTop = getTopMargin(iY);
    var sWinDir = "left";
    if (LOCID_UI_DIR == "RTL")
    sWinDir = "right";
    return safeWindowOpen(sPath, sName, "width=" + iX + ",height=" + iY + ",status=1,resizable=1," + sWinDir + "=" + iLeft + ",top=" + iTop + (IsNull(sCustomWinFeatures) ? "" : "," + sCustomWinFeatures));
}
// The following functions do try/catch, and display the popup blocker error message as an alert when the window fails to open.
function safeWindowOpen(sUrl, sName, sFeatures, bReplace) {
    var windowOpened = null;
    try
    {
        windowOpened = window.open(sUrl, sName, sFeatures, bReplace);
    }
    catch (e)
    {
    }
    if (IsNull(windowOpened))
    {
        handlePopupBlockerError();
    }
    return windowOpened;
}

function findObj(theObj, theDoc) {
    var p, i, foundObj;
    if (!theDoc) {
        theDoc = document;
    }
    if ((p = theObj.indexOf("?")) > 0 && parent.frames.length) {
        theDoc = parent.frames[theObj.substring(p + 1)].document;
        theObj = theObj.substring(0, p);
    }
    if (!(foundObj = theDoc[theObj]) && theDoc.all) {
        foundObj = theDoc.all[theObj];
    }
    for (i = 0; !foundObj && i < theDoc.forms.length; i++) {
        foundObj = theDoc.forms[i][theObj];
    }
    for (i = 0; !foundObj && theDoc.layers && i < theDoc.layers.length; i++) {
        foundObj = findObj(theObj, theDoc.layers[i].document);
    }
    if (!foundObj && document.getElementById) {
        foundObj = document.getElementById(theObj);
    }
    return foundObj;
}
function RemoveAllRows(tableObjectId) {
    var tableObject = findObj(tableObjectId, document);
    var rowscount = tableObject.rows.length;
    for (i = rowscount - 1; i >= 0; i--) {
        tableObject.deleteRow(i);
    }
}
function CardUtils_GetSex(val) {
    var sexCode;
    //15 bit ID Card
    if (15 == val.length) {
        if (parseInt(val.charAt(14) / 2) * 2 != val.charAt(14))
            sexCode = "0";
        else
            sexCode = "1";
    }
    //18 bit ID Card
    if (18 == val.length) {
        if (parseInt(val.charAt(16) / 2) * 2 != val.charAt(16))
            sexCode = "0";
        else
            sexCode = "1";
    }

    return sexCode;
}

function GetBrowserType() {
    var Sys = {};
    var ua = navigator.userAgent.toLowerCase();
    var s;
    (s = ua.match(/msie ([\d.]+)/)) ? Sys.ie = s[1] :
    (s = ua.match(/firefox\/([\d.]+)/)) ? Sys.firefox = s[1] :
    (s = ua.match(/chrome\/([\d.]+)/)) ? Sys.chrome = s[1] :
    (s = ua.match(/opera.([\d.]+)/)) ? Sys.opera = s[1] :
    (s = ua.match(/version\/([\d.]+).*safari/)) ? Sys.safari = s[1] : 0;
    if (Sys.ie) {// IE
        return "ie";
    }
    if (Sys.firefox) {// firefox
        return "firefox";
    }
    if (Sys.chrome) {// chrome
        return "chrome";
    }
    if (Sys.opera) {// opera
        return "opera";
    }
    if (Sys.safari) {// safari
        return "safari";
    }
}
function copyToClipboard(obj) {
    obj.select();
}
function formatPhoneNumber(_obj) {
    switch (_obj.value.length) {
        case 11:
            $("#" + _obj.id).val(_obj.value.replace(/(\d)(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1-$2-$3-$4'));
            break;
        default:
            $("#" + _obj.id).val(_obj.value.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1-$2-$3'));
            break;
    }
}

function GetMaxRowId(tableObjectId) {
    var tableObject = findObj(tableObjectId, document);
    var temp = 0, max = 0;
    for (i = 0; i < tableObject.rows.length; i++) {

        temp = parseInt(tableObject.rows[i].id.split("_")[1]);

        if (temp > max)
            max = temp;
    }

    return max + 1;
}
function TabInit() {
    $(".dijitTab").each(function () {
        $(this).bind("click", function () {
            $(".dijitTab").each(function () {
                $(this).removeClass("dijitTabChecked dijitChecked dijitTabHover");
                $("#" + $(this).attr("widgetid")).css("display", "none");
            });
            $(this).addClass("dijitTabChecked dijitChecked dijitTabHover");
            $("#" + $(this).attr("widgetid")).css("display", "");
        });
    });
}
function tab_switch(obj) {
    if (obj.parentElement.className != "currentBtn") {
        $("#" + obj.id).parent().parent().children().removeClass("currentBtn");
        $("#" + obj.id).parent().addClass("currentBtn");

        var $p = $("#" + obj.id).parent().parent().children();
        for (var i = 0; i < $p.length; i++) {
            if ($p[i].firstChild.rel == obj.rel)
                $("#tab_" + $p[i].firstChild.rel).css("display", "");
            else
                $("#tab_" + $p[i].firstChild.rel).css("display", "none");
        }
    }
}
String.prototype.format = function (args) {
    var result = this;
    if (arguments.length > 0) {
        if (arguments.length == 1 && typeof (args) == "object") {
            for (var key in args) {
                if (args[key] != undefined) {
                    var reg = new RegExp("({" + key + "})", "g");
                    result = result.replace(reg, args[key]);
                }
            }
        }
        else {
            for (var i = 0; i < arguments.length; i++) {
                if (arguments[i] != undefined) {
                    var reg = new RegExp("({[" + i + "]})", "g");
                    result = result.replace(reg, arguments[i]);
                }
            }
        }
    }
    return result;
}