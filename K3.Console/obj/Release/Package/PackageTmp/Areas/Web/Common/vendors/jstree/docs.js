/* global jQuery */
(function ($) {
    "use strict";
    $(function () {
        var search = function (str) {
            var tmp = str && str.length ? $.vakata.search(str, true, { threshold: 0.2, fuzzy: true, caseSensitive: false }) : null,
					res = $('#api_inner')
								.find('.item').hide()
								.filter(function () {
								    return tmp ? tmp.search($(this).find('>h4>code').text()).isMatch : true;
								}).show().length;
            if (!$('#srch').is(':focus')) {
                $('#srch').val(str);
            }
            $(window).resize();
        },
			filter = function (str) {
			    $('.item-inner').hide();
			    if (str) {
			        var i = $('.item[rel="' + str + '"]');
			        if (!i.length) { i = $('.item[rel^="' + str + '"]'); }
			        if (i && i.length) {
			            i = i.eq(0);
			            i.children('.item-inner').show().end();
			            if (i.offset().top < $(document).scrollTop() || i.offset().top + i.height() > $(document).scrollTop() + $(window).height()) {
			                i[0].scrollIntoView();
			            }
			        }
			    }
			};

        var to1 = false;
        $(window).resize(function () {
            if (to1) { clearTimeout(to1); }
            to1 = setTimeout(function () {
                $('.page').css('minHeight', '0px').css('minHeight', ($(document).height() - $('#head').outerHeight()) + 'px');
            }, 50);
        });

        $('.tab-content').children().hide().eq(0).show();
        $('.nav a').on('click', function () { $(this).blur(); });

        $.address
			.init(function (e) {
			    $('a:not([href^=http])').not($('.demo a')).address().on('click', function () { if ($.address.pathNames().length < 2 && !$.address.parameter('f')) { $(document).scrollTop(0); } });
			})
			.change(function (e) {
			    var page, elem, cont, srch;
			    if (!e.pathNames.length || !$('#content').children('#' + e.pathNames[0]).length) {
			        $('#menu a').eq(0).click();
			        return;
			    }
			    page = e.pathNames[0];

			    $('#menu').find('a[href$="' + page + '"]').blur().parent().addClass('active').siblings().removeClass('active');
			    cont = $('#content').children('#' + page).show().siblings().hide().end();

			    $('#srch').val('');
			    cont.find('.item').show();
			    elem = e.pathNames[1] ? cont.find('#' + e.pathNames[1]) : [];
			    if (elem.length) {
			        if (elem.hasClass('tab-content-item')) {
			            elem.siblings().hide().end().show().parent().prev().children().removeClass('active').eq(elem.index()).addClass('active');
			        }
			        else {
			            elem[0].scrollIntoView();
			        }
			    }

			    $(window).resize();
			});

        var to2 = false;
        $('#srch').on('keyup', function () {
            if (to2) { clearTimeout(to2); }
            to2 = setTimeout(function () {
                var f = $.address.parameter('f'),
					q = $('#srch').val(),
					d = [];
                if (q && q.length) {
                    d.push('q=' + q);
                }
                if (f && f.length && false) {
                    d.push('f=' + f);
                }
                $.address.value('/api/' + (d.length ? '?' + d.join('&') : ''));
            }, 250);
        });
    });
} (jQuery));