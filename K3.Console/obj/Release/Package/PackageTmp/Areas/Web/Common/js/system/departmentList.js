﻿
var observeId;

function RefreshTree() {
    $('#jstree_catalog').jstree("refresh");
}
function modify() {
    var ref = $('#jstree_catalog').jstree(true),
    sel = ref.get_selected();
    if (!sel.length) { return false; }
    sel = sel[0];
    observeId = sel;
    if (sel) {
        update(sel);
    }
    else {
        alert("Please select a node!");
    }
};
function goOnRemove() {
    if (!confirm('请确认是否要执行操作？')) return false;
    var param = {};
    param.id = observeId;

    $.post("/api/systemDept/delete", param, function (rel) {

        if (rel) {
            if (rel.isSuccess) {
                alert("操作成功");
                RefreshTree();
            } else {
                alert(rel.errorMessage)
            }
        } else {
            alert("unknown error!")
        }


    }).fail(function (ex) { });
}

$(".wrapper-tree").niceScroll({ cursorcolor: "#ccc", horizrailenabled: "false" });

$(function () {
    var to = false;
    loadMenu();
    $.ajax({
        url: "/api/systemDept/deptTree",
        type: "GET",
        success: function (result) {
            $('#jstree_catalog').jstree({
                "core": {
                    "animation": 0,
                    "check_callback": true,
                    "themes": {
                        "name": "default",
                        "dots": true,
                        "icons": true
                    },
                    'data': result
                },
                'contextmenu': {
                    items: {
                        "ccp": false
                    }
                },
                "types": {
                    "#": { "max_children": 1, "max_depth": 4, "valid_children": ["root"] },
                    "root": { "icon": "ion-social-buffer-outline", "valid_children": ["default"] },
                    "default": { "valid_children": ["default", "file"] },
                    "file": { "icon": "glyphicon glyphicon-map-marker", "valid_children": [] }
                },
                "plugins": ["contextmenu", "dnd", "search", "state", "types", "wholerow"]
            }).on('activate_node.jstree', function (event, data) {
                $("#SettingModal").modal('show');
                observeId = data.node.id;
                clearValues();
                loadShow();
                $(".depHidd").show();
                showMenuMapp();
              
            });
        }
    });
    $(".deptSelect").autocomplete({
        source: function (request, response) {
            var textV = request.term.toLowerCase();

            var result = deptOptions.filter(function (item) {
                return item.deptName.toLowerCase().indexOf(textV) >= 0;
            });
            response(result.map(function (item) {
                return {
                    label: item.deptName,
                    value: item.deptName,
                    entity: item,
                };
            }));
        },
        autoFocus: true,
        change: function (event, ui) {
            if ($(event.target).val() == "") {
                $(event.target).attr("data-deptid", "");
                return;
            }
            if (ui.item) {
                for (var i = 0; i < deptOptions.length; i++) {
                    var item = deptOptions[i];

                }
            }
        },
        select: function (event, ui) {
            $(event.target).attr("data-deptid", ui.item.entity.id);
        },
        close: function (event, ui) {
            if ($(event.target).val() == "") {
                $(event.target).attr("data-deptid", "");
                return;
            }
            if (!$(event.target).attr("data-deptid")) {
                $(event.target).attr("data-deptid", "").attr("data-deptid", "").val("");
                return;
            } else {
                for (var i = 0; i < deptOptions.length; i++) {
                    var item = deptOptions[i];
                    if (item.id == $(event.target).attr("data-deptid")) {
                        $(event.target).val(item.deptName);
                        break;
                    }
                }
            }
        },
        minLength: 0
    });
    $("#add").on("click", function () {
        location.href="/Web/System/DepartmentEditor";
    });
});
var selectedId = null;
function addNew() {
    var url = "/api/systemDept/add";
    var param = {};
    param.DeptName = $("#dept_name").val();
    param.DeptNumber = $("#dept_no").val();
    param.DeptParentID = $("#dept_parent").attr("data-deptid");
    param.DeptGroupStID = $("#dept_gno").val();
    param.DeptGroupStName = $("#dept_gname").val();
    param.DeptOperManID = $("#dept_mno").val();
    param.DeptOperManName = $("#dept_mname").val();
    param.DeptEmail = $("#dept_email").val();
    param.DeptTelePhone = $("#dept_name").val();
    param.DeptFax = $("#dept_fax").val();
    //param.DeptCoID = $("#dept_name").val();
    param.DeptType = $("#dept_type").val();
    param.DeptNote = "";
    param.DeptTreeCode = "";
    param.DeptSortNo = "";
    param.DeptState = "";
    param.DeptIsShow = "";
    param.DeptClientIP = "";
    if (observeId != null) {
        url = "/api/systemDept/update";
        param.id = observeId;
    }
    $.post(url, param, function (rel) {
        if (rel) {
            if (rel.isSuccess) {
                alert("success");
                clearValues();
                RefreshTree();
                $("#SettingModal").modal('hide');
            } else {
                alert(rel.errorMessage)
            }
        } else {
            alert("unknown error!")
        }

    }).done(function (data) { location.reload(); })
        .fail(function (ex) { });
}
function loadMenu()
{
    $.ajax({
        url: "/api/systemMenu/catalogTree",
        type: "GET",
        success: function (result) {
            $('#catalog_tree').jstree({
                "core": {
                    "animation": 0,
                    "check_callback": true,
                    "themes": {
                        "name": "default",
                        "dots": true,
                        "icons": true
                    },
                    'data': result,
                },
                'contextmenu': {
                    items: {
                        "ccp": false
                    }
                },
                "types": {
                    "#": { "max_children": 1, "max_depth": 4, "valid_children": ["root"] },
                    "root": { "icon": "ion-social-buffer-outline", "valid_children": ["default"] },
                    "default": { "valid_children": ["default", "file"] },
                    "file": { "icon": "glyphicon glyphicon-map-marker", "valid_children": [] }
                },
                "checkbox": {
                    "keep_selected_style": true
                },

                "plugins": ["contextmenu", "dnd", "search", "state", "types", "wholerow", "checkbox"]
            });
        }
    });
}
function loadShow()
{
    $(".modfiy").html('编辑');
    var url = "/api/systemDept/info/"+observeId;
    $.get(url, null, function (rel) {
        if (rel) {
            if (rel.isSuccess) {
                if (rel.entity) {
                    if (rel.entity.deptParent != null)
                    {
                        $("#dept_parent").val(rel.entity.deptParent.deptName);
                        $("#dept_parent").attr("data-deptid", rel.entity.deptParent.id);
                    }                   
                    $("#dept_name").val(rel.entity.deptName);
                    $("#dept_no").val(rel.entity.deptNumber);
                    $("#dept_gno").val(rel.entity.deptGroupStID);
                    $("#dept_gname").val(rel.entity.deptGroupStName);
                    $("#dept_mname").val(rel.entity.deptOperManName);
                    $("#dept_email").val(rel.entity.deptEmail);
                    $("#dept_fax").val(rel.entity.deptFax);
                    $("#dept_type").val(rel.entity.deptType);
                }
            } else {
                alert(rel.errorMessage)
            }
        } else {
            alert("unknown error!")
        }

    }).done(function (data) { })
        .fail(function (ex) { });
}
function saveDeptMenu()
{
    var url = "/api/systemMenu/menuExtendMapping";
    var param = {};
    var catalogs = $("#catalog_tree").jstree().get_checked(0, true);
    if (catalogs == null || catalogs == '')
        catalogs = [];
    param.MenuIds = catalogs;
    param.ObjectId = observeId;
    param.MenuMapingType = 1;
   
    $.post(url, param, function (rel) {

        if (rel) {
            if (rel.isSuccess) {

                alert("success");
            } else {
                alert(rel.errorMessage)
            }
        } else {
            alert("unknown error!")
        }

    }).done(function (data) { location.reload(); })
        .fail(function (ex) { });
}
function showMenuMapp() {
    var jstree = $("#catalog_tree").jstree();
    jstree.uncheck_all();//清空
    //$("#catalog_tree").jstree("uncheck_all");//清空
    $.ajax({
        url: "/api/systemMenu/getExtendMenuMapping/" + observeId + "/1",
        type: "GET",
       
        success: function (result) {
            function expandNode(node) {
                if (!jstree.is_open(node)) {
                    jstree.open_node(node);
                    if (node.parents.length) {
                        node.parents.map(function (id) {
                            var n = jstree.get_node(id);
                            expandNode(n);
                        });
                    }
                }
            }
            result.map(function (id) {
                expandNode(jstree.get_node(id));
            });
            var regionsIds = result;
            for (var i = 0; i < result.length; i++) {
                jstree.check_node(result[i] + '_anchor');
                
            }
        }
    });
}
function addShow()
{
    clearValues();
    $(".modfiy").html("新增");
    $("#SettingModal").modal('show');
    $(".depHidd").hide();
    $(".mapp").hide();
    observeId = null;
}
function clearValues() {
   
    $("#dept_parent").val('');
    $("#dept_name").val('');
    $("#dept_no").val('');
    $("#dept_gno").val('');
    $("#dept_gname").val('');
    $("#dept_mname").val('');
    $("#dept_email").val('');
    $("#dept_fax").val('');
    $("#dept_type").val('');
}
//close the parent nav menu in the index when click in all window
function closeMenu() {
    parent.closeMenu();
}