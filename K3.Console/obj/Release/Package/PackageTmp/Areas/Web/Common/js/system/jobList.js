﻿
var observeId;

function RefreshTree() {
    $('#jstree_catalog').jstree("refresh");
}
function modify() {
    var ref = $('#jstree_catalog').jstree(true),
    sel = ref.get_selected();
    if (!sel.length) { return false; }
    sel = sel[0];
    observeId = sel;
    if (sel) {
        update(sel);
    }
    else {
        alert("Please select a node!");
    }
};
function goOnRemove() {
    if (!confirm('请确认是否要执行操作？')) return false;
    var param = {};
    param.id = observeId;

    $.post("/api/systemDept/delete", param, function (rel) {

        if (rel) {
            if (rel.isSuccess) {
                alert("操作成功");
                RefreshTree();
            } else {
                alert(rel.errorMessage)
            }
        } else {
            alert("unknown error!")
        }


    }).fail(function (ex) { });
}

$(".wrapper-tree").niceScroll({ cursorcolor: "#ccc", horizrailenabled: "false" });

$(function () {
    var to = false;
    loadMenu();
    $.ajax({
        url: "/api/systemJob/jobTree",
        type: "GET",
        success: function (result) {
            $('#jstree_catalog').jstree({
                "core": {
                    "animation": 0,
                    "check_callback": true,
                    "themes": {
                        "name": "default",
                        "dots": true,
                        "icons": true
                    },
                    'data': result
                },
                'contextmenu': {
                    items: {
                        "ccp": false
                    }
                },
                "types": {
                    "#": { "max_children": 1, "max_depth": 4, "valid_children": ["root"] },
                    "root": { "icon": "ion-social-buffer-outline", "valid_children": ["default"] },
                    "default": { "valid_children": ["default", "file"] },
                    "file": { "icon": "glyphicon glyphicon-map-marker", "valid_children": [] }
                },
                "plugins": ["contextmenu", "dnd", "search", "state", "types", "wholerow"]
            }).on('activate_node.jstree', function (event, data) {
                $("#SettingModal").modal('show');
                observeId = data.node.id;
                clearValues();
                loadShow();
                $(".depHidd").show();
                showMenuMapp();

            });
        }
    });
    $(".deptSelect").autocomplete({
        source: function (request, response) {
            var textV = request.term.toLowerCase();

            var result = deptOptions.filter(function (item) {
                return item.deptName.toLowerCase().indexOf(textV) >= 0;
            });
            response(result.map(function (item) {
                return {
                    label: item.deptName,
                    value: item.deptName,
                    entity: item,
                };
            }));
        },
        autoFocus: true,
        change: function (event, ui) {
            if ($(event.target).val() == "") {
                $(event.target).attr("data-deptid", "");
                return;
            }
            if (ui.item) {
                for (var i = 0; i < deptOptions.length; i++) {
                    var item = deptOptions[i];

                }
            }
        },
        select: function (event, ui) {
            $(event.target).attr("data-deptid", ui.item.entity.id);
        },
        close: function (event, ui) {
            if ($(event.target).val() == "") {
                $(event.target).attr("data-deptid", "");
                return;
            }
            if (!$(event.target).attr("data-deptid")) {
                $(event.target).attr("data-deptid", "").attr("data-deptid", "").val("");
                return;
            } else {
                for (var i = 0; i < deptOptions.length; i++) {
                    var item = deptOptions[i];
                    if (item.id == $(event.target).attr("data-deptid")) {
                        $(event.target).val(item.deptName);
                        break;
                    }
                }
            }
        },
        minLength: 0
    });
    $(".companySelect").autocomplete({
        source: function (request, response) {
            var textV = request.term.toLowerCase();

            var result = companyOptions.filter(function (item) {
                return item.companyName.toLowerCase().indexOf(textV) >= 0;
            });
            response(result.map(function (item) {
                return {
                    label: item.companyName,
                    value: item.companyName,
                    entity: item,
                };
            }));
        },
        autoFocus: true,
        change: function (event, ui) {
            if ($(event.target).val() == "") {
                $(event.target).attr("data-companyid", "");
                return;
            }
            if (ui.item) {
                for (var i = 0; i < companyOptions.length; i++) {
                    var item = companyOptions[i];

                }
            }
        },
        select: function (event, ui) {
            $(event.target).attr("data-companyid", ui.item.entity.id);
        },
        close: function (event, ui) {
            if ($(event.target).val() == "") {
                $(event.target).attr("data-companyid", "");
                return;
            }
            if (!$(event.target).attr("data-companyid")) {
                $(event.target).attr("data-companyid", "").attr("data-companyid", "").val("");
                return;
            } else {
                for (var i = 0; i < companyOptions.length; i++) {
                    var item = companyOptions[i];
                    if (item.id == $(event.target).attr("data-companyid")) {
                        $(event.target).val(item.companyName);
                        break;
                    }
                }
            }
        },
        minLength: 0
    });
    $(".jobSelect").autocomplete({
        source: function (request, response) {
            var textV = request.term.toLowerCase();

            var result = jobOptions.filter(function (item) {
                return item.jobName.toLowerCase().indexOf(textV) >= 0;
            });
            response(result.map(function (item) {
                return {
                    label: item.jobName,
                    value: item.jobName,
                    entity: item,
                };
            }));
        },
        autoFocus: true,
        change: function (event, ui) {
            if ($(event.target).val() == "") {
                $(event.target).attr("data-jobid", "");
                return;
            }
            if (ui.item) {
                for (var i = 0; i < jobOptions.length; i++) {
                    var item = jobOptions[i];

                }
            }
        },
        select: function (event, ui) {
            $(event.target).attr("data-jobid", ui.item.entity.id);
        },
        close: function (event, ui) {
            if ($(event.target).val() == "") {
                $(event.target).attr("data-jobid", "");
                return;
            }
            if (!$(event.target).attr("data-jobid")) {
                $(event.target).attr("data-jobid", "").attr("data-jobid", "").val("");
                return;
            } else {
                for (var i = 0; i < jobOptions.length; i++) {
                    var item = jobOptions[i];
                    if (item.id == $(event.target).attr("data-jobid")) {
                        $(event.target).val(item.jobName);
                        break;
                    }
                }
            }
        },
        minLength: 0
    });
    $("#add").on("click", function () {
        location.href = "/Web/System/DepartmentEditor";
    });
});

var selectedId = null;



function addNew() {
    var url = "/api/systemJob/add";
    var param = {};
    param.JobName = $("#txtJobName").val();
    //param.JobNumber = $("#dept_no").val();
    param.JobParentID = $("#txtParent").attr("data-jobid");
    param.JobDeptID = $("#txtDept").attr("data-deptid");
    param.JobCoID = $("#txtDept").attr("data-companyid");
    //param.JobLevel = $("#dept_mno").val();
    //param.JobType = $("#dept_mname").val();
    //param.JobNote = $("#dept_email").val();
    //param.JobTreeCode = $("#dept_name").val();
    //param.JobSortNo = $("#dept_fax").val();
    //param.JobState = $("#dept_type").val();
    //param.JobtIsShow = "";
    param.JobClientIP = "";

    if (observeId != null) {
        url = "/api/systemJob/update";
        param.id = observeId;
    }
    $.post(url, param, function (rel) {
        if (rel) {
            if (rel.isSuccess) {
                alert("success");
                clearValues();
            } else {
                alert(rel.errorMessage)
            }
        } else {
            alert("unknown error!")
        }

    }).done(function (data) { location.reload(); })
        .fail(function (ex) { });
}

function loadMenu() {
    $.ajax({
        url: "/api/systemMenu/catalogTree",
        type: "GET",
        success: function (result) {
            $('#catalog_tree').jstree({
                "core": {
                    "animation": 0,
                    "check_callback": true,
                    "themes": {
                        "name": "default",
                        "dots": true,
                        "icons": true
                    },
                    'data': result,
                },
                'contextmenu': {
                    items: {
                        "ccp": false
                    }
                },
                "types": {
                    "#": { "max_children": 1, "max_depth": 4, "valid_children": ["root"] },
                    "root": { "icon": "ion-social-buffer-outline", "valid_children": ["default"] },
                    "default": { "valid_children": ["default", "file"] },
                    "file": { "icon": "glyphicon glyphicon-map-marker", "valid_children": [] }
                },
                "checkbox": {
                    "keep_selected_style": true
                },

                "plugins": ["contextmenu", "dnd", "search", "state", "types", "wholerow", "checkbox"]
            });
        }
    });
}
function loadShow() {
    $(".modfiy").html('编辑');

    var url = "/api/systemJob/info/" + observeId;
    $.get(url, null, function (rel) {
        if (rel) {
            if (rel.isSuccess) {
                if (rel.entity) {
                    if (rel.entity.deptParent != null) {
                        $("#txtParent").val(rel.entity.jobParent.JobParaentName);
                        $("#txtParent").attr("data-jobid", rel.entity.id);
                    }

                    $("#txtJobName").val(rel.entity.jobName);
              

                    $("#txtDept").val(rel.entity.deptName);
                    $("#txtDept").attr("data-deptid", rel.entity.jobDeptID);

                    $("#txtCompany").val(rel.entity.coName);
                    $("#txtCompany").attr("data-companyid", rel.entity.jobCoID);
                }
            } else {
                alert(rel.errorMessage)
            }
        } else {
            alert("unknown error!")
        }

    }).done(function (data) { })
        .fail(function (ex) { });

}
function saveDeptMenu() {
    var url = "/api/systemMenu/menuExtendMapping";
    var param = {};
    var catalogs = $("#catalog_tree").jstree().get_checked(0, true);
    if (catalogs == null || catalogs == '')
        catalogs = [];
    param.MenuIds = catalogs;
    param.ObjectId = observeId;
    param.MenuMapingType = 1;

    $.post(url, param, function (rel) {

        if (rel) {
            if (rel.isSuccess) {

                alert("success");
            } else {
                alert(rel.errorMessage)
            }
        } else {
            alert("unknown error!")
        }

    }).done(function (data) { location.reload(); })
        .fail(function (ex) { });
}
function showMenuMapp() {
    var jstree = $("#catalog_tree").jstree();
    jstree.uncheck_all();//清空
    //$("#catalog_tree").jstree("uncheck_all");//清空
    $.ajax({
        url: "/api/systemMenu/getExtendMenuMapping/" + observeId + "/1",
        type: "GET",

        success: function (result) {
            function expandNode(node) {
                if (!jstree.is_open(node)) {
                    jstree.open_node(node);
                    if (node.parents.length) {
                        node.parents.map(function (id) {
                            var n = jstree.get_node(id);
                            expandNode(n);
                        });
                    }
                }
            }
            result.map(function (id) {
                expandNode(jstree.get_node(id));
            });
            var regionsIds = result;
            for (var i = 0; i < result.length; i++) {
                jstree.check_node(result[i] + '_anchor');

            }
        }
    });
}
function addShow() {
    $(".modfiy").html("新增");
    $("#SettingModal").modal('show');
    $(".depHidd").hide();
    $(".mapp").hide();
    clearValues();
}
function clearValues() {
    observeId = null;
    $("#txtParent").val('');
    $("#txtJobName").val('');
    $("#txtDept").val('');
    $("#txtCompany").val('');

}
//close the parent nav menu in the index when click in all window
function closeMenu() {
    parent.closeMenu();
}