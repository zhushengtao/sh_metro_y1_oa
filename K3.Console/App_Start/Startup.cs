﻿using Microsoft.Owin;
using Owin;
using GeneralFramework.EntityFreamework;
using GeneralFramework.DependencyResolution;
using GeneralFramework.Infrastructure.Formatting;
using System.Web.Http;
using System.Web.Mvc;
using StructureMap;
using StructureMap.Graph;
using StructureMap.Graph.Scanning;
using StructureMap.Pipeline;
using StructureMap.TypeRules;
using StructureMap.Web;
using Microsoft.Owin.Cors;
[assembly: OwinStartup(typeof(K3.Console.Startup))]
namespace K3.Console
{
    public partial class Startup
    {
        public static HttpConfiguration WebApiConfig { get; private set; }
        public void Configuration(IAppBuilder app)
        {

            IContainer container = GetContainer();
            var dependencyResolver = new StructureMapDependencyResolver(container);
            DependencyResolver.SetResolver(dependencyResolver);
            GlobalConfiguration.Configuration.DependencyResolver = new StructureMapDependencyResolver(container);

            ConfigureAuth(app);
            ConfigureWebApi(app, dependencyResolver);
        }



        public static void ConfigureWebApi(IAppBuilder app, System.Web.Http.Dependencies.IDependencyResolver dependencyResolver)
        {
            WebApiConfig = new HttpConfiguration();
            WebApiConfig.MapHttpAttributeRoutes();
            FilterConfig.RegisterWebApiFilters(WebApiConfig.Filters);
            JsonFormatterConfig.ConfigureFormatter(WebApiConfig);
            WebApiConfig.DependencyResolver = dependencyResolver;
            WebApiConfig.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            app.UseWebApi(WebApiConfig);
        }

      

        private static IContainer GetContainer()
        {
            var registry = new Registry();
            registry.IncludeRegistry<DefaultRegistry>();
            return new Container(registry);
        }
    }
    public class DefaultRegistry : Registry
    {
        #region Constructors and Destructors

        public DefaultRegistry()
        {
            Scan(
                scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                    scan.Convention<ControllerConvention>();
                });

            For<GeneralContext>().HybridHttpOrThreadLocalScoped().Use<GeneralContext>();
        }

        #endregion
    }
    public class ControllerConvention : IRegistrationConvention
    {
        #region Public Methods and Operators

        public void ScanTypes(TypeSet typeSets, Registry registry)
        {
            var types = typeSets.FindTypes(TypeClassification.Concretes | TypeClassification.Closed);
            foreach (var type in types)
            {
                if (type.CanBeCastTo<Controller>() && !type.IsAbstract)
                {
                    registry.For(type).LifecycleIs(new TransientLifecycle());
                }
            }
        }

        #endregion

    }
}