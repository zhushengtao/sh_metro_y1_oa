﻿using Owin;
using System;
using System.Security.Claims;
using Microsoft.Owin.Security.DataHandler;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using GeneralFramework.Security;
using Microsoft.Owin.Extensions;

[assembly: OwinStartup(typeof(K3.Console.Startup))]
namespace K3.Console
{
    public partial class Startup
    {
        public void ConfigureAuth(IAppBuilder app)
        {
            var Options = new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                AuthenticationMode = AuthenticationMode.Active,
                LoginPath = new PathString("/UnAuthorized"),
                CookieHttpOnly = true,
                CookieName = string.Format(".General_AUTH_{0}", "2020.05.20.001".Replace(".", "_")),
                CookieSecure = CookieSecureOption.SameAsRequest,
                ExpireTimeSpan = TimeSpan.FromDays(0.5),
            };
            Options.TicketDataFormat = new TicketDataFormat(new CustomDataProtector());

            // 配置Middleware 組件
            app.UseCookieAuthentication(Options);

            app.Use((context, next) =>
            {
                try
                {
                    if ((context.Request.User != null) && !(context.Request.User.Identity is GeneralIdentity) && context.Request.User.Identity.IsAuthenticated)
                    {
                        GeneralIdentity identity = GeneralIdentity.CreateFromClaimsIdentity((ClaimsIdentity)context.Request.User.Identity);
                        GeneralPrincipal principal = new GeneralPrincipal(identity);
                        context.Request.User = principal;
                    }
                }
                catch (Exception e)
                {
                    //to do 异常处理
                }
                return next.Invoke();
            });

            app.UseStageMarker(PipelineStage.PostAuthenticate);
        }
    }
    public class CustomDataProtector : IDataProtector
    {
        public byte[] Protect(byte[] userData)
        {
            var h = new CryptoHelper();
            h.StrongInitialize();
            var a = h.Encrypt(Convert.ToBase64String(userData));
            return Convert.FromBase64String(a);
        }

        public byte[] Unprotect(byte[] protectedData)
        {
            var h = new CryptoHelper();
            h.StrongInitialize();
            var a = h.Decrypt(Convert.ToBase64String(protectedData));
            return Convert.FromBase64String(a);
        }
    }
}
