﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.SessionState;

namespace K3.Console
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            try
            {
                //AliyunONSHelper.StartProducer();
            }
            catch (Exception e)
            {
                //ignore
            }
        }

        protected void Application_End(object sender, EventArgs e)
        {
            try
            {
                //AliyunONSHelper.ShutdownProducer();
            }
            catch (Exception ex)
            {
                // 处理异常
            }
        }

        protected void Application_PostAuthorizeRequest()
        {
            HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
        }

        protected void Application_EndRequest()
        {
            var statusCode = Context.Response.StatusCode;
            if (statusCode == 404)
            {
                Response.Clear();
                //Response.RedirectToRoute("Default", new { controller = "Home", action = "ResourceNotFound", id = UrlParameter.Optional });
                Response.Redirect("/ResourceNotFound");
            }
        }
    }
}
