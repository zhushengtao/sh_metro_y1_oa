﻿
var observeId;
function add() {
    observeId = null;
    clearValues();
}

function RefreshTree() {
    $('#jstree_catalog').jstree("refresh");
}
function modify() {


    var treeNode = $('#jstree_catalog').jstree(true).get_selected(true);


    var ref = $('#jstree_catalog').jstree(true),
    sel = ref.get_selected();
    if (!sel.length) { return false; }
    sel = sel[0];
    observeId = sel;
    if (sel) {
        update(sel);
    }
    else {
        alert("Please select a node!");
    }
};

$(".wrapper-tree").niceScroll({ cursorcolor: "#ccc", horizrailenabled: "false" });

$(function () {
    var to = false;

    $.ajax({
        url: "/api/systemDept/deptTree",
        type: "GET",
        success: function (result) {
            $('#dept_tree').jstree({
                "core": {
                    "animation": 0,
                    "check_callback": true,
                    "themes": {
                        "name": "default",
                        "dots": true,
                        "icons": true
                    },
                    'data': result
                },
                'contextmenu': {
                    items: {
                        "ccp": false
                    }
                },
                "types": {
                    "#": { "max_children": 1, "max_depth": 4, "valid_children": ["root"] },
                    "root": { "icon": "ion-social-buffer-outline", "valid_children": ["default"] },
                    "default": { "valid_children": ["default", "file"] },
                    "file": { "icon": "glyphicon glyphicon-map-marker", "valid_children": [] }
                },
                "plugins": ["contextmenu", "dnd", "search", "state", "types", "wholerow"]
            });
        }
    });
    $("#dept_parent").on("click", function () {
        $('#SettingModal').modal('show');
    });
});
function selectedParentId()
{
    var treeNode = $('#dept_tree').jstree(true).get_selected(true);

    if (!treeNode.length) { return false; }
    
    $("#hidParaentId").val(treeNode[0].id);
    $("#dept_parent").val(treeNode[0].text);
    $('#SettingModal').modal('hide');
}
var selectedId = null;

function GetCatalogList() {
    $.get("/api/catalog/catalogroot", null, function (entity) {
        if (entity.length) {
            $("#ParentId").empty();
            $("#ParentId").append('<option value="">-- Select --</option>');
            for (var i = 0; i < entity.length ; i++) {
                if (entity[i].id == selectedId)
                    $("#ParentId").append('<option value="' + entity[i].id + '" selected>' + entity[i].text + '</option>');
                else
                    $("#ParentId").append('<option value="' + entity[i].id + '">' + entity[i].text + '</option>');
            }
        }
    }).fail(function (ex) { });
}

function GetSubCatalogList(parentId) {
    $.get("/api/catalog/getchildrenbypid/" + parentId, null, function (rel) {
        if (rel) {
            if (rel.isSuccess) {
                if (rel.entity) {
                    $("#SubParentId").empty();
                    $("#SubParentId").append('<option value="">-- Select --</option>');
                    for (var i = 0; i < rel.entity.length ; i++) {
                        if (rel.entity[i].id == selectedId)
                            $("#SubParentId").append('<option value="' + rel.entity[i].id + '" selected>' + rel.entity[i].text + '</option>');
                        else
                            $("#SubParentId").append('<option value="' + rel.entity[i].id + '">' + rel.entity[i].text + '</option>');
                    }
                }
            } else {
                alert(rel.errorMessage)
            }
        } else {
            alert("unknown error!")
        }
    }).fail(function (ex) { });
}
function update(id) {

    observeId = id;

    var url = "/api/catalog/tree";

    var param = {};
    param.Id = id;

    $.post(url, param, function (rel) {
        if (rel) {
            if (rel.isSuccess) {
                if (rel.entity) {
                    $("#ParentId").val(rel.entity.parent.parentId);
                    GetSubCatalogList(rel.entity.parent.parentId);
                    $("#CatalogName").val(rel.entity.catalogName);
                    $("#InternalNumber").val(rel.entity.internalNumber);
                    $("#LanguageLabelName").val(rel.entity.languageLabelName);
                    $("#LinkUrl").val(rel.entity.linkUrl);
                    $("#Description").val(rel.entity.description);
                    $("#LineNum").val(rel.entity.lineNum);
                    $("#CatalogType").val(rel.entity.catalogType);
                    $("#IconUrl").val(rel.entity.iconUrl);
                    setTimeout(function () { $("#SubParentId").val(rel.entity.parent.id); }, 1500);
                }
            } else {
                alert(rel.errorMessage)
            }
        } else {
            alert("unknown error!")
        }

    }).done(function (data) { })
        .fail(function (ex) { });

}

function addNew() {
    var url = "/api/systemDept/add";
    var param = {};
    param.DeptName=$("#dept_name").val();
    param.DeptNumber = $("#dept_no").val();
    param.DeptParentID = $("#hidParaentId").val();
    param.DeptGroupStID = $("#dept_gno").val();
    param.DeptGroupStName = $("#dept_gname").val();
    param.DeptOperManID = $("#dept_mno").val();
    param.DeptOperManName = $("#dept_mname").val();
    param.DeptEmail = $("#dept_email").val();
    param.DeptTelePhone = $("#dept_name").val();
    param.DeptFax =$("#dept_fax").val();
    //param.DeptCoID = $("#dept_name").val();
    param.DeptType = $("#dept_type").val();
    param.DeptNote = "";
    param.DeptTreeCode =""; 
    param.DeptSortNo = "";
    param.DeptState ="";
    param.DeptIsShow = "";
    param.DeptClientIP = "";
    if (observeId != null) {
        url = "/api/systemDept/update";
        param.id = observeId;
    }

    $.post(url, param, function (rel) {

        if (rel) {
            if (rel.isSuccess) {

                alert("success");

                clearValues();

                RefreshTree();

            } else {
                alert(rel.errorMessage)
            }
        } else {
            alert("unknown error!")
        }

    }).done(function (data) { location.reload(); })
        .fail(function (ex) { });
}

function clearValues() {
    $("#CatalogName").val("");

    $("#LinkUrl").val("");

    $("#ParentId").val("");

}
//close the parent nav menu in the index when click in all window
function closeMenu() {
    parent.closeMenu();
}