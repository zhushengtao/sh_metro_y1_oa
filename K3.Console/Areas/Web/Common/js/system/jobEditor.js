﻿
var observeId;
function add() {
    observeId = null;
    clearValues();
}

$(function () {
    var to = false;
    if (getUrlParam("id") != "") {
        observeId = getUrlParam("id");
        updateShow(observeId);
    }
   
    $(".deptSelect").autocomplete({
        source: function (request, response) {
            var textV = request.term.toLowerCase();

            var result = deptOptions.filter(function (item) {
                return item.deptName.toLowerCase().indexOf(textV) >= 0;
            });
            response(result.map(function (item) {
                return {
                    label: item.deptName,
                    value: item.deptName,
                    entity: item,
                };
            }));
        },
        autoFocus: true,
        change: function (event, ui) {
            if ($(event.target).val() == "") {
                $(event.target).attr("data-deptid", "");
                return;
            }
            if (ui.item) {
                for (var i = 0; i < deptOptions.length; i++) {
                    var item = deptOptions[i];

                }
            }
        },
        select: function (event, ui) {
            $(event.target).attr("data-deptid", ui.item.entity.id);
        },
        close: function (event, ui) {
            if ($(event.target).val() == "") {
                $(event.target).attr("data-deptid", "");
                return;
            }
            if (!$(event.target).attr("data-deptid")) {
                $(event.target).attr("data-deptid", "").attr("data-deptid", "").val("");
                return;
            } else {
                for (var i = 0; i < deptOptions.length; i++) {
                    var item = deptOptions[i];
                    if (item.id == $(event.target).attr("data-deptid")) {
                        $(event.target).val(item.deptName);
                        break;
                    }
                }
            }
        },
        minLength: 0
    });
    $(".companySelect").autocomplete({
        source: function (request, response) {
            var textV = request.term.toLowerCase();

            var result = companyOptions.filter(function (item) {
                return item.companyName.toLowerCase().indexOf(textV) >= 0;
            });
            response(result.map(function (item) {
                return {
                    label: item.companyName,
                    value: item.companyName,
                    entity: item,
                };
            }));
        },
        autoFocus: true,
        change: function (event, ui) {
            if ($(event.target).val() == "") {
                $(event.target).attr("data-companyid", "");
                return;
            }
            if (ui.item) {
                for (var i = 0; i < companyOptions.length; i++) {
                    var item = companyOptions[i];

                }
            }
        },
        select: function (event, ui) {
            $(event.target).attr("data-companyid", ui.item.entity.id);
        },
        close: function (event, ui) {
            if ($(event.target).val() == "") {
                $(event.target).attr("data-companyid", "");
                return;
            }
            if (!$(event.target).attr("data-companyid")) {
                $(event.target).attr("data-companyid", "").attr("data-companyid", "").val("");
                return;
            } else {
                for (var i = 0; i < companyOptions.length; i++) {
                    var item = companyOptions[i];
                    if (item.id == $(event.target).attr("data-companyid")) {
                        $(event.target).val(item.companyName);
                        break;
                    }
                }
            }
        },
        minLength: 0
    });
    $(".jobSelect").autocomplete({
        source: function (request, response) {
            var textV = request.term.toLowerCase();

            var result = jobOptions.filter(function (item) {
                return item.jobName.toLowerCase().indexOf(textV) >= 0;
            });
            response(result.map(function (item) {
                return {
                    label: item.jobName,
                    value: item.jobName,
                    entity: item,
                };
            }));
        },
        autoFocus: true,
        change: function (event, ui) {
            if ($(event.target).val() == "") {
                $(event.target).attr("data-jobid", "");
                return;
            }
            if (ui.item) {
                for (var i = 0; i < jobOptions.length; i++) {
                    var item = jobOptions[i];

                }
            }
        },
        select: function (event, ui) {
            $(event.target).attr("data-jobid", ui.item.entity.id);
        },
        close: function (event, ui) {
            if ($(event.target).val() == "") {
                $(event.target).attr("data-jobid", "");
                return;
            }
            if (!$(event.target).attr("data-jobid")) {
                $(event.target).attr("data-jobid", "").attr("data-jobid", "").val("");
                return;
            } else {
                for (var i = 0; i < jobOptions.length; i++) {
                    var item = jobOptions[i];
                    if (item.id == $(event.target).attr("data-jobid")) {
                        $(event.target).val(item.jobName);
                        break;
                    }
                }
            }
        },
        minLength: 0
    });
});

var selectedId = null;
function updateShow(id) {

    observeId = id;
    var url = "/api/systemJob/info/"+id;
    $.get(url, function (rel) {
        if (rel) {
            if (rel.isSuccess) {
                if (rel.entity) {
                 
                    $("#txtParent").val(rel.entity.jobParaentName);
                    $("#txtJobName").val(rel.entity.jobName);
                    $("#txtDept").val(rel.entity.deptName);
                    $("#txtCompany").val(rel.entity.companyName);
                    $("#txtParent").attr("data-jobid", rel.entity.jobParentID);
                    $("#txtDept").attr("data-deptid", rel.entity.jobDeptID);
                    $("#txtCompany").attr("data-companyid", rel.entity.jobCoID);
                }
            } else {
                alert(rel.errorMessage)
            }
        } else {
            alert("unknown error!")
        }

    }).done(function (data) { })
        .fail(function (ex) { });

}

function addNew() {
    var url = "/api/systemJob/add";
    var param = {};
    param.JobName = $("#txtJobName").val();
    param.JobParentID = $("#txtParent").attr("data-jobid");
    param.JobDeptID = $("#txtDept").attr("data-deptid");
    param.JobCoID = $("#txtCompany").attr("data-companyid");
    if (observeId != null) {
        url = "/api/systemJob/update";
        param.id = observeId;
    }
    $.post(url, param, function (rel) {
        if (rel) {
            if (rel.isSuccess) {
                alert("success");
                top.location.href = "/Web/System/JobList";

            } else {
                alert(rel.errorMessage)
            }
        } else {
            alert("unknown error!")
        }
    }).done(function (data) { location.reload(); })
        .fail(function (ex) { });
}
function clearValues() {
    $("#txtParent").val("");
    $("#txtJobName").val("");
    $("#txtCompany").val("");
    $("#txtDept").val("");
}
