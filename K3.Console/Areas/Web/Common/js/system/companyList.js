﻿ 
var observeId = null;
$(function ()
{
    listObj.initLoadFun();
});
var listObj = {
    //查询条件
    filter: {
        pageSize: 15,
        pageIndex: 1
    },
    //页面初始化加载
    initLoadFun: function () {
        //加载列表
        listObj.loadDataFun();
        //查询
        $('.buttonSearch').click(function () {
            listObj.filter.pageIndex = 1;
            listObj.loadDataFun();
        });
        //添加
        $('#add').on('click', '#save_btn', function () {
            listObj.saveFun();
        });
        $("#addModal").click(function () {
            listObj.clearField();
            $("#add").modal('show');
        });
       
    },
    //分页加载列表数据
    loadDataFun: function () {
        $.post("/api/systemCompany/list", listObj.filter, function (data) {
            $('.gtco-loader').hide();
            if (data) {
                if (data.isSuccess) {
                  
                    listObj.createPaginationFun(data.pagination.pageCount);
                    var template = $("#template").html();
                    Mustache.parse(template); //
                    var rendered = Mustache.render(template, data);
                    $("#gridList").html(rendered);
                    $("#totalPagesCounter").html(data.pagination.pageCount);
                    $("#totalRecords").html(data.pagination.totalRecords);
                }
                else
                    alert(rel.errorMessage)
            } else
                alert("unknown error!")

        }).fail(function (ex) { $('.gtco-loader').hide(); });
    },
    //生成分页码
    createPaginationFun: function (totalPage) {

        $('#number-page').off().empty();
        $('#number-page').bootpag({
            total: totalPage,
            maxVisible: 10,
            page: listObj.filter.pageIndex,
            firstLastUse: true,
            next: '>',
            prev: '<',
            first: '<<',
            last: '>>'
        })
        .on("page", function (event, num) {
            listObj.filter.pageIndex = num;
            listObj.loadDataFun();
        });
        $('#number-page').find("li").removeClass("active");
        if (listObj.filter.pageIndex != 1) {
            $('#number-page').find("li[data-lp='" + listObj.filter.pageIndex + "']").first().addClass("active");
        } else {
            $('#number-page').find("li[data-lp='" + listObj.filter.pageIndex + "']").last().addClass("active");
        }
    },
    //保存
    saveFun: function (obj)
    {
        var url = "/api/systemCompany/add";
        var param = {};
        param.CoAddress = $("#CompanyLocation").val();
        param.CoTelephone = $("#CompanyPhone").val();
        param.CoName = $("#CompanyName").val();
        if (observeId != null) {
            url = "/api/systemCompany/update";
            param.id = observeId;
        }
        $.post(url, param, function (rel) {

            if (rel) {
                if (rel.isSuccess) {

                    alert("success");
                    listObj.loadDataFun();

                } else {
                    alert(rel.errorMessage)
                }
            } else {
                alert("unknown error!")
            }

        }).done(function (data) { location.reload(); })
            .fail(function (ex) { });
    },
    updateFun: function (obj) {
        $("#add").modal('show');
        observeId = obj;
        var url = "/api/systemCompany/detail/" + observeId;
        $.get(url, function (rel) {
            if (rel) {
                if (rel.isSuccess) {
                    if (rel.entity) {
                        $("#CompanyName").val(rel.entity.coName);

                        $("#CompanyPhone").val(rel.entity.coTelephone);
                     
                        $("#CompanyLocation").val(rel.entity.coAddress);
                    }
                } else {
                    alert(rel.errorMessage)
                }
            } else {
                alert("unknown error!")
            }

        }).done(function (data) { })
            .fail(function (ex) { });
    },
    clearField: function () {
        $("#CompanyName").val('');
        $("#CompanyPhone").val('');
        $("#CompanyLocation").val('');
        observeId = null;
    },
    
};