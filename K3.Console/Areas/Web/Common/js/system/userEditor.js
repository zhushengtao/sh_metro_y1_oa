﻿var observeId;
function add() {
    observeId = null;
    clearValues();
}
$(function () {
    if (getUrlParam("id") != "") {
        observeId = getUrlParam("id");
        updateShow(observeId);
    }
    $(".workIdSelect").autocomplete({
        source: function (request, response) {
            var textV = request.term.toLowerCase();

            var result = empOptions.filter(function (item) {
                return item.workId.toLowerCase().indexOf(textV) >= 0;
            });
            response(result.map(function (item) {
                return {
                    label: item.workId,
                    value: item.workId,
                    entity: item,
                };
            }));
        },
        autoFocus: true,
        change: function (event, ui) {
            if ($(event.target).val() == "") {
                $(event.target).attr("data-empid", "");
                return;
            }
            if (ui.item) {
                for (var i = 0; i < empOptions.length; i++) {
                    var item = empOptions[i];

                }
            }
        },
        select: function (event, ui) {
            $(event.target).attr("data-empid", ui.item.entity.id);
        },
        close: function (event, ui) {
            if ($(event.target).val() == "") {
                $(event.target).attr("data-empid", "");
                return;
            }
            if (!$(event.target).attr("data-empid")) {
                $(event.target).attr("data-empid", "").attr("data-empid", "").val("");
                return;
            } else {
                for (var i = 0; i < empOptions.length; i++) {
                    var item = empOptions[i];
                    if (item.id == $(event.target).attr("data-empid")) {
                        $(event.target).val(item.workId);
                        break;
                    }
                }
            }
        },
        minLength: 0
    });

});
var selectedId = null;
function updateShow(id) {
    observeId = id;
    var url = "/api/systemuser/"+id;

    $.get(url, function (rel) {
        if (rel) {
            if (rel.isSuccess) {
                if (rel.entity) {
                    $("#u_name").val(rel.entity.username);
                  
                    $("#u_fullname").val(rel.entity.fullName);
                    $("#u_password").val(rel.entity.password);
                    $("#u_phone").val(rel.entity.mobilePhone);
                    $("#u_email").val(rel.entity.personalEmail);
                    $("#u_workId").val(rel.entity.userWorkNumber);
                    var $radios2 = $('input:radio[name=locked]');
                    if (rel.entity.isLockedOut) {
                        $radios2.filter('[value=true]').prop('checked', true);
                    } else {
                        $radios2.filter('[value=false]').prop('checked', true);
                    }
                }
            } else {
                alert(rel.errorMessage)
            }
        } else {
            alert("unknown error!")
        }

    }).done(function (data) { })
        .fail(function (ex) { });

}
function addNew() {
    var url = "/api/systemuser/add";
    var param = {};
    param.Username = $("#u_name").val();
    param.Password = $("#u_password").val();
    param.PersonalEmail = $("#u_email").val();
    param.FullName = $("#u_fullname").val();
    param.MobilePhone = $("#u_phone").val();
    param.isLockedout = $('input[name="locked"]:checked').val();
    param.UserEmpID = $("#u_workId").attr("data-empid");
 
    param.UserWorkNumber = $("#u_workId").val();
    if (observeId != null) {
        url = "/api/systemuser/update";
        param.id = observeId;
    }
    $.post(url, param, function (rel) {

        if (rel) {
            if (rel.isSuccess) {
                alert("success");
            } else {
                alert(rel.errorMessage)
            }
        } else {
            alert("unknown error!")
        }

    }).done(function (data) { location.reload(); })
        .fail(function (ex) { });
}

//close the parent nav menu in the index when click in all window
function closeMenu() {
    parent.closeMenu();
}