﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GeneralFramework.Console.Areas.Web
{
    public class WebAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Web";
            }
        }


        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
               "Web_default1",
               "Web/Login",
               new { controller = "Console", action = "Login" }
           );

            context.MapRoute(
                "Web_default",
                "Web/{controller}/{action}/{id}",
                new { controller = "Console", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}