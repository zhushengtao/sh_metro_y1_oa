﻿using GeneralFramework.BLL;
using GeneralFramework.Infrastructure.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;

using System.Web.Mvc;
namespace GeneralFramework.Console.Areas.Web.Controllers
{
    [MvcAuthorize(SiteMap.DeFault)]
    public class HumanController : Controller
    {
        private readonly System_MenuBaseService _system_MenuService;
        private readonly System_DepartmentBaseServicec _system_deptService;
        private readonly System_JobBaseService _system_jobService;
        private readonly System_CompanyBaseService _system_companyService;
        public HumanController(System_MenuBaseService system_MenuService, System_DepartmentBaseServicec system_deptService, System_JobBaseService system_jobService, System_CompanyBaseService system_companyService)
        {
            _system_MenuService = system_MenuService;
            _system_deptService = system_deptService;
            _system_jobService = system_jobService;
            _system_companyService = system_companyService;
        }

        public ActionResult EmployeeList()
        {
            ViewData["Dept"] = _system_deptService.GetAll().ToList();
            ViewData["Job"] = _system_jobService.GetAll().ToList();
            ViewData["Company"] = _system_companyService.GetAll().ToList();
            return View();
        }
    }
}