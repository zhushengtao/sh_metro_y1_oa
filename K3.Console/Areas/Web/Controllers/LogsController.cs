﻿using GeneralFramework.Infrastructure.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GeneralFramework.Console.Areas.Web.Controllers
{
    [MvcAuthorize(SiteMap.DeFault)]

    public class LogsController : Controller
    {
        // GET: Web/Logs
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult LogQuery()
        {
            return View();
        }
    }
}