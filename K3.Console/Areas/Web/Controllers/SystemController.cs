﻿using GeneralFramework.BLL;
using GeneralFramework.Infrastructure.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;

using System.Web.Mvc;

namespace GeneralFramework.Console.Areas.Web.Controllers
{
    [MvcAuthorize(SiteMap.DeFault)]

    public class SystemController : Controller
    {
        private readonly System_MenuBaseService _system_MenuService;
        private readonly System_DepartmentBaseServicec _system_deptService;
        private readonly System_JobBaseService _system_jobService;
        private readonly System_CompanyBaseService _system_companyService;
        private readonly Human_EmployeeBaseService _human_empService;
        public SystemController(System_MenuBaseService system_MenuService,System_DepartmentBaseServicec system_deptService, System_JobBaseService system_jobService,Human_EmployeeBaseService human_empService, System_CompanyBaseService system_companyService)
        {
            _system_MenuService = system_MenuService;
            _system_deptService = system_deptService;
            _system_jobService = system_jobService;
            _system_companyService = system_companyService;
            _human_empService = human_empService;
        }
        // GET: Web/SystemUser
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List()
        {
            return View();
        }
        public ActionResult CompanyList()
        {
            return View();
        }
        public ActionResult DepartmentList()
        {
            ViewData["DeptParent"] = _system_deptService.GetAll().Where(c => !c.DeptParentID.HasValue);
            ViewData["Dept"] = _system_deptService.GetAll().ToList();
            return View();
        }
        public ActionResult DepartmentEditor()
        {
            return View();
        }
        public ActionResult MenuList()
        {
            ViewData["MenuParent"] = _system_MenuService.GetAll().Where(c => !c.MenuParentID.HasValue);
            return View();
        }
        public ActionResult UserList()
        {
            ViewData["EmployeeWorkId"] = _human_empService.GetAll().ToList();
            ViewData["Dept"] = _system_deptService.GetAll().ToList();
            ViewData["Job"] = _system_jobService.GetAll().ToList();
            return View();
        }
        public ActionResult UserEditor()
        {
            ViewData["EmployeeWorkId"] = _human_empService.GetAll().ToList();
            return View();
        }
        public ActionResult JobList()
        {
            ViewData["Dept"] = _system_deptService.GetAll().ToList();
            ViewData["Company"] = _system_companyService.GetAll().ToList();
            ViewData["Job"] = _system_jobService.GetAll().ToList();
            return View();
        }
        public ActionResult JobEditor()
        {
            ViewData["Dept"] = _system_deptService.GetAll().ToList();
            ViewData["Company"] = _system_companyService.GetAll().ToList();
            ViewData["Job"] = _system_jobService.GetAll().ToList();
            return View();
        }
        public ActionResult RoleList()
        {
            return View();
        }
    }
}