﻿using GeneralFramework.BLL;
using GeneralFramework.EntityFreamework;
using GeneralFramework.Infrastructure.ActionFilters;
using GeneralFramework.Infrastructure.Utilities;
using GeneralFramework.Model;
using GeneralFramework.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
namespace GeneralFramework.Console.Areas.Web.Controllers
{
    [RoutePrefix("Web")]
    public class ConsoleController : Controller
    {
        private readonly System_MenuBaseService _system_MenuService;
        private readonly SystemUserService _systemUserService;
        private readonly Human_EmployeeBaseService _human_EmployeeService;

        public ConsoleController(System_MenuBaseService system_MenuService,SystemUserService systemUserService,Human_EmployeeBaseService human_EmployeeService)
        {
            _system_MenuService = system_MenuService;
            _systemUserService = systemUserService;
            _human_EmployeeService = human_EmployeeService;
           
        }
        [MvcAuthorize]
        public ActionResult Index()
        {
           
            ViewData["GetUserCatalogsSubTree"] = GetUserCatalogsTree();
            return View();
        }
        private JsTreeVM GetChildren(System_MenuBase catalog)
        {
            var jsTreeVm = new JsTreeVM(catalog.Id.ToString(), catalog.MenuName, null, "root", catalog.MenuUrl,catalog.MenuIcoUrl);
            if (catalog.Children.Any(c => !c.IsDeleted))
            {
                jsTreeVm.children = new List<JsTreeVM>();
                var children = new List<JsTreeVM>();
                foreach (var child in catalog.Children.Where(c => !c.IsDeleted))
                {
                    children.Add(GetChildren(child));
                }
                jsTreeVm.children = children;
            }

            return jsTreeVm;
        }

        public IEnumerable<JsTreeVM> CatalogTree()
        {
            var list = _system_MenuService.GetByParentId(null);

            JsTreeVM[] _list = new JsTreeVM[list.Entity.Count];
            var i = 0;
            foreach (System_MenuBase _item in list.Entity)
            {
                _list[i] = GetChildren(_item);
                i++;
            }
            return _list;
        }
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult Logout()
        {

            Request.GetOwinContext().Authentication.SignOut();
            FormsAuthentication.SignOut();
            Request.Cookies.Clear();
            SessionHelper.Abandon();
            return RedirectToAction("Login");
        }
        public ActionResult Dashboard()
        {
            return View();
        }
        /// <summary>
        /// 获取当前用户菜单
        /// </summary>
        /// <returns></returns>
        protected IEnumerable<CatelogTreeVM> GetUserCatalogsTree()
        {
            if (SessionHelper.CurrentUser == null)
            {
                return null;
            }
            try
            {
                var catalogIds = _systemUserService.GetUserCatalogsIdsFromDb(SessionHelper.CurrentUser.Id);


                if (SessionHelper.CurrentUser.Username == "admin")
                {
                    catalogIds = _system_MenuService.GetAll().Select(c=>c.Id).ToList();
                }
                
                var userCatalogAll = _system_MenuService.GetAll().Where(o => catalogIds.Contains(o.Id)).ToList();
                var root = _system_MenuService.GetAll().FirstOrDefault(o => o.Layer == 1);
                if (root == null)
                {
                    return null;
                }
                foreach (var item in catalogIds)
                {
                    var entity = _system_MenuService.GetById(item).Entity;
                    if (entity != null)
                    {
                        if (entity.MenuParentID != null)
                        {
                            userCatalogAll.Add(entity.MenuParent);
                        }
                    }
                }
                userCatalogAll = userCatalogAll.Distinct().ToList();
                var list = userCatalogAll.Where(o => o.Layer == 1).OrderBy(o => o.LineNum).ToList();

                CatelogTreeVM[] _list = new CatelogTreeVM[list.Count];

                var i = 0;
                foreach (System_MenuBase _item in list)
                {
                    var sublist = userCatalogAll.Where(o => o.Layer == 2 && o.MenuParentID == _item.Id).OrderBy(o => o.LineNum).ToList();

                    CatelogTreeVM[] _children = new CatelogTreeVM[sublist.Count];
                    var r = 0;
                    foreach (var _subitems in sublist)
                    {
                        _children[r] = new CatelogTreeVM(_subitems.Id, _subitems.MenuName,_subitems.MenuParentID, _subitems.Layer, _subitems.MenuUrl, null);
                        r++;
                    }

                    _list[i] = new CatelogTreeVM(_item.Id, _item.MenuName,  _item.MenuParentID, _item.Layer, _item.MenuUrl, _children);
                    i++;
                }
                return _list;
            }
            catch (Exception ex)
            {
               
            }
            return null;
        }
    }
}