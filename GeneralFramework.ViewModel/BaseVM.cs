﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using GeneralFramework.Model;

namespace GeneralFramework.ViewModel
{
    public class BaseVM
    {
        public Guid Id { get; set; }

        [JsonIgnore]
        public long LongId { get; set; }

        public int LineNum { get; set; }

        [Display(Name = "备注信息")]
        public string Remarks { get; set; }

        [Display(Name = "创建时间")]
        public DateTime? CreatedOn { get; set; }

        [Display(Name = "操作员")]
        public Guid? CreatedBy { get; set; }

        [Display(Name = "修改时间")]
        public DateTime? ModifiedOn { get; set; }

        [Display(Name = "修改者")]
        public Guid? ModifiedBy { get; set; }


        public BaseVM() { }

        public BaseVM(BaseEntity domain)
        {
            if (domain != null)
            {
                Id = domain.Id;
                CreatedOn = domain.CreatedOn;
                CreatedBy = domain.CreatedBy;
                ModifiedOn = domain.ModifiedOn;
                ModifiedBy = domain.ModifiedBy;
                LongId = domain.LongId;
                LineNum = domain.LineNum;
                Remarks = domain.Remarks;
            }
        }


    }

    public class SimpleBaseVM
    {
        public Guid Id { get; set; }

        [JsonIgnore]
        public long LongId { get; set; }
        public int LineNum { get; set; }        
        public string Remarks { get; set; }
         [JsonIgnore]
        public DateTime? CreatedOn { get; set; }
        [JsonIgnore]
        public Guid? CreatedBy { get; set; }
        [JsonIgnore]
        public DateTime? ModifiedOn { get; set; }
        [JsonIgnore]
        public Guid? ModifiedBy { get; set; }

        public SimpleBaseVM() { }

        public SimpleBaseVM(BaseEntity domain)
        {
            if (domain != null)
            {
                Id = domain.Id;
                CreatedOn = domain.CreatedOn;
                CreatedBy = domain.CreatedBy;
                ModifiedOn = domain.ModifiedOn;
                ModifiedBy = domain.ModifiedBy;
                LongId = domain.LongId;
                LineNum = domain.LineNum;
                Remarks = domain.Remarks;
            }
        }


    }
}
