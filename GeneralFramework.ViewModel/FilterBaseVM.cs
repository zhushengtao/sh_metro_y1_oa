﻿namespace GeneralFramework.ViewModel
{
    public class FilterBaseVM
    {
        private int pageSize = 10;
        private int pageIndex = 1;
        private string keywords = string.Empty;

        public string Keywords
        {
            get
            {
                if (keywords == null)
                    return string.Empty;
                else
                    return keywords;
            }
            set
            {
                keywords = value;
            }
        }
        public int PageSize
        {
            get
            {
                if (pageSize <= 0)
                    return 10;
                else
                    return pageSize;
            }
            set
            {
                pageSize = value;
            }
        }
        public int PageIndex
        {
            get
            {
                if (pageIndex <= 0)
                    return 10;
                else
                    return pageIndex;
            }
            set
            {
                pageIndex = value;
            }
        }

        public FilterBaseVM()
        {
            Keywords = string.Empty;
            pageSize = 10;
            pageIndex = 1;
        }

    }
}
