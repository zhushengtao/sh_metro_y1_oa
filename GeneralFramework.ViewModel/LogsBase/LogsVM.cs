﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFramework.ViewModel
{
    #region 说明
    //viewmodel用来定义前端需要显示的对象，定义尽量简单清晰明了,减少不必要的嵌套。过多的嵌套会影响前端数据加载速度
    #endregion
    public class LogsVM
    {
        public Guid Id { get; set; }
        public Guid? objectId { get; set; }
        public string requestContent { get; set; }
        public string responseContent { get; set; }
        public string tags { get; set; }
        public long? duration { get; set; }
        public string methodName { get; set; }
        public string typeCode { get; set; }
        public string sourceIP { get; set; }
        public string createdOn { get; set; }
    }
    public class LogsQueryParam:FilterBaseVM
    {
        public string objectId { get; set; }
        public string requestContent { get; set; }
        public string responseContent { get; set; }
        public string tags { get; set; }
        public long? duration { get; set; }
        public string methodName { get; set; }
        public string typeCode { get; set; }
        public string sourceIP { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
    }
}
