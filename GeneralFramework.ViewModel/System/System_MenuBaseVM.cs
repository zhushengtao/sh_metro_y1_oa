﻿using GeneralFramework.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFramework.ViewModel
{
    public class System_MenuBaseVM : BaseVM
    {
        public string MenuName { get; set; }
        public Guid? MenuParentID { get; set; }
        public string MenuUrl { get; set; }
        public string MenuDescription { get; set; }
        public string MenuIcoUrl { get; set; }
        public string MenuIsShow { get; set; }
        public string MenuNumber { get; set; }
        public string MenuNote { get; set; }
        public int? MenuSortNo { get; set; }
        public int? MenuState { get; set; }
        public string MenuClientIP { get; set; }
        public int Layer { get; set; }
        public virtual System_MenuBaseVM Parent { get; set; }
        /// <summary>
        /// Sub Nodes 
        /// </summary>
        public virtual ICollection<System_MenuBaseVM> Children { get; set; }
        public System_MenuBaseVM() { }

        public System_MenuBaseVM(System_MenuBase entity, bool withChildren = true)
            : base(entity)
        {
            if (entity != null)
            {
                MenuClientIP = entity.MenuClientIP;
                MenuDescription = entity.MenuDescription;
                MenuIcoUrl = entity.MenuIcoUrl;
                MenuIsShow = entity.MenuIsShow;
                MenuName = entity.MenuName;
                MenuNote = entity.MenuNote;
                MenuNumber = entity.MenuNumber;
                MenuParentID = entity.MenuParentID;
                MenuSortNo = entity.MenuSortNo;
                MenuState = entity.MenuState;
                MenuUrl = entity.MenuUrl;
                if (entity.Children != null && entity.Children.Count > 0 && withChildren)
                {
                    Children = entity.Children.Where(c => !c.IsDeleted).Select(c => new System_MenuBaseVM(c)).ToList();
                }
                if (entity.MenuParent != null && withChildren)
                {
                    Parent = new System_MenuBaseVM(entity.MenuParent, false);
                }
                Layer = entity.Layer;
            }

        }
    }
    public class System_MenuBaseParam : BaseVM
    {
        public string MenuName { get; set; }
        public Guid? MenuParentID { get; set; }
        public string MenuUrl { get; set; }
        public string MenuDescription { get; set; }
        public string MenuIcoUrl { get; set; }
        public string MenuIsShow { get; set; }
        public string MenuNumber { get; set; }
        public string MenuNote { get; set; }
        public int MenuSortNo { get; set; }
        public int MenuState { get; set; }
        public string MenuClientIP { get; set; }
    }
    public class System_MenuBaseFilter : FilterBaseVM
    {
        public string MenuName { get; set; }
        public Guid? MenuParentID { get; set; }
        public string MenuUrl { get; set; }
        public string MenuDescription { get; set; }
        public string MenuIcoUrl { get; set; }
        public string MenuIsShow { get; set; }
        public string MenuNumber { get; set; }
        public string MenuNote { get; set; }
        public int? MenuSortNo { get; set; }
        public int? MenuState { get; set; }
        public string MenuClientIP { get; set; }
    }
    /// <summary>
    /// 菜映射
    /// </summary>
    public class MenuMappingParam
    {
        public List<Guid> MenuIds { get; set; }
        public Guid ObjectId { get; set; }
        public MenuMappingType MenuMapingType { get; set; }
    }
    public class MenumappingVM
    {
        public Guid MenuId { get; set; }
        public Guid ObjectId { get; set; }
    }



    public class CatelogTreeVM
    {
        /// <summary>
        /// node id
        /// </summary>       
        public Guid Id { get; set; }

        public int Layer { get; set; }

        public Guid? ParentId { get; set; }

        public string CatalogName { get; set; }

        public string InternalNumber { get; set; }

        public string LinkUrl { get; set; }
        public string IconUrl { get; set; }

        public string LanguageLabelName { get; set; }

        public string CatalogType { get; set; }

        public int LineNum { get; set; }

        public virtual ICollection<CatelogTreeVM> Children { get; set; }

        public CatelogTreeVM() { }

        public CatelogTreeVM(System_MenuBase entity, bool withChildren = true)
        {
            if (entity != null)
            {
                Id = entity.Id;
                CatalogName = entity.MenuName;
                InternalNumber = entity.MenuNumber;

                ParentId = entity.MenuParentID;
                Layer = entity.Layer;

                LinkUrl = entity.MenuUrl;

                LineNum = entity.LineNum;

                if (entity.Children != null && withChildren)
                {
                    Children = entity.Children.Select(c => new CatelogTreeVM(c)).ToList();
                }
            }

        }

        public CatelogTreeVM(Guid Id, string CatalogName, Guid? ParentId, int Layer, string LinkUrl, ICollection<CatelogTreeVM> Children)
        {
            this.Id = Id;
            this.CatalogName = CatalogName;

            this.ParentId = ParentId;
            this.Layer = Layer;

            this.LinkUrl = LinkUrl;
            this.Children = Children;
        }

       
    }
}
