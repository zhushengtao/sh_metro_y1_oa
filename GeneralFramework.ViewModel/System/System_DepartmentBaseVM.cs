﻿using GeneralFramework.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFramework.ViewModel
{
    public class System_DepartmentBaseVM:BaseVM
    {
        public string DeptName { get; set; }
        public string DeptNumber { get; set; }
        public Guid? DeptParentID { get; set; }
        public string DeptGroupStID { get; set; }
        public string DeptGroupStName { get; set; }
        public string DeptOperManID { get; set; }
        public string DeptOperManName { get; set; }
        public string DeptEmail { get; set; }
        public string DeptTelePhone { get; set; }
        public string DeptFax { get; set; }
        public Guid? DeptCoID { get; set; }
        public string DeptType { get; set; }
        public string DeptNote { get; set; }
        public string DeptTreeCode { get; set; }
        public string DeptSortNo { get; set; }
        public string DeptState { get; set; }
        public string DeptIsShow { get; set; }
        public string DeptClientIP { get; set; }
        public virtual System_DepartmentBaseVM Parent { get; set; }
        public virtual ICollection<System_DepartmentBaseVM> Children { get; set; }
        public System_DepartmentBaseVM() { }

        public System_DepartmentBaseVM(System_DepartmentBase entity, bool withChildren = true)
            : base(entity)
        {
            if (entity != null)
            {
                DeptName = entity.DeptName;
                DeptNumber = entity.DeptNumber;
                DeptParentID = entity.DeptParentID;
                DeptGroupStID = entity.DeptGroupStID;
                DeptGroupStName = entity.DeptGroupStName;
                DeptOperManID = entity.DeptOperManID;
                DeptOperManName = entity.DeptOperManName;
                DeptEmail = entity.DeptEmail;
                DeptTelePhone = entity.DeptTelePhone;
                DeptFax = entity.DeptFax;
                DeptCoID = entity.DeptCoID;
                DeptType = entity.DeptType;
                DeptNote = entity.DeptNote;
                DeptTreeCode = entity.DeptTreeCode;
                DeptSortNo = entity.DeptSortNo;
                DeptState = entity.DeptState;
                DeptIsShow = entity.DeptIsShow;
                DeptClientIP = entity.DeptClientIP;
                if (entity.Children != null && entity.Children.Count > 0 && withChildren)
                {
                    Children = entity.Children.Where(c => !c.IsDeleted).Select(c => new System_DepartmentBaseVM(c)).ToList();
                }
                if (entity.DeptParent != null && withChildren)
                {
                    Parent = new System_DepartmentBaseVM(entity.DeptParent, false);
                }
            }

        }
    }
    public class System_DepartmentBaseParam : BaseVM
    {
        public string DeptName { get; set; }
        public string DeptNumber { get; set; }
        public Guid? DeptParentID { get; set; }
        public string DeptGroupStID { get; set; }
        public string DeptGroupStName { get; set; }
        public string DeptOperManID { get; set; }
        public string DeptOperManName { get; set; }
        public string DeptEmail { get; set; }
        public string DeptTelePhone { get; set; }
        public string DeptFax { get; set; }
        public Guid? DeptCoID { get; set; }
        public string DeptType { get; set; }
        public string DeptNote { get; set; }
        public string DeptTreeCode { get; set; }
        public string DeptSortNo { get; set; }
        public string DeptState { get; set; }
        public string DeptIsShow { get; set; }
        public string DeptClientIP { get; set; }
       
    }
    public class System_DepartmentBaseFilter : FilterBaseVM
    {
        public string DeptName { get; set; }
        public string DeptNumber { get; set; }
        public Guid DeptParentID { get; set; }
        public string DeptGroupStID { get; set; }
        public string DeptGroupStName { get; set; }
        public string DeptOperManID { get; set; }
        public string DeptOperManName { get; set; }
        public string DeptEmail { get; set; }
        public string DeptTelePhone { get; set; }
        public string DeptFax { get; set; }
        public Guid DeptCoID { get; set; }
        public string DeptType { get; set; }
        public string DeptNote { get; set; }
        public string DeptTreeCode { get; set; }
        public string DeptSortNo { get; set; }
        public string DeptState { get; set; }
        public string DeptIsShow { get; set; }
        public string DeptClientIP { get; set; }

    }
}
