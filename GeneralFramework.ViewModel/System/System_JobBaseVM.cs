﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneralFramework.Model;

namespace GeneralFramework.ViewModel
{
    public class System_JobBaseVM : BaseVM
    {
        public string JobName { get; set; }
        public string JobNumber { get; set; }
        public Guid? JobParentID { get; set; }
        public Guid? JobDeptID { get; set; }
        public Guid? JobCoID { get; set; }
        public string JobLevel { get; set; }
        public string JobType { get; set; }
        public string JobNote { get; set; }
        public string JobTreeCode { get; set; }
        public string JobSortNo { get; set; }
        public int? JobState { get; set; }
        public int? JobtIsShow { get; set; }
        public string JobClientIP { get; set; }
        public string CompanyName { get; set; }
        public string DeptName { get; set; }
        public string JobParaentName { get; set; }
        public virtual System_JobBaseVM Parent { get; set; }
        public virtual ICollection<System_JobBaseVM> Children { get; set; }
        public System_JobBaseVM() { }

        public System_JobBaseVM(System_JobBase entity, bool withChildren = true)
            : base(entity)
        {
            if (entity != null)
            {
                JobName = entity.JobName;
                JobNumber = entity.JobNumber;
                JobParentID = entity.JobParentID;
                JobDeptID = entity.JobDeptID;
                JobCoID = entity.JobCoID;
                JobLevel = entity.JobLevel;
                JobType = entity.JobType;
                JobNote = entity.JobNote;
                JobTreeCode = entity.JobTreeCode;
                JobSortNo = entity.JobSortNo;
                JobState = entity.JobState;
                JobtIsShow = entity.JobtIsShow;
                JobClientIP = entity.JobClientIP;


                if (entity.Children != null && entity.Children.Count > 0 && withChildren)
                {
                    Children = entity.Children.Where(c => !c.IsDeleted).Select(c => new System_JobBaseVM(c)).ToList();
                }
                if (entity.JobParent != null && withChildren)
                {
                    Parent = new System_JobBaseVM(entity.JobParent, false);
                }
            }

        }
    }

    public class System_JobBaseParam : BaseVM
    {
        public string JobName { get; set; }
        public string JobNumber { get; set; }
        public Guid? JobParentID { get; set; }
        public Guid? JobDeptID { get; set; }
        public Guid? JobCoID { get; set; }
        public string JobLevel { get; set; }
        public string JobType { get; set; }
        public string JobNote { get; set; }
        public string JobTreeCode { get; set; }
        public string JobSortNo { get; set; }
        public int? JobState { get; set; }
        public int? JobtIsShow { get; set; }
        public string JobClientIP { get; set; }
    }
    public class System_JobFilter : FilterBaseVM
    {
        public string JobName { get; set; }
        public string JobNumber { get; set; }
        public Guid? JobParentID { get; set; }
        public Guid? JobDeptID { get; set; }
        public Guid? JobCoID { get; set; }
        public string JobLevel { get; set; }
        public string JobType { get; set; }
        public string JobNote { get; set; }
        public string JobTreeCode { get; set; }
        public string JobSortNo { get; set; }
        public int? JobState { get; set; }
        public int? JobtIsShow { get; set; }
        public string JobClientIP { get; set; }
    }
    public class JobBaseVM:BaseVM
    {
        public string JobName { get; set; }
        public string JobNumber { get; set; }
        public Guid? JobParentID { get; set; }
        public Guid? JobDeptID { get; set; }
        public Guid? JobCoID { get; set; }
        public string JobLevel { get; set; }
        public string JobType { get; set; }
        public string JobNote { get; set; }
        public string JobTreeCode { get; set; }
        public string JobSortNo { get; set; }
        public int? JobState { get; set; }
        public int? JobtIsShow { get; set; }
        public string JobClientIP { get; set; }
        public string CompanyName { get; set; }
        public string DeptName { get; set; }
    }
}
