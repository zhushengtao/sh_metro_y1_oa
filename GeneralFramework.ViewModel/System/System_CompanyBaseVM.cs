﻿using GeneralFramework.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFramework.ViewModel
{
    public class System_CompanyBaseVM:BaseVM
    { 
        /// <summary>
        /// 名称
        /// </summary>
        public string CoName { get; set; }
        /// <summary>
        /// 编号
        /// </summary>
        public string CoNumber { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string CoAddress { get; set; }
        /// <summary>
        /// 公司电话
        /// </summary>
        public string CoTelephone { get; set; }
        /// <summary>
        /// 公司邮箱
        /// </summary>
        public string CoEmail { get; set; }
        /// <summary>
        /// 公司传真
        /// </summary>
        public string CoFax { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int CoState { get; set; }
        /// <summary>
        /// 是否显示
        /// </summary>
        public int CoIsShow { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        public int CoSortNo { get; set; }
        /// <summary>
        /// 对应其他系统名称
        /// </summary>
        public string CoOtherName { get; set; }
        /// <summary>
        /// 对应其他系统编号
        /// </summary>
        public string CoOtherNumber { get; set; }
        /// <summary>
        /// 菜单编码（对应菜单ID或者菜单码）
        /// </summary>
        public string CoTreeCode { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string CoNote { get; set; }
        public string CoClientIP { get; set; }
        public System_CompanyBaseVM() { }
        public System_CompanyBaseVM(System_CompanyBase entity)
            : base(entity)
        {
            if (entity != null)
            {
                CoName = entity.CoName;
                CoNumber = entity.CoNumber;
                CoAddress = entity.CoAddress;
                CoTelephone = entity.CoTelephone;
                CoEmail = entity.CoEmail;
                CoFax = entity.CoFax;
                CoState = entity.CoState;
                CoIsShow = entity.CoIsShow;
                CoSortNo = entity.CoSortNo;
                CoOtherName = entity.CoOtherName;
                CoOtherNumber = entity.CoOtherNumber;
                CoTreeCode = entity.CoTreeCode;
                CoNote = entity.CoNote;
                CoClientIP = entity.CoClientIP;
            }
        }
    }
    public class System_CompanyBaseParam:BaseVM
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string CoName { get; set; }
        /// <summary>
        /// 编号
        /// </summary>
        public string CoNumber { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string CoAddress { get; set; }
        /// <summary>
        /// 公司电话
        /// </summary>
        public string CoTelephone { get; set; }
        /// <summary>
        /// 公司邮箱
        /// </summary>
        public string CoEmail { get; set; }
        /// <summary>
        /// 公司传真
        /// </summary>
        public string CoFax { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int CoState { get; set; }
        /// <summary>
        /// 是否显示
        /// </summary>
        public int CoIsShow { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        public int CoSortNo { get; set; }
        /// <summary>
        /// 对应其他系统名称
        /// </summary>
        public string CoOtherName { get; set; }
        /// <summary>
        /// 对应其他系统编号
        /// </summary>
        public string CoOtherNumber { get; set; }
        /// <summary>
        /// 菜单编码（对应菜单ID或者菜单码）
        /// </summary>
        public string CoTreeCode { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string CoNote { get; set; }
        public string CoClientIP { get; set; }
    }
    public class System_CompanyBaseFilter : FilterBaseVM
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string CoName { get; set; }
        /// <summary>
        /// 编号
        /// </summary>
        public string CoNumber { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string CoAddress { get; set; }
        /// <summary>
        /// 公司电话
        /// </summary>
        public string CoTelephone { get; set; }
        /// <summary>
        /// 公司邮箱
        /// </summary>
        public string CoEmail { get; set; }
        /// <summary>
        /// 公司传真
        /// </summary>
        public string CoFax { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int CoState { get; set; }
        /// <summary>
        /// 是否显示
        /// </summary>
        public int CoIsShow { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        public int CoSortNo { get; set; }
        /// <summary>
        /// 对应其他系统名称
        /// </summary>
        public string CoOtherName { get; set; }
        /// <summary>
        /// 对应其他系统编号
        /// </summary>
        public string CoOtherNumber { get; set; }
        /// <summary>
        /// 菜单编码（对应菜单ID或者菜单码）
        /// </summary>
        public string CoTreeCode { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string CoNote { get; set; }
        public string CoClientIP { get; set; }
    }
}
