﻿using GeneralFramework.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFramework.ViewModel
{
    public class System_RoleBaseVM : BaseVM
    {
        /// <summary>
        /// 角色名称
        /// </summary>
        public string RoleName { get; set; }
        public bool IsProtected { get; set; }
        public bool IsLockout { get; set; }
        public string Organization { get; set; }
        public System_RoleBaseVM() { }
        public System_RoleBaseVM(System_RolesBase entity)
            : base(entity)
        {

            if (entity != null)
            {
                RoleName = entity.RoleName;
                IsProtected = entity.IsProtected;
                IsLockout = entity.IsLockedOut;
                Organization = entity.Organization;
            }
        }
    }

    public class RoleParam : BaseVM
    {
        public string RoleName { get; set; }
        public bool IsProtected { get; set; }
        public bool IsLockout { get; set; }
        public string Organization { get; set; }
    }
    public class RoleFilter : FilterBaseVM
    {
        public string RoleName { get; set; }
        public bool IsProtected { get; set; }
        public bool IsLockout { get; set; }
        public string Organization { get; set; }
    }
}
