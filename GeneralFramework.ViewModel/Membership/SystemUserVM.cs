﻿using GeneralFramework.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace GeneralFramework.ViewModel
{
    public class SystemUserVM : BaseVM
    {
        public string Username { get; set; }

        [JsonIgnore]
        public string Password { get; set; }

        [JsonIgnore]
        public string PasswordHash { get; set; }

        [JsonIgnore]
        public string PasswordSalt { get; set; }

        [JsonIgnore]
        public DateTime? PasswordModifiedOn { get; set; }
        public string PersonalEmail { get; set; }
        public string InternalEmail { get; set; }

        [JsonIgnore]
        public string InternalEmailPassword { get; set; }
        public string FullName { get; set; }
        public string NickName { get; set; }
        public string Salutation { get; set; }
        public string MobilePhone { get; set; }
        public string BusinessPhone { get; set; }
        public string HomePhone { get; set; }
        public string Fax { get; set; }
        public string WindowsLiveID { get; set; }
        public bool IsApproved { get; set; }
        public bool IsLockedOut { get; set; }
        public string Comment { get; set; }
        public string Language { get; set; }
        public string ClientCookieValue { get; set; }

        public SystemUserVM() { }
        public SystemUserVM(SystemUser entity)
            : base(entity)
        {
            if (entity != null)
            {
                Username = entity.Username;
                Password = entity.Password;
                PasswordHash = entity.PasswordHash;
                PasswordSalt = entity.PasswordSalt;
                PasswordModifiedOn = entity.PasswordModifiedOn;
                PersonalEmail = entity.PersonalEmail;
                InternalEmail = entity.InternalEmail;
                InternalEmailPassword = entity.InternalEmailPassword;
                FullName = entity.FullName;
                NickName = entity.NickName;
                Salutation = entity.Salutation;
                MobilePhone = entity.MobilePhone;
                BusinessPhone = entity.BusinessPhone;
              
                
            }
        }
    }

    public class SystemUserParam
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string PasswordHash { get; set; }

        public string PasswordSalt { get; set; }

        public DateTime? PasswordModifiedOn { get; set; }
        public string PersonalEmail { get; set; }
        public string InternalEmail { get; set; }

        public string InternalEmailPassword { get; set; }
        public string FullName { get; set; }
        public string NickName { get; set; }
        public string Salutation { get; set; }
        public string MobilePhone { get; set; }
        public string BusinessPhone { get; set; }
        public string HomePhone { get; set; }
        public string Fax { get; set; }
        public string WindowsLiveID { get; set; }
        public bool IsApproved { get; set; }
        public bool IsLockedOut { get; set; }
    }

}
