﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFramework.ViewModel
{
    [Serializable]
    public class JsTreeVM
    {
        /// <summary>
        /// node id
        /// </summary>       
        public string id { get; set; }

        /// <summary>
        /// node text 
        /// </summary>     
        public string text { get; set; }


        /// <summary>
        /// node type
        /// </summary>      
        public string type { get; set; }

        /// <summary>
        /// object code
        /// </summary>      
        public string url { get; set; }

        public string icon { get; set; }

        public IEnumerable<JsTreeVM> children { get; set; }

        public JsTreeVM() { }

        public JsTreeVM(string id, string text, IEnumerable<JsTreeVM> children, string type,string url,string icon)
        {
            this.id = id;
            this.text = text;
            this.children = children;
            this.type = type;
            this.url = url;
            this.icon = icon;
        }

        //public JsTreeVM(string id, string text, IEnumerable<JsTreeVM> children, string type, string url)
        //{
        //    this.id = id;
        //    this.text = text;
        //    this.children = children;
        //    this.type = type;
        //    this.ocode = ocode;
        //}
        //public JsTreeVM(string id, string text, IEnumerable<JsTreeVM> children, string type, string ocode, string riskCode)
        //{
        //    this.id = id;
        //    this.text = text;
        //    this.children = children;
        //    this.type = type;
        //    this.ocode = ocode;
        //    this.riskCode = riskCode;
        //}
    }

    [Serializable]
    public class JsTree
    {
        /// <summary>
        /// node id
        /// </summary>       
        public string id { get; set; }

        /// <summary>
        /// node text 
        /// </summary>     
        public string text { get; set; }

        /// <summary>
        /// node type
        /// </summary>      
        public string type { get; set; }

        /// <summary>
        /// object code
        /// </summary>      
        public string url { get; set; }

        public bool children { get; set; }
        public string icon { get; set; }
        public JsTree() { }

        public JsTree(string id, string text, bool children,string url,string icon)
        {
            this.id = id;
            this.text = text;
            this.children = children;
            this.url = url;
            this.icon = icon;
        }
    }
}
