﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace GeneralFramework.Model
{
    #region 说明
    //这里为数据结构的定义、数据结构定义尽量避免多层映射。对于查询比较频繁的字段增加索引。字符串类型的字段请标识字段大小(MaxLength)
    //BaseEntity为基类，继承后就会自动创建BaseEntity定义的字段
    #endregion
    [Table("System_UserBase")]
    public class SystemUser : BaseEntity
    {
        [MaxLength(256)]
        public string Username { get; set; }

        [MaxLength(256), JsonIgnore]
        public string Password { get; set; }


        [MaxLength(256), JsonIgnore]
        public string PasswordHash { get; set; }

        [MaxLength(256), JsonIgnore]
        public string PasswordSalt { get; set; }
        [JsonIgnore]
        public DateTime? PasswordModifiedOn { get; set; }

        [MaxLength(256)]
        public string PersonalEmail { get; set; }

        [MaxLength(256)]
        public string InternalEmail { get; set; }

        [MaxLength(256), JsonIgnore]
        public string InternalEmailPassword { get; set; }

        [MaxLength(128)]
        public string FullName { get; set; }
        [MaxLength(128)]
        public string NickName { get; set; }
        [MaxLength(128)]
        public string Salutation { get; set; }

        [MaxLength(256)]
        public string MobilePhone { get; set; }

        [MaxLength(256)]
        public string BusinessPhone { get; set; }


    }

}
