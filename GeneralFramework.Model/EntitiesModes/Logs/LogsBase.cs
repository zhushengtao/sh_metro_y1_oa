﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Text;

namespace GeneralFramework.Model
{
    [Table("LogsBase")]
    public class LogsBase : BaseEntity
    {
        public Guid? objectId { get; set; }
        public string requestContent { get; set; }
        public string responseContent { get; set; }
        public string tags { get; set; }
        public long? duration { get; set; }
        public string methodName { get; set; }
        public string typeCode { get; set; }
        public string sourceIP { get; set; }
        public string LogsContent { get; set; }

        [NotMapped, IgnoreDataMember]
        public StringBuilder LogDetail { get; set; }
        [NotMapped, IgnoreDataMember]
        public bool DontSaveLog { get; set; }
        private void AppendNewLineLogContent(string line)
        {
            if (LogDetail == null)
            {
                LogDetail = new StringBuilder();
            }
            LogDetail.Append(DateTime.Now.ToString("【HH:mm:ss.fff】 ") + line + "\n");
        }
        /// <summary>
        /// 往logContent里边添加一行 有时间标记
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public void AppendLine(string format, params object[] args)
        {
            if (string.IsNullOrEmpty(format))
            {
                AppendNewLineLogContent(string.Empty);
                return;
            }
            if (args.Length == 0)
            {
                AppendNewLineLogContent(format);
                return;
            }
            AppendNewLineLogContent(string.Format(format, args));
        }
    }
}
