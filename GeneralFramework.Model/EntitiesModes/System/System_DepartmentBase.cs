﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFramework.Model
{
    [Table("System_DepartmentBase")]
    public class System_DepartmentBase : BaseEntity
    {
        [MaxLength(256)]
        public string DeptName { get; set; }
        [MaxLength(256)]
        public string DeptNumber { get; set; }
        public Guid? DeptParentID { get; set; }
        [ForeignKey("DeptParentID")]
        public virtual System_DepartmentBase DeptParent { get; set; }
        public string DeptGroupStID { get; set; }
        [MaxLength(256)]
        public string DeptGroupStName { get; set; }
        public string DeptOperManID { get; set; }
        [MaxLength(256)]
        public string DeptOperManName { get; set; }
        [MaxLength(256)]
        public string DeptEmail { get; set; }
        [MaxLength(256)]
        public string DeptTelePhone { get; set; }
        [MaxLength(256)]
        public string DeptFax { get; set; }
        public Guid? DeptCoID { get; set; }
        public string DeptType { get; set; }
        [MaxLength(256)]
        public string DeptNote { get; set; }
        [MaxLength(256)]
        public string DeptTreeCode { get; set; }
        public string DeptSortNo { get; set; }
        public string DeptState { get; set; }
        public string DeptIsShow { get; set; }
        public string DeptClientIP { get; set; }
        public virtual ICollection<System_DepartmentBase> Children { get; set; }
    }
}
