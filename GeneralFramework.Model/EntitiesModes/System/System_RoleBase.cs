﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFramework.Model
{
    /// <summary>
    /// 角色
    /// </summary>
    [Table("System_Roles")]
    public class System_RolesBase : BaseEntity
    {
        /// <summary>
        /// 角色名称
        /// </summary>
        [MaxLength(256)]
        public string RoleName { get; set; }
        [MaxLength(256)]
        public string LoweredRoleName { get; set; }
        public bool IsProtected { get; set; }
        public bool IsLockedOut { get; set; }
        [MaxLength(256)]
        public string Organization { get; set; }
        /// <summary>
        /// 用户
        /// </summary>
        public virtual ICollection<SystemUser> SystemUsers { get; set; }

       
    }
}
