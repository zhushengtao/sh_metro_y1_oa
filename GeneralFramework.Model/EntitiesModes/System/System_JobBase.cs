﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFramework.Model
{
    [Table("System_JobBase")]
    public class System_JobBase:BaseEntity
    {
        [MaxLength(256)]
        public string JobName { get; set; }
        [MaxLength(256)]
        public string JobNumber { get; set; }
        public Guid? JobParentID { get; set; }
        [ForeignKey("JobParentID")]
        public virtual System_JobBase JobParent { get; set; }
        public Guid? JobDeptID { get; set; }
        public Guid? JobCoID { get; set; }
        [MaxLength(256)]
        public string JobLevel { get; set; }
        [MaxLength(256)]
        public string JobType { get; set; }
        [MaxLength(256)]
        public string JobNote { get; set; }
        [MaxLength(256)]
        public string JobTreeCode { get; set; }
        [MaxLength(256)]
        public string JobSortNo { get; set; }
        public int? JobState { get; set; }
        public int? JobtIsShow { get; set; }
        [MaxLength(256)]
        public string JobClientIP { get; set; }
        public virtual ICollection<System_JobBase> Children { get; set; }

    }
}
