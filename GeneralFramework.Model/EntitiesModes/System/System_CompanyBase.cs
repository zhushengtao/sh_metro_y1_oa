﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
namespace GeneralFramework.Model
{
    [Table("System_CompanyBase")]
    public class System_CompanyBase:BaseEntity
    {
        /// <summary>
        /// 名称
        /// </summary>
        [MaxLength(256)]
        public string CoName { get; set; }
        /// <summary>
        /// 编号
        /// </summary>
        [MaxLength(256)]
        public string CoNumber { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        [MaxLength(512)]
        public string CoAddress { get; set; }
        /// <summary>
        /// 公司电话
        /// </summary>
        [MaxLength(256)]
        public string CoTelephone { get; set; }
        /// <summary>
        /// 公司邮箱
        /// </summary>
        [MaxLength(256)]
        public string CoEmail { get; set; }
        /// <summary>
        /// 公司传真
        /// </summary>
        [MaxLength(256)]
        public string CoFax { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int CoState { get; set; }
        /// <summary>
        /// 是否显示
        /// </summary>
        public int CoIsShow { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        public int CoSortNo { get; set; }
        /// <summary>
        /// 对应其他系统名称
        /// </summary>
        [MaxLength(256)]
        public string CoOtherName { get; set; }
        /// <summary>
        /// 对应其他系统编号
        /// </summary>
        [MaxLength(256)]
        public string CoOtherNumber { get; set; }
        /// <summary>
        /// 菜单编码（对应菜单ID或者菜单码）
        /// </summary>
        [MaxLength(256)]
        public string CoTreeCode { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [MaxLength(256)]
        public string CoNote { get; set; }
        [MaxLength(256)]
        public string CoClientIP { get; set; }

    }
}
