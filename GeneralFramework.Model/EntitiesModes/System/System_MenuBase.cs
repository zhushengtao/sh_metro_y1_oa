﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFramework.Model
{
    [Table("System_MenuBase")]
    public class System_MenuBase:BaseEntity
    {
        [MaxLength(256)]
        public string MenuName { get; set; }
        public Guid? MenuParentID { get; set; }
        [ForeignKey("MenuParentID")]
        public virtual System_MenuBase MenuParent { get; set; }
        [MaxLength(256)]
        public string MenuUrl { get; set; }
        [MaxLength(256)]
        public string MenuDescription { get; set; }
        [MaxLength(256)]
        public string MenuIcoUrl { get; set; }
        [MaxLength(256)]
        public string MenuIsShow { get; set; }
        [MaxLength(256)]
        public string MenuNumber { get; set; }
        [MaxLength(256)]
        public string MenuNote { get; set; }

        public int? MenuSortNo { get; set; }
        public int? MenuState { get; set; }
        [MaxLength(256)]
        public string MenuClientIP { get; set; }
        public virtual ICollection<System_MenuBase> Children { get; set; }
        public int Layer { get; set; }

    }
}
