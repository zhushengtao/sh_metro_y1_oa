﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFramework.Model
{
    [Table("System_MenuMappingBase")]
    public class System_MenuMappingBase:BaseEntity
    {
        /// <summary>
        /// 菜单ID
        /// </summary>
        /// 

        public Guid MenuId { get; set; }
        /// <summary>
        /// 映射类型-1部门 2岗位 3角色
        /// </summary>
        public MenuMappingType MappingType { get; set; }
        /// <summary>
        /// 部门ID OR 岗位ID OR 角色ID
        /// </summary>
        public Guid ObjectId { get; set; }
    }
    public enum MenuMappingType
    {
        部门=1,
        岗位=2,
        角色=3
    }
}
