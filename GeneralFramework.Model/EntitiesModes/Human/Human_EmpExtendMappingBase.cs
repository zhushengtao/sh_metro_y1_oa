﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFramework.Model
{
    [Table("Human_EmpExtendMappingBase")]
    public class Human_EmpExtendMappingBase : BaseEntity
    {
        public Guid EmpId { get; set; }
        public Guid ObjectId { get; set; }
        /// <summary>
        /// 1 部门 2岗位 3角色
        /// </summary>
        public UserExtType ExtType { get; set; }
    }
    public enum UserExtType
    {
        部门=1,
        岗位=2,
        角色=3
    }
}
