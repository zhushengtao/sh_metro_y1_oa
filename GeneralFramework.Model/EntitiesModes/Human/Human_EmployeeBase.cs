﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFramework.Model
{
    [Table("Human_EmployeeBase")]
    public class Human_EmployeeBase : BaseEntity
    {
        public string EmpName { get; set; }
        public string EmpUsedName { get; set; }
        public string EmpSex { get; set; }
        public DateTime? EmpBirthday { get; set; }
        public string EmpDdentityNumber { get; set; }
        public string EmpWorkID { get; set; }
        public string EmpOldWorkID { get; set; }
        public string EmpEmail { get; set; }
        public string EmpTelePhone { get; set; }
        public string EmpOfficePhone { get; set; }
        public string EmpFax { get; set; }
        public Guid EmpCoID { get; set; }
        public Guid EmpDeptID { get; set; }
        public Guid EmpJobID { get; set; }
        public string EmpJobNote { get; set; }
        public string EmpBirthplace { get; set; }
        public string EmpNationality { get; set; }
        public string EmpPlaceOfBirth { get; set; }
        public string EmpAddress { get; set; }
        public string EmpPoliticalStatus { get; set; }
        public string EmpType { get; set; }
        public DateTime? EmpJoinYearMonth { get; set; }
        public string EmpPosition { get; set; }
        public DateTime? EmpIdentitydate { get; set; }
        public string EmpEducation { get; set; }
        public string EmpDegree { get; set; }
        public string EmpBankCardNumber { get; set; }
        public string EmpProvidentFundAccount { get; set; }
        public string EmpMaritalStatus { get; set; }
        public string EmpZipCode { get; set; }
        public string EmpMainAccountLocation { get; set; }
        public string EmpJoinWorkTime { get; set; }
        public string EmpJoinCompanyYear { get; set; }
        public string EmpHomePhone { get; set; }
        public string EmpLanguageLevel { get; set; }
        public string EmpSocialSecurityMonth { get; set; }
        public string EmpNote { get; set; }
        public string EmpSortNo { get; set; }
        public string EmpState { get; set; }

    }
}
