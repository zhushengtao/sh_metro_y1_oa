﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFramework.Model
{
    /// <summary>
    /// Entity base class
    /// </summary>
    [Serializable]
    public class BaseEntity 
    {
        [Key]
        [DataMember]
        public Guid Id { get; set; }
        public long LongId { get; set; }
        [DataMember]
        public int LineNum { get; set; }
        public bool IsDeleted { get; set; }
        [MaxLength(255), Display(Name = "备注信息")]
        [DataMember]
        public string Remarks { get; set; }
        [DataMember]
        public DateTime? CreatedOn { get; set; }
        [DataMember]
        public Guid? CreatedBy { get; set; }
        [DataMember]
        public DateTime? ModifiedOn { get; set; }
        [DataMember]
        public Guid? ModifiedBy { get; set; }
        public bool Equals(BaseEntity obj)
        {
            return Id == obj.Id;
        }
    }
}
