﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RCA_RAS.Model
{
    public class CustomDescriptionAttribute : Attribute
    {
        public string Name { get; set; }
        public CustomDescriptionAttribute()
        {
        }

        public CustomDescriptionAttribute(string name)
        {
            Name = name;
        }
    }
}
