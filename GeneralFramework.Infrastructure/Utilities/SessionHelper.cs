﻿
using System;
using System.Collections.Generic;
using System.Web;
using GeneralFramework.ViewModel;
using GeneralFramework.Common.Utilities;

namespace GeneralFramework.Infrastructure.Utilities
{
    public sealed class SessionHelper
    {
        public static List<string> KilledSessions = new List<string>();

        public static void Add<T>(string key, T obj)
        {
            HttpContext.Current.Session[key] = obj;
        }

        public static void Remove(string key)
        {
            HttpContext.Current.Session[key] = null;
        }

        public static T Get<T>(string key)
        {
            var obj = default(T);

            if (HttpContext.Current.Session[key] != null)
            {
                obj = (T)HttpContext.Current.Session[key];
            }
            return obj;
        }
        public static SystemUserVM CurrentUser
        {
            get
            {
                var result = Get<SystemUserVM>(ConstantsBag.CurrentUserKey);
                return result;
            }

            set
            {
                Add<SystemUserVM>(ConstantsBag.CurrentUserKey, value);
               
            }
        }

        private static string GetSessionId()
        {
            //以客户端cookie中为准，可跨机器
            var cookie = HttpContext.Current.Request.Cookies.Get("ASP.NET_SessionId");
            return cookie!=null?cookie.Value:string.Empty;
        }


        /// <summary>
        /// 抛弃当前会话。
        /// </summary>
        /// <remarks>
        /// 一旦调用 Abandon 方法，当前会话不再有效，同时新的会话将可以启动。
        /// (这指的是Abandon后当次请求不可以再访问吗？还是访问会创建新的会话？)
        /// 
        /// 建议在登出后调用该方法，防止他人重用旧的会话；也建议在登入后调用该方法，
        /// 以防止会话固定攻击 (session fixation attack)。
        /// </remarks>
        /// <seealso cref="System.Web.SessionState.HttpSessionState.Abandon"/>
        public static void Abandon()
        {
            HttpContext.Current.Session.Abandon();

            System.Diagnostics.Trace.TraceInformation("[HqqSession] Session Flushed!");
        }
    }
}
