﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFramework.Infrastructure
{
    public class SimpleAccessLog
    {
        public string IPAddress { get; set; }

        public Guid? AccountId { get; set; }

        public string SessionId { get; set; }

        public DateTime AccessTime { get; set; }
    }
}
