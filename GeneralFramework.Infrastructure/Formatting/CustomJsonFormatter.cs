﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using Newtonsoft.Json.Converters;
using System.Web;
using System.Net.Http.Headers;
using GeneralFramework.Common;

namespace GeneralFramework.Infrastructure.Formatting
{
	public class CustomJsonFormatter : JsonMediaTypeFormatter
	{
		static CustomJsonFormatter()
		{
			ApplicationJsonFormatter = GetApplicationFormatter();
		}

		public static JsonMediaTypeFormatter ApplicationJsonFormatter { get; private set; }

		public override Task WriteToStreamAsync(Type type, object value, Stream writeStream, HttpContent content,
			TransportContext transportContext)
		{
			var entityResponse = value as IEntityResponse;
			if (entityResponse != null && entityResponse.Entity != null)
			{
				return base.WriteToStreamAsync(
					entityResponse.Entity.GetType(),
					entityResponse.Entity,
					writeStream,
					content,
					transportContext);
			}

			var responses = value as IEnumerable<EntityResponse>;
			if (responses != null)
			{
				List<object> result = responses.Select(response => !response.IsSuccess
					? response as object
					: response.Entity).ToList();
				return base.WriteToStreamAsync(result.GetType(), result, writeStream, content, transportContext);
			}


			return base.WriteToStreamAsync(type, value, writeStream, content, transportContext);
		}

		private static JsonMediaTypeFormatter GetApplicationFormatter()
		{
			CustomJsonFormatter result = new CustomJsonFormatter();
			result.SerializerSettings.Converters.Add(new StringEnumConverter());
			result.SerializerSettings.ContractResolver = new CustomContractResolver();
			result.SerializerSettings.DateFormatString = "yyyy'-'MM'-'dd' 'HH':'mm':'ss";
			return result;
		}
	}

    public class JsonpMediaTypeFormatter : JsonMediaTypeFormatter
    {
        private string _callbackQueryParamter;
        static JsonpMediaTypeFormatter()
		{
			ApplicationJsonFormatter = GetApplicationFormatter();
		}

		public static JsonMediaTypeFormatter ApplicationJsonFormatter { get; private set; }

        public JsonpMediaTypeFormatter()
        {
            SupportedMediaTypes.Add(DefaultMediaType);
            SupportedMediaTypes.Add(new MediaTypeWithQualityHeaderValue("text/javascript"));
            MediaTypeMappings.Add(new UriPathExtensionMapping("jsonp", DefaultMediaType));          
        }

        public string CallbackQueryParameter
        {
            get { return _callbackQueryParamter ?? "callback"; }
            set { _callbackQueryParamter = value; }
        }

        public override System.Threading.Tasks.Task WriteToStreamAsync(Type type, object value, System.IO.Stream writeStream, System.Net.Http.HttpContent content, System.Net.TransportContext transportContext)
        {
            string callback;
            if (IsJsonpRequest(out callback))
            {
                return Task.Factory.StartNew(() =>
                {
                    var writer = new StreamWriter(writeStream);
                    writer.Write(callback + "(");
                    writer.Flush();
                    base.WriteToStreamAsync(type, value, writeStream, content, transportContext).Wait();
                    writer.Write(")");
                    writer.Flush();
                });
            }
            return base.WriteToStreamAsync(type, value, writeStream, content, transportContext);
        }

        private bool IsJsonpRequest(out string callback)
        {
            callback = null;
            switch (HttpContext.Current.Request.HttpMethod)
            {
                case "POST":
                    callback = HttpContext.Current.Request.Form[CallbackQueryParameter];
                    break;
                default:
                    callback = HttpContext.Current.Request.QueryString[CallbackQueryParameter];
                    break;
            }
            return !string.IsNullOrEmpty(callback);
        }

        private static JsonMediaTypeFormatter GetApplicationFormatter()
        {
            JsonpMediaTypeFormatter result = new JsonpMediaTypeFormatter();
            result.SerializerSettings.Converters.Add(new StringEnumConverter());
            result.SerializerSettings.ContractResolver = new CustomContractResolver();
            result.SerializerSettings.DateFormatString = "yyyy'-'MM'-'dd' 'HH':'mm':'ss";
            return result;
        }
    }
}