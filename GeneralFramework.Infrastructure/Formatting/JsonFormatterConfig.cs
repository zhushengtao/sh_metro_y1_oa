﻿using System.Web.Http;

namespace GeneralFramework.Infrastructure.Formatting
{
	public class JsonFormatterConfig
	{

		public static void ConfigureFormatter(HttpConfiguration config)
		{
			// Remove Xml formatters. This means when we visit an endpoint from a browser,
			// Instead of returning Xml, it will return Json.
			// More information from Dave Ward: http://jpapa.me/P4vdx6
			config.Formatters.Remove(config.Formatters.XmlFormatter);
			config.Formatters.Remove(config.Formatters.JsonFormatter);
          
            config.Formatters.Insert(0, JsonpMediaTypeFormatter.ApplicationJsonFormatter);
           
		} 
	}
}