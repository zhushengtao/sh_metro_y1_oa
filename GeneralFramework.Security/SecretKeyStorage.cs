﻿using System;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;

namespace GeneralFramework.Security
{
	public sealed class SecretKeyStorage
	{
		static SecretKeyStorage()
		{
			string encriptionKeyRaw = "322b6077537341725855564e6d2fc3246a4f6721304864636a26674a5465636f743943656f4b4a544beb6775437c675c7a6c2042405d7741497d3a6264722c";

			byte[] keyBytes = StringToByteArray(encriptionKeyRaw);

			SHA512Managed sha = new SHA512Managed();
			sha.Initialize();
			sha.TransformBlock(keyBytes, 0, keyBytes.Length, keyBytes, 0);

			byte[] saltData = GetBytes("This is salt for encrypted entities");

			sha.TransformFinalBlock(saltData, 0, saltData.Length);

			SecretKey = sha.Hash;
		}

		private static byte[] StringToByteArray(string hex)
		{
			return Enumerable.Range(0, hex.Length)
							 .Where(x => x % 2 == 0)
							 .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
							 .ToArray();
		}


		static byte[] GetBytes(string str)
		{
			byte[] bytes = new byte[str.Length * sizeof(char)];
			System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
			return bytes;
		}

		public static byte[] SecretKey { get; private set; }
	}
}