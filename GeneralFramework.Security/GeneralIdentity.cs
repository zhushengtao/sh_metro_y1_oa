﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using GeneralFramework.Model;
using Newtonsoft.Json;

namespace GeneralFramework.Security
{
    public class GeneralIdentity : ClaimsIdentity
    {

        private const string IdentityProvider = "General Identity";

        private GeneralIdentity(ClaimsIdentity claimsIdentity) : base(claimsIdentity) { }

        private GeneralIdentity(ClaimsIdentity claimsIdentity, string authenticattioType) :
            base(claimsIdentity, null, authenticattioType, claimsIdentity.NameClaimType, claimsIdentity.RoleClaimType)
        {
        }

        public string Fullname { get; private set; }
        public string Nickname { get; private set; }
        public string Username { get; private set; }
        public string Language { get; private set; }
        public Guid SystemUserId { get; private set; }

        private void Initialize()
        {
            SystemUserId = new Guid(FindFirst(ClaimTypes.NameIdentifier).Value);
            Fullname = FindFirst(ApplicationClaimTypes.Fullname).Value;
            Username = FindFirst(ClaimTypes.Name).Value;
            Nickname = FindFirst(ApplicationClaimTypes.Nickname).Value;            
        }

        public static GeneralIdentity Create(SystemUser user, string authenticationType)
        {
            return CreateFromClaimsIdentity(new ClaimsIdentity(GetClaims(user)), authenticationType);
        }

        public static GeneralIdentity CreateFromClaimsIdentity(ClaimsIdentity claimsIdentity)
        {
            return CreateFromClaimsIdentity(claimsIdentity, claimsIdentity.AuthenticationType);
        }

        private static List<Claim> GetClaims(SystemUser user)
        {
            List<Claim> claims = new List<Claim>();

            claims.Add(new Claim(ClaimTypes.Name, user.Username));
            claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
            claims.Add(new Claim(ApplicationClaimTypes.IdentityProviderClaimType, IdentityProvider));
            claims.Add(new Claim(ApplicationClaimTypes.Fullname, user.FullName ?? user.Username));
            claims.Add(new Claim(ApplicationClaimTypes.Nickname, user.NickName ?? user.Username));
            claims.Add(new Claim("lastLoginTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));

            return claims;
        }

        public static GeneralIdentity CreateFromClaimsIdentity(ClaimsIdentity claimsIdentity, string authenticationType)
        {
            var result = new GeneralIdentity(claimsIdentity, authenticationType);
            try
            {
                result.Initialize();
                return result;
            }
            catch(Exception ex)
            {
                return null;
            }
        }



        private static class ApplicationClaimTypes
        {
            private const string Prefix = "General";
            public const string Fullname = Prefix + "Fullname";
            public const string Nickname = Prefix + "Nickname";
            public const string Language = Prefix + "ULanguage";

            public const string IdentityProviderClaimType = "http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider";
        }
    }
}