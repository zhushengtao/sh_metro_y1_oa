﻿using System;
using System.Threading;
using System.Web;
namespace GeneralFramework.Security
{
    public class BaseSessionInfo : ISessionInfo
    {
        public GeneralIdentity CurrentIdentity
        {
            get
            {
                var identity = HttpContext.Current == null ? Thread.CurrentPrincipal.Identity as GeneralIdentity : HttpContext.Current.User == null ? null : HttpContext.Current.User.Identity as GeneralIdentity;
                return identity;
            }
        }

        public Guid? CurrentUserId
        {
            get
            {
                if (CurrentIdentity != null)
                {
                    return CurrentIdentity.SystemUserId;
                }
                return null;
            }
        }
    }
}