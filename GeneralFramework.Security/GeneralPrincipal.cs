﻿using System.Security.Claims;
using System.Security.Principal;

namespace GeneralFramework.Security
{
	public class GeneralPrincipal : ClaimsPrincipal
	{
        public GeneralPrincipal(IIdentity identity)
			: base(identity)
		{
		}
	}
}