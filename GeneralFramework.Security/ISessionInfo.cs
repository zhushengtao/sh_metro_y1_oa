﻿using System;

namespace GeneralFramework.Security
{
    public interface ISessionInfo
    {
		Guid? CurrentUserId { get; }

        GeneralIdentity CurrentIdentity { get; }
    }
}
