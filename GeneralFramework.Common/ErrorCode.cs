﻿namespace GeneralFramework.Common
{
    public enum ErrorCode
    {
        None = 0,
        Existed = 1,
        Unauthorized = 100003,
        DatabaseError=9999999,
            InvalidPassword=1000002,
    }
}
