﻿namespace GeneralFramework.Common.Interfaces
{
    public interface ISoftDelete
    {
        bool IsDeleted { get; set; }
      
    }
}
