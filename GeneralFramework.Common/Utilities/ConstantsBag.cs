﻿using System;
using System.Globalization;

namespace GeneralFramework.Common.Utilities
{
    public static class ConstantsBag
    {
        public const string DateTimeFormatOfService = "yyyy-MM-dd HH:mm:ss";
        /// <summary>
        /// 当前用户的Session Key
        /// </summary>
        public const string CurrentUserKey = "currentUser";

    }
}
