﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Cache;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace GeneralFramework.Common.Utilities
{
    public class WebHelper
    {
        /// <summary>
        /// 统一发送请求
        /// </summary>
        /// <param name="demo"></param>
        /// <returns></returns>
        public static string SendHttpRequest(Parameters demo)
        {
            ServicePointManager.DefaultConnectionLimit = 500;
            //Json格式请求数据
            string requestData = demo.PostData;

            //拼接请求
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(demo.RequestURI);



            // HTTPS
            if (demo.NeedSSL)
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                ServicePointManager.ServerCertificateValidationCallback = RemoteCertificateCallback;
            }
            if (demo.Timeout.HasValue)
            {
                myRequest.Timeout = 1000 * demo.Timeout.Value;
            }
            //编码
            byte[] buf = System.Text.Encoding.GetEncoding(demo.Encoding).GetBytes(requestData);

            //请求方式
            myRequest.Method = demo.PostType;
            myRequest.ContentLength = buf.Length;
            myRequest.Accept = demo.Accept;

            //指定为文本格式否则会出错
            myRequest.ContentType = demo.ContentType;
            myRequest.MaximumAutomaticRedirections = 1;
            myRequest.AllowAutoRedirect = true;

            if (demo.NeedCookie)
            {
                myRequest.CookieContainer = demo.Cookie;
            }

            if (demo.NeedCredentials)
            {
                myRequest.Credentials = new NetworkCredential(demo.Username, demo.Password);
            }

            if (demo.Header != null)
            {
                foreach (string k in demo.Header.Keys)
                {
                    myRequest.Headers[k] = demo.Header[k];
                }
            }

            string ReqResult = "";
            try
            {
                if (demo.PostType == Parameters.ConstPost)
                {
                    Stream newStream = myRequest.GetRequestStream();
                    newStream.Write(buf, 0, buf.Length);
                    newStream.Close();
                }
                HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                StreamReader reader = new StreamReader(myResponse.GetResponseStream(), Encoding.UTF8);
                ReqResult = reader.ReadToEnd();
                reader.Close();
                myResponse.Close();
            }
            catch (Exception er)
            {
                //todo delete just test
                throw er;
            }

            return ReqResult;
        }


        /// <summary>
        /// 统一发送请求
        /// </summary>
        /// <param name="demo"></param>
        /// <returns></returns>
        public static string SendHttpRequest(Parameters _params, out string res)
        {
            GC.Collect();
            ServicePointManager.DefaultConnectionLimit = 500;
            //Json格式请求数据
            string requestData = _params.PostData;

            //拼接请求
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(_params.RequestURI);

            // HTTPS
            if (_params.NeedSSL)
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                ServicePointManager.ServerCertificateValidationCallback = RemoteCertificateCallback;
            }
            if (_params.Timeout.HasValue)
            {
                myRequest.Timeout = 1000 * _params.Timeout.Value;
            }
            myRequest.KeepAlive = _params.KeepAlived;
            myRequest.CachePolicy = new RequestCachePolicy(RequestCacheLevel.NoCacheNoStore);

            //编码
            byte[] buf = System.Text.Encoding.GetEncoding(_params.Encoding).GetBytes(requestData);

            //请求方式
            myRequest.Method = _params.PostType;
            if (!string.IsNullOrEmpty(requestData))
                myRequest.ContentLength = buf.Length;

            //指定为文本格式否则会出错
            myRequest.ContentType = _params.ContentType;

            myRequest.AllowAutoRedirect = _params.AllowAutoRedirect;
            myRequest.Accept = _params.Accept;
            myRequest.UserAgent = _params.UserAgent;

            myRequest.MaximumAutomaticRedirections = 1;
            myRequest.AllowAutoRedirect = true;

            if (_params.NeedCookie)
            {
                myRequest.CookieContainer = _params.Cookie;
            }

            if (_params.NeedCredentials)
            {
                myRequest.Credentials = new NetworkCredential(_params.Username, _params.Password);
            }
            //默认为HttpVersion.Version11
            if (_params.NotUseDefaultHttpVersion)
            {
                myRequest.ProtocolVersion = HttpVersion.Version10;
            }
            string ReqResult = "";
            try
            {
                if (_params.PostType == Parameters.ConstPost || _params.PostType == Parameters.ConstPut)
                {
                    Stream newStream = myRequest.GetRequestStream();
                    newStream.Write(buf, 0, buf.Length);
                    newStream.Close();
                }
                HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                StreamReader reader = new StreamReader(myResponse.GetResponseStream(), Encoding.GetEncoding(string.IsNullOrEmpty(_params.OutEncoding) ? _params.Encoding : _params.OutEncoding));
                ReqResult = reader.ReadToEnd();
                reader.Close();
                myResponse.Close();
                myResponse = null;
                res = "succeed";
            }
            catch (Exception er)
            {
                ReqResult = res = er.Message;
                //throw er;
            }
            finally
            {
                if (myRequest != null)
                {
                    myRequest.Abort();
                    myRequest = null;
                }
            }

            return ReqResult;
        }

        /// <summary>
        /// 统一发送请求
        /// </summary>
        /// <param name="demo"></param>
        /// <returns></returns>
        public static string SendHttpRequestForPICCPolicy(Parameters demo)
        {
            ServicePointManager.DefaultConnectionLimit = 500;
            //Json格式请求数据
            string requestData = demo.PostData;

            //拼接请求
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(demo.RequestURI);

            // HTTPS
            if (demo.NeedSSL)
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                ServicePointManager.ServerCertificateValidationCallback = RemoteCertificateCallback;
            }
            if (demo.Timeout.HasValue)
            {
                myRequest.Timeout = 1000 * demo.Timeout.Value;
            }
            //编码
            byte[] buf = System.Text.Encoding.GetEncoding(demo.Encoding).GetBytes(requestData);

            //请求方式
            myRequest.Method = demo.PostType;
            myRequest.ContentLength = buf.Length;
            myRequest.Accept = demo.Accept;

            //指定为文本格式否则会出错
            myRequest.ContentType = demo.ContentType;
            myRequest.MaximumAutomaticRedirections = 1;
            myRequest.AllowAutoRedirect = false;
            myRequest.UserAgent = demo.UserAgent;
            myRequest.KeepAlive = true;

            if (demo.NeedCookie)
            {
                myRequest.CookieContainer = demo.Cookie;
            }

            if (demo.NeedCredentials)
            {
                myRequest.Credentials = new NetworkCredential(demo.Username, demo.Password);
            }

            if (demo.Header != null)
            {
                foreach (string k in demo.Header.Keys)
                {
                    myRequest.Headers[k] = demo.Header[k];
                }
            }

            string ReqResult = "";
            try
            {
                if (demo.PostType == Parameters.ConstPost)
                {
                    Stream newStream = myRequest.GetRequestStream();
                    newStream.Write(buf, 0, buf.Length);
                    newStream.Close();
                }
                HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                StreamReader reader = new StreamReader(myResponse.GetResponseStream(), Encoding.UTF8);
                ReqResult = reader.ReadToEnd();
                reader.Close();
                myResponse.Close();
            }
            catch (Exception er)
            {
                //todo delete just test
                throw er;
            }

            return ReqResult;
        }
        /// <summary>
        /// 统一发送请求
        /// </summary>
        /// <param name="demo"></param>
        /// <returns></returns>
        public static string SendHttpRequest(Parameters demo, ref CookieContainer cookie)
        {
            //Json格式请求数据
            string requestData = demo.PostData;

            //拼接请求
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(demo.RequestURI);

            // HTTPS
            if (demo.NeedSSL)
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                ServicePointManager.ServerCertificateValidationCallback = RemoteCertificateCallback;
            }

            //编码
            byte[] buf = System.Text.Encoding.GetEncoding(demo.Encoding).GetBytes(requestData);

            //请求方式
            myRequest.Method = demo.PostType;
            myRequest.ContentLength = buf.Length;

            //指定为文本格式否则会出错
            myRequest.ContentType = demo.ContentType;
            myRequest.MaximumAutomaticRedirections = 1;
            myRequest.AllowAutoRedirect = false;

            if (demo.NeedCredentials)
            {
                myRequest.Credentials = new NetworkCredential(demo.Username, demo.Password);
            }

            if (demo.PostType == Parameters.ConstPost)
            {
                Stream newStream = myRequest.GetRequestStream();
                newStream.Write(buf, 0, buf.Length);
                newStream.Close();
            }

            HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();

            CookieContainer _cookies = new CookieContainer();
            foreach (var a in myResponse.Headers["Set-Cookie"].Replace("path=/", "").Replace("HttpOnly,", "").Split(';'))
            {
                var str = a.Trim();
                if (!string.IsNullOrEmpty(str))
                {
                    _cookies.Add(new Cookie(str.Split('=')[0], str.Split('=')[1], "/", new Uri(demo.CookieURL).Host));
                }
            }

            cookie = _cookies;

            StreamReader reader = new StreamReader(myResponse.GetResponseStream(), Encoding.UTF8);
            string ReqResult = reader.ReadToEnd();
            reader.Close();
            myResponse.Close();

            return ReqResult;
        }

        public static CookieContainer GetCookies(string url, string cookieName, string cookieURL)
        {
            CookieContainer _cookies = new CookieContainer();

            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            ServicePointManager.ServerCertificateValidationCallback = RemoteCertificateCallback;

            request.Method = "GET";
            request.UserAgent =
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36";
            request.CookieContainer = new CookieContainer();

            HttpWebResponse res;
            try
            {
                res = (HttpWebResponse)request.GetResponse();

                string _cookieName = "", _cookieValue = "";
                int i = 0;
                foreach (Cookie _cookie in res.Cookies)
                {
                    if (i > 0)
                        break;

                    _cookieName = _cookie.Name;
                    _cookieValue = _cookie.Value;

                    i++;
                }

                _cookies.Add(
                    new Uri(cookieURL),
                    new Cookie(cookieName, _cookieValue));
            }
            catch (WebException ex)
            {
                res = (HttpWebResponse)ex.Response;
                _cookies = null;
            }

            return _cookies;
        }

        private static bool RemoteCertificateCallback(Object sender, X509Certificate certificate, X509Chain chain,
            SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }


        public static byte[] Get(string url, out WebHeaderCollection responseHeaders)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            //https support
            if (url.StartsWith("https"))
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                ServicePointManager.ServerCertificateValidationCallback = RemoteCertificateCallback;
            }

            using (var response = request.GetResponse())
            {
                using (var responseStream = response.GetResponseStream())
                {
                    responseHeaders = response.Headers;
                    return responseStream.ReadBytes();
                }
            }
        }
    }

    [Serializable]
    public class Parameters
    {
        public Parameters()
        {
            PostData = string.Empty;
            NeedCredentials = true;
            NeedSSL = false;
            PostType = "GET";
            ContentType = "application/json";
            Encoding = "UTF-8";
            NeedCookie = false;
            KeepAlived = true;
        }

        public Parameters(string requestURI, string postData, string postType = ConstPost, string contentType = ConstContentType, string encoding = ConstEncoding)
        {
            ContentType = string.IsNullOrEmpty(contentType) ? ConstContentType : contentType;
            Encoding = string.IsNullOrEmpty(encoding) ? ConstEncoding : encoding;
            PostType = string.IsNullOrEmpty(postType) ? ConstPost : postType;
            RequestURI = requestURI;
            PostData = postData;
        }

        public Parameters(string requestURI, string postData, bool needCredentials, bool needSSL, string username, string password, string postType = ConstPost, string contentType = ConstContentType, string encoding = ConstEncoding)
        {
            ContentType = string.IsNullOrEmpty(contentType) ? ConstContentType : contentType;
            Encoding = string.IsNullOrEmpty(encoding) ? ConstEncoding : encoding;
            PostType = string.IsNullOrEmpty(postType) ? ConstPost : postType;
            RequestURI = requestURI;
            PostData = postData;
            Username = username;
            Password = password;
            NeedCredentials = needCredentials;
            NeedSSL = needSSL;
        }
        /// <summary>
        /// 请求方式
        /// </summary>
        public string PostType { set; get; }

        /// <summary>
        /// 文本格式
        /// </summary>
        public string ContentType { set; get; }

        /// <summary>
        /// 编码方式
        /// </summary>
        public string Encoding { set; get; }

        /// <summary>
        /// 请求地址
        /// </summary>
        public string RequestURI { set; get; }


        /// <summary>
        /// AllowAutoRedirect 
        /// </summary>
        public bool AllowAutoRedirect { set; get; }


        /// <summary>
        /// Accept 
        /// </summary>
        public string Accept { set; get; }


        /// <summary>
        /// UserAgent
        /// </summary>
        public string UserAgent { set; get; }


        /// <summary>
        /// 提交数据
        /// </summary>
        public string PostData { set; get; }

        /// <summary>
        /// Webservice 用户名
        /// </summary>
        public string Username { set; get; }

        /// <summary>
        /// Webservice 密码
        /// </summary>
        public string Password { set; get; }

        /// <summary>
        /// Credentials 身份验证
        /// </summary>
        public bool NeedCredentials { set; get; }

        /// <summary>
        /// NeedSSL HTTPS请求
        /// </summary>
        public bool NeedSSL { set; get; }

        /// <summary>
        /// CookieName
        /// </summary>
        public string CookieName { set; get; }


        /// <summary>
        /// NeedCookie
        /// </summary>
        public bool NeedCookie { set; get; }
        /// <summary>
        /// Cookie
        /// </summary>
        public CookieContainer Cookie { set; get; }

        /// <summary>
        /// 超时时间(秒)
        /// </summary>
        public int? Timeout { get; set; }

        /// <summary>
        /// 保持连接
        /// </summary>
        public bool KeepAlived { get; set; }
        public string OutEncoding { set; get; }
        public WebHeaderCollection Header { get; set; }

        /// <summary>
        /// 默认为11 部分api需要使用10
        /// </summary>
        public bool NotUseDefaultHttpVersion { get; set; }
        /// <summary>
        /// CookieURL
        /// </summary>
        public string CookieURL { set; get; }

        public const string ConstGet = "GET";
        public const string ConstPost = "POST";
        public const string ConstPut = "PUT";
        public const string ConstContentType = "application/json";
        public const string ConstEncoding = "UTF-8";
        public const string ConstEncodingGbk = "GBK";
        public const string ConstContentTypeForm = "application/x-www-form-urlencoded";
    }

    static class StreamExtensions
    {
        /// <summary>
        /// 将流读取成字节组。
        /// </summary>
        /// <param name="stream">流。</param>
        /// <returns>字节组。</returns>
        public static byte[] ReadBytes(this Stream stream)
        {
            if (!stream.CanRead)
                throw new NotSupportedException(stream + "不支持读取。");

            Action trySeekBegin = () =>
            {
                if (!stream.CanSeek)
                    return;

                stream.Seek(0, SeekOrigin.Begin);
            };

            trySeekBegin();

            var list = new List<byte>(stream.CanSeek ? (stream.Length > int.MaxValue ? int.MaxValue : (int)stream.Length) : 300);

            int b;

            while ((b = stream.ReadByte()) != -1)
                list.Add((byte)b);

            trySeekBegin();

            return list.ToArray();
        }
    }
}
