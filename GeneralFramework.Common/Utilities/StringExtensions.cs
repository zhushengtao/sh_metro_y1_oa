﻿using System;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace GeneralFramework.Common.Utilities
{
    public static class StringExtensions
    {

        /// <summary>
        /// Splits string representation of <paramref name="value"/> by capital letters (e.g. "InitialString" → "Initial String")
        /// </summary>
        public static string SplitCamelCase<T>(this T value)
        {
            Regex regex = new Regex(@"
				(?<=[A-Z])(?=[A-Z][a-z]) |
				(?<=[^A-Z])(?=[A-Z]) |
				(?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);

            string resultString = regex.Replace(value.ToString(), " ");
            return resultString;
        }

        public static bool IgnoreCaseEquals(this string first, string second)
        {
            bool result = string.Equals(first, second, StringComparison.InvariantCultureIgnoreCase);
            return result;
        }

        public static string MakeValidFileName(this string name)
        {
            string invalidChars = Regex.Escape(new string(Path.GetInvalidFileNameChars()));
            string invalidRegStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

            return Regex.Replace(name, invalidRegStr, "_");
        }

        public static string LowercaseFirstLetter(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return source;
            }

            string result = Char.ToLowerInvariant(source[0]) + source.Substring(1);
            return result;
        }

        public static string UppercaseFirstLetter(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return source;
            }

            string result = Char.ToUpperInvariant(source[0]) + source.Substring(1);
            return result;
        }

        public static bool IsEmailAddress(this string email)
        {

            try
            {
                MailAddress m = new MailAddress(email);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static bool IsMobliePhone(this string phone)
        {
            return Regex.IsMatch(phone, @"^[1][0-9][0-9]\d{8}$");
        }

        public static Guid? ParseGuid(this string source)
        {
            Guid? result = null;
            Guid g;
            Guid.TryParse(source, out g);
            if (g != Guid.Empty)
            {
                result = g;
            }
            return result;
        }

        /// <summary>
        /// file name extension
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string ExFileName(this string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                return null;
            }

            var array = fileName.Split('.');
            if (array.Length > 1)
            {
                return array[array.Length - 1];
            }

            return null;

        }

        /// <summary>
        /// file name extension
        /// </summary>
        /// <param name="fileFullPath"></param>
        /// <returns></returns>
        public static string FileName(this string fileFullPath, char delimiter)
        {
            if (string.IsNullOrEmpty(fileFullPath))
            {
                return null;
            }

            var lastIndex = fileFullPath.LastIndexOf(delimiter);
            if (lastIndex > 0)
            {
                return fileFullPath.Substring(lastIndex + 1, fileFullPath.Length - lastIndex - 1);
            }

            return null;
        }

        /// <summary>
        /// replace special character
        /// </summary>
        /// <param name="objString"></param>
        /// <returns></returns>
        public static string ReplaceSpecialCharacter(string objString)
        {
            objString = objString.Replace("<", "&lt;");
            objString = objString.Replace(">", "&gt;");
            objString = objString.Replace("&", "&amp;");
            objString = objString.Replace("'", "&apos;");
            objString = objString.Replace("\"", "&quot;");

            return objString.Trim();
        }

        public static string ReplaceHexadecimalCharacter(string text)
        {
            string r = "[\x00-\x08\x0B\x0C\x0E-\x1F\x26]";
            return Regex.Replace(text, r, "", RegexOptions.Compiled);
        }
        /// <summary>
        /// 字符串如果操过指定长度则将超出的部分用指定字符串代替
        /// </summary>
        /// <param name="p_SrcString">要检查的字符串</param>
        /// <param name="p_Length">指定长度</param>
        /// <param name="p_TailString">用于替换的字符串</param>
        /// <returns>截取后的字符串</returns>
        public static string GetSubString(string p_SrcString, int p_Length, string p_TailString)
        {
            string myResult = p_SrcString;

            if (p_Length >= 0)
            {
                byte[] bsSrcString = Encoding.UTF8.GetBytes(p_SrcString);

                if (bsSrcString.Length > p_Length)
                {
                    int nRealLength = p_Length;
                    int[] anResultFlag = new int[p_Length];
                    byte[] bsResult = null;

                    int nFlag = 0;
                    for (int i = 0; i < p_Length; i++)
                    {

                        if (bsSrcString[i] > 127)
                        {
                            nFlag++;
                            if (nFlag == 3)
                            {
                                nFlag = 1;
                            }
                        }
                        else
                        {
                            nFlag = 0;
                        }

                        anResultFlag[i] = nFlag;
                    }

                    if ((bsSrcString[p_Length - 1] > 127) && (anResultFlag[p_Length - 1] == 1))
                    {
                        nRealLength = p_Length + 1;
                    }

                    bsResult = new byte[nRealLength];

                    Array.Copy(bsSrcString, bsResult, nRealLength);

                    myResult = Encoding.UTF8.GetString(bsResult);

                    myResult = myResult + p_TailString;
                }

            }

            return myResult;
        }
        #region 截取字符长度
        /// <summary>
        /// 截取字符长度
        /// </summary>
        /// <param name="inputString">字符</param>
        /// <param name="len">长度</param>
        /// <returns></returns>
        public static string CutString(string inputString, int len)
        {
            if (string.IsNullOrEmpty(inputString))
                return "";
            inputString = DropHTML(inputString);
            ASCIIEncoding ascii = new ASCIIEncoding();
            int tempLen = 0;
            string tempString = "";
            byte[] s = ascii.GetBytes(inputString);
            for (int i = 0; i < s.Length; i++)
            {
                if ((int)s[i] == 63)
                {
                    tempLen += 2;
                }
                else
                {
                    tempLen += 1;
                }

                try
                {
                    tempString += inputString.Substring(i, 1);
                }
                catch
                {
                    break;
                }

                if (tempLen > len)
                    break;
            }
            //如果截过则加上半个省略号 
            byte[] mybyte = System.Text.Encoding.Default.GetBytes(inputString);
            if (mybyte.Length > len)
                tempString += "…";
            return tempString;
        }

        #endregion
        #region 清除HTML标记
        public static string DropHTML(string Htmlstring)
        {
            if (string.IsNullOrEmpty(Htmlstring)) return "";
            //删除脚本  
            Htmlstring = Regex.Replace(Htmlstring, @"<script[^>]*?>.*?</script>", "", RegexOptions.IgnoreCase);
            //删除HTML  
            Htmlstring = Regex.Replace(Htmlstring, @"<(.[^>]*)>", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"([\r\n])[\s]+", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"-->", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"<!--.*", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(quot|#34);", "\"", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(amp|#38);", "&", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(lt|#60);", "<", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(gt|#62);", ">", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(nbsp|#160);", " ", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(iexcl|#161);", "\xa1", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(cent|#162);", "\xa2", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(pound|#163);", "\xa3", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(copy|#169);", "\xa9", RegexOptions.IgnoreCase);

            Htmlstring = Regex.Replace(Htmlstring, @"&#(\d+);", "", RegexOptions.IgnoreCase);
            Htmlstring.Replace("<", "");
            Htmlstring.Replace(">", "");
            Htmlstring.Replace("\r\n", "");
            Htmlstring.Replace("&emsp;", "");
            Htmlstring = HttpContext.Current.Server.HtmlEncode(Htmlstring).Trim();
            return Htmlstring;
        }
        #endregion
    }
}