﻿using System;

namespace GeneralFramework.Common.Utilities
{
    public class Pagination
    {
        private string jsonParams = "{}";

        public int PageSize { get; set; }

        public int PageIndex { get; set; }
        public long RecordCount { get; set; }

        public int PageCount
        {
            get
            {
                if (PageSize <= 0) return 0;

                return (int)Math.Ceiling((double)RecordCount / PageSize);
            }
        }

        /// <summary>
        /// Url
        /// </summary>
        public string HtmlUrl { get; set; }

        /// <summary>
        /// 分页html 容器
        /// </summary>
        public string HtmlElement { get; set; }

        /// <summary>
        /// 参数对象
        /// </summary>
        public string JsonParams
        {
            get
            {
                if (string.IsNullOrEmpty(jsonParams))
                {
                    jsonParams = "{}";
                }

                return jsonParams;

            }
            set
            {
                jsonParams = value;
            }
        }

    }
}
