﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace GeneralFramework.Common.Utilities
{
    public static  class EnumerableExtensions
    {
        /// <summary>
        /// Get certain range
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="step"></param>
        /// <returns></returns>
        public static IEnumerable<double> Range(double min, double max, double step)
        {
            double i;
            for (i = min; i <= max; i += step)
            {
                yield return i;
            }
        }

        /// <summary>
        /// Identifies whether the value is null or empty. 
        /// </summary>
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> value)
        {
            if (value == null)
            {
                return true;
            }

            var collection = value as ICollection;
            if (collection != null)
            {
                return collection.Count == 0;
            }

            bool result = !value.Any();
            return result;
        }
    }
}
