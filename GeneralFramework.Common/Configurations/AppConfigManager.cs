﻿using System;
using System.Configuration;
using System.Globalization;

namespace GeneralFramework.Common.Configurations
{
    public static class AppConfigManager
    {
        #region Private methods

        private static T GetValueFromConfig<T>(string name) where T : IConvertible
        {
            string value = GetValueFromConfig(name);
            T result = (T)Convert.ChangeType(value, typeof(T), CultureInfo.InvariantCulture);
            return result;
        }

        private static string GetValueFromConfig(string name)
        {
            string value = ConfigurationManager.AppSettings[name];
            if (string.IsNullOrWhiteSpace(value))
            {
                string errorMessage = string.Format("Value '{0}' has not been found in Web.config'", name);
                // Logger.LogErrorAndThrow(errorMessage);
            }
            return value;
        }

        #endregion
    }
}
