﻿namespace GeneralFramework.Common
{
    public interface IEntityResponse
    {

        ErrorCode ErrorCode { get; }

        string ErrorMessage { get; }

        bool IsSuccess { get; }

        object Entity { get; }
    }

    public interface IEntityResponse<out T> : IEntityResponse
    {
        new T Entity { get; }
    }
}
