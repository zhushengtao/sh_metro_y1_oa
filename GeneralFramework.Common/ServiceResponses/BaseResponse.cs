﻿
namespace GeneralFramework.Common
{
    public class BaseResponse
    {
        public bool IsSuccess { get { return ErrorCode == ErrorCode.None; } }

        public ErrorCode ErrorCode { get; set; }

        public string ErrorMessage { get; set; }

        public long ResCode
        {
            get
            {
                return (long)this.ErrorCode;
            }
        }
        public string ResDes
        {
            get
            {
                return this.ErrorMessage;
            }
        }

        public BaseResponse()
        {
            ErrorCode = ErrorCode.None;
            ErrorMessage = null;
        }

        public BaseResponse(ErrorCode errorCode, string errorMessage)
        {
            ErrorCode = errorCode;
        
            ErrorMessage = errorMessage;
        }

        public static BaseResponse Success()
        {
            return new BaseResponse(ErrorCode.None, null);
        }
        public static BaseResponse Existed()
        {
            return new BaseResponse(ErrorCode.Existed, null);
        }

        public static BaseResponse Error(ErrorCode errCode, string msg = null)
        {
            return new BaseResponse(errCode, msg);
        }

        public static implicit operator BaseResponse(ErrorCode errCode)
        {
            return Error(errCode, null);
        }
    }
}
