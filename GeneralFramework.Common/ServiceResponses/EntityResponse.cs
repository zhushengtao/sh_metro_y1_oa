﻿using System;

namespace GeneralFramework.Common
{

    public class EntityResponse : BaseResponse, IEntityResponse
    {
        protected EntityResponse(ErrorCode code, string formattedMessage)
            : base(code, formattedMessage)
        {
            Entity = null;
        }

        protected EntityResponse(object entity)
        {
            Entity = entity;
        }

        public static EntityResponse<T> Wrap<T>(BaseResponse r, T entity)
        {
            if (!r.IsSuccess)
            {
                return EntityResponse<T>.Error(r.ErrorCode, r.ErrorMessage);
            }
            EntityResponse<T> result = EntityResponse<T>.Success(entity);
            return result;
        }

        public BaseResponse ToBaseResponse()
        {
            return IsSuccess ? Success() : Error(ErrorCode, ErrorMessage);
        }

        public object Entity { get; protected set; }

    }

    public class EntityResponse<T> : EntityResponse, IEntityResponse<T>
    {
        public EntityResponse(ErrorCode error, string errorMessage)
            : base(error, errorMessage)
        {
        }

        public EntityResponse(T entity, ResponsePagination pagination = null)
            : base(entity)
        {
            Entity = entity;
            Pagination = pagination;
        }


        public new T Entity { get; set; }

        public ResponsePagination Pagination { get; set; }

        public static EntityResponse<T> Success(T entity, ResponsePagination pagination = null)
        {
            EntityResponse<T> result = new EntityResponse<T>(entity, pagination);

            return result;
        }

        public new static EntityResponse<T> Error(ErrorCode errorCode, string formattedmessage = null)
        {
            EntityResponse<T> result = new EntityResponse<T>(errorCode, formattedmessage);
            return result;
        }

        public static implicit operator EntityResponse<T>(T entity)
        {
            EntityResponse<T> result = Success(entity);
            return result;
        }

        public static implicit operator T(EntityResponse<T> entityResponse)
        {
            if (entityResponse.ErrorCode != ErrorCode.None)
            {
                throw new Exception("EntityResponse has an error");
            }

            return entityResponse.Entity;
        }

        public static implicit operator EntityResponse<T>(ErrorCode errorCode)
        {
            EntityResponse<T> result = Error(errorCode);
            return result;
        }
    }
    

    /// <summary>
    /// 返回页码参数
    /// </summary>
    public class ResponsePagination
    {

        public int PageIndex { get; set; }
        public int TotalRecords { get; set; }
        public int PageSize { get; set; }
        public int PageCount { get; set; }
    }


}
