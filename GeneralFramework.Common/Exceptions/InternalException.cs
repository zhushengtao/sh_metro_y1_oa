﻿using System;

namespace GeneralFramework.Common.Exceptions
{
    [Serializable]
    public class InternalException : Exception
    {
        public InternalException(BaseResponse response)
        {
            Response = response;
            ErrorCode = response.ErrorCode;
            ErrorMessage = response.ErrorMessage;
        }

        public InternalException(ErrorCode errorCode, string errorMessage = null)
        {
            ErrorCode = errorCode;
            ErrorMessage = errorMessage;
        }

        public BaseResponse Response { get; private set; }

        public ErrorCode ErrorCode { get; private set; }

        public string ErrorMessage { get; private set; }

        public override string Message
        {
            get { return ErrorMessage ?? ErrorCode.ToString(); }
        }
    }
}