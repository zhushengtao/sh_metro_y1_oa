﻿using GeneralFramework.Common;
using GeneralFramework.EntityFreamework;
using GeneralFramework.Model;
using GeneralFramework.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
namespace GeneralFramework.BLL
{
    public class Human_EmployeeBaseService:BaseService<Human_EmployeeBase>
    {
        public Human_EmployeeBaseService(GeneralContext dbContext)
            : base(dbContext)
        {
        }
        public EntityResponse<Human_EmployeeBase> Create(Human_EmployeeBase param)
        {
            return this.Add(param);
        }
        public Tuple<IEnumerable<Human_EmployeeBaseVM>, int> QueryPaging(Human_EmployeeBaseFilterVM query)
        {
            using (var context = new GeneralContext())
            {
                using (var conn = context.Database.Connection)
                {
                    var sqlFilterWithParam = GetQueryParams(query);

                    var sql =string.Format(@"SELECT
	                                *
                                FROM
	                                (
		                                SELECT
			                                EB.*,JB.JobName,DB.DeptName,CB.Coname, ROW_NUMBER () OVER (ORDER BY EB.EmpWorkID DESC) AS RowNumber
		                                FROM
			                                Human_EmployeeBase EB
		                                LEFT JOIN System_JobBase JB ON EB.EmpJobID = JB.Id
		                                LEFT JOIN System_DepartmentBase DB ON EB.EmpDeptID = DB.Id
		                                LEFT JOIN System_CompanyBase CB ON EB.EmpCoID = DB.Id {0}
	                                ) Page
                                WHERE
	                                Page.RowNumber >@FROM
                                AND Page.RowNumber <= @TO
                                ORDER BY
	                                EmpWorkID DESC;
                                SELECT
	                                COUNT (*)
                                FROM
	                                Human_EmployeeBase EB
                                LEFT JOIN System_JobBase JB ON EB.EmpJobID = JB.Id
                                LEFT JOIN System_DepartmentBase DB ON EB.EmpDeptID = DB.Id
                                LEFT JOIN System_CompanyBase CB ON EB.EmpCoID = DB.Id {0}", sqlFilterWithParam.Item1);
                    using (var multi = conn.QueryMultiple(sql, sqlFilterWithParam.Item2))
                    {
                        var list = multi.Read<Human_EmployeeBaseVM>().ToList();
                        var totalCount = multi.Read<int>().Single();
                        return new Tuple<IEnumerable<Human_EmployeeBaseVM>, int>(list, totalCount);
                    }
                }
            }
        }
        private Tuple<string, DynamicParameters> GetQueryParams(Human_EmployeeBaseFilterVM query)
        {
            if (query == null)
            {
                return null;
            }
            #region generate sqlFilter
            var sqlFilter = " where 1=1 ";
            var paramObj = new DynamicParameters();
            paramObj.Add("from", (query.PageIndex - 1) * query.PageSize);
            paramObj.Add("to", query.PageIndex * query.PageSize);

            if (!string.IsNullOrEmpty(query.Keywords))
            {
                sqlFilter += " and  EB.EmpName like @keywords or EB.EmpTelePhone like @keywords or EB.EmpWorkID LIKE @keywords";
                paramObj.Add("keywords",string.Format("%{0}%",query.Keywords));
            }
            if (query.EmpJobID.HasValue)
            {
                sqlFilter += " and EB.EmpJobID=@empjobid";
                paramObj.Add("empjobid",query.EmpJobID);
            }
            if (query.EmpDeptID.HasValue)
            {
                sqlFilter += " and EB.EmpDeptID=@depid";
                paramObj.Add("depid", query.EmpDeptID);
            }
            #endregion

            return new Tuple<string, DynamicParameters>(sqlFilter, paramObj);
        }
        public BaseResponse EmpExtendMapping(EmpMappingParam param)
        {
            using (var context = new GeneralContext())
            {
                var conn = context.Database.Connection;
                conn.Open();
                var trans = conn.BeginTransaction();
                var sql = string.Empty;
                try
                {
                    context.Database.Connection.Execute("Delete Human_EmpExtendMappingBase WHERE EmpId='" + param.EmpId + "' And ExtType='" + (int)param.ExtType + "'", null, trans);
                    foreach (var item in param.ExtendIds)
                    {
                        context.Database.Connection.Execute("Insert Into Human_EmpExtendMappingBase(Id,EmpId,ObjectId,ExtType,LongId,LineNum,IsDeleted,CreatedOn) Values (newid(),'" + param.EmpId + "','" + item + "','" + (int)param.ExtType + "',0,0,0,getdate())", null, trans);
                    }
                    trans.Commit();
                    return new BaseResponse();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    return ErrorCode.DatabaseError;
                }
            }
        }
        public IEnumerable<Guid> GetExtendEmpMapping(Guid objectId, int type)
        {
            using (var context = new GeneralContext())
            {
                var sql = @"SELECT ObjectId FROM Human_EmpExtendMappingBase WHERE EmpId='" + objectId + "' and ExtType='" + type + "'";
                return context.Database.Connection.Query<Guid>(sql);
            }
        }
        public Human_EmployeeBaseVM GetEmpInfo(Guid id)
        {
            using (var context = new GeneralContext())
            {
                var conn = context.Database.Connection;
                var sqlStr = @"SELECT
	                                HEB.*, SDB.DeptName,
	                                SJB.JobName,
	                                SCB.CoName
                                FROM
	                                Human_EmployeeBase HEB
                                LEFT JOIN System_DepartmentBase SDB ON HEB.EmpDeptID = SDB.Id
                                LEFT JOIN System_JobBase SJB ON HEB.EmpJobID = SJB.Id
                                LEFT JOIN System_CompanyBase SCB ON HEB.EmpCoID = SCB.Id
                                WHERE
	                                HEB.Id =@id";
                return conn.QueryFirst<Human_EmployeeBaseVM>(sqlStr, new { id });
            }
        }

        public void CreateExtendMapping(Guid empId,Guid objectId,int type,string oldObjId="")
        {
            using (var context = new GeneralContext())
            {
                var sql = @"SELECT ObjectId FROM Human_EmpExtendMappingBase WHERE EmpId='" + empId + "' and objectId='" + objectId + "'";
                if (!string.IsNullOrEmpty(oldObjId))
                {
                    var r=context.Database.Connection.Execute("Delete Human_EmpExtendMappingBase WHERE ObjectId='" + oldObjId + "' and EmpId='" + empId + "'");
                }
                var result = context.Database.Connection.Query<Guid>(sql);
                if (!result.Any() && result.Count() == 0)
                {
                    var r1=context.Database.Connection.Execute("Insert Into Human_EmpExtendMappingBase(Id,EmpId,ObjectId,ExtType,LongId,LineNum,IsDeleted,CreatedOn) Values (newid(),'" + empId + "','" + objectId + "','" + type + "',0,0,0,getdate())");
                }
            }
        }
    }
}
