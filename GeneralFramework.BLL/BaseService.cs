﻿using GeneralFramework.EntityFreamework;
using GeneralFramework.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GeneralFramework.Common;
using System.Data.Entity;
using GeneralFramework.Common.Exceptions;
using GeneralFramework.Common.Utilities;

namespace GeneralFramework.BLL
{
    /// <summary>
    /// 单个模型操作服务类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BaseService<T> where T : BaseEntity
    {
        protected GeneralContext DbContext { get; set; }

        public BaseService(GeneralContext dbContext)
        {
            DbContext = dbContext;
        }
        /// <summary>
        /// 缓存实体？默认不缓存
        /// 如果需缓存，更新关联的实体的时候需要单独更新相应的实体。
        /// 由于EF的原因 关联的实体不会更新，需要一个个记录单独更新，
        /// 因为从缓存中读取之后，实体的lazy更新代理已经消失了
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual bool CacheEntity()
        {
            return false;
        }

        public virtual bool CacheTableLocal()
        {
            return false;
        }

        public virtual TimeSpan CacheExpire()
        {
            return TimeSpan.MaxValue;
        }

       
        /// <summary>
        /// 
        /// 查询多条数据的时候，尝试读缓存,避免阻塞，最好是分页读取。
        /// todo  
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public virtual IList<T> GetEntitiesFromQuery(IQueryable<T> query)
        {
            if (CacheEntity())
            {
                return query.ToList();
                var ids = query.Select(q => q.Id).ToList();
                var entities = new List<T>();
                foreach (var id in ids)
                {
                    entities.Add(GetById(id).Entity);
                }
                return entities;
            }
            else
            {
                return query.ToList();
            }
        }

        /// <summary>
        /// 根据对象Id查询记录
        /// </summary>
        /// <param name="id">对象Id</param>
        /// <param name="includeDeleted">是否查询已删除的对象</param>
        /// <param name="forceGet">是否直接从数据库中获取数据</param>
        /// <returns>查询结果</returns>
        public virtual EntityResponse<T> GetById(Guid id, bool includeDeleted = false, bool forceGet = false)
        {
            EntityResponse<T> result = null;
          
            if (result != null && result.IsSuccess && result.Entity != null)
            {
                return result;
            }
            return Execute(() =>
            {
                var entity = DbContext.Set<T>().FirstOrDefault(e => e.Id == id);
                if (!includeDeleted && entity != null && entity.IsDeleted)
                {
                    entity = null;
                }
              
                return entity;
            });
        }

        /// <summary>
        /// 查询全部记录
        /// </summary>
        /// <param name="includeDeleted">是否包括已删除记录</param>
        /// <returns>查询结果</returns>
        public virtual IQueryable<T> GetAll(bool includeDeleted = false)
        {
            if (!includeDeleted)
            {
                return DbContext.Set<T>().Where(x => !x.IsDeleted);
            }
            return DbContext.Set<T>();
        }

        /// <summary>
        /// 添加新的记录
        /// </summary>
        /// <param name="entity">新记录</param>
        /// <returns>新记录</returns>
        protected virtual EntityResponse<T> Add(T entity)
        {

            var response = Execute(() =>
            {
                entity.Id = Guid.NewGuid();
                entity.CreatedOn = DateTime.Now;
                DbContext.Set<T>().Add(entity);
                DbContext.SaveChanges();
                DbContext.Set<T>().Attach(entity);
                return entity;
            });
            return response;
        }

        /// <summary>
        /// 添加新的记录
        /// </summary>
        /// <param name="entity">新记录</param>
        /// <returns>新记录</returns>
        protected virtual EntityResponse<ICollection<T>> Add(ICollection<T> entities)
        {
            var response = Execute(() =>
            {
                foreach (var entity in entities)
                {
                    entity.CreatedOn = DateTime.Now;
                    entity.ModifiedOn = DateTime.Now;
                    DbContext.Set<T>().Add(entity);
                }

                DbContext.SaveChanges();
                //foreach (var entity in entities)
                //{
                //    DbContext.Set<T>().Attach(entity);
                //}

                return entities;
            });

            return response;
        }

        /// <summary>
        /// 更新记录
        /// </summary>
        /// <param name="entity">更新后记录</param>
        /// <returns>更新后记录</returns>
        public virtual EntityResponse<T> Update(T entity)
        {
            var response = Execute(() =>
            {
                entity.ModifiedOn = DateTime.Now;
                DbContext.Set<T>().Attach(entity);
                DbContext.Entry(entity).State = EntityState.Modified;
                DbContext.SaveChanges();
                return entity;
            });
          
            return response;
        }

        public virtual EntityResponse<T> Update(T entity, IEnumerable<string> fields)
        {
            var response = Execute(() =>
            {
                entity.ModifiedOn = DateTime.Now;
                DbContext.Set<T>().Attach(entity);
                foreach (var field in fields)
                {
                    DbContext.Entry(entity).Property(field).IsModified = true;
                }

                DbContext.SaveChanges();
                return entity;
            });
          
            return response;

        }

        /// <summary>
        /// 逻辑删除
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual EntityResponse<T> Delete(T entity)
        {

            var response = Execute(() =>
            {
                entity.IsDeleted = true;
                DbContext.Set<T>().Attach(entity);
                DbContext.Entry(entity).State = EntityState.Modified;
                DbContext.SaveChanges();
                return entity;
            });

           
            return response;
        }

        /// <summary>
        /// 逻辑删除
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual EntityResponse<ICollection<T>> Delete(ICollection<T> entities)
        {

            var response = Execute(() =>
            {
                foreach (var entity in entities)
                {
                    entity.IsDeleted = true;
                    DbContext.Set<T>().Attach(entity);
                    DbContext.Entry(entity).State = EntityState.Modified;
                }

                DbContext.SaveChanges();
                return entities;
            });

           
            return response;
        }

        /// <summary>
        /// 物理删除 从数据库直接删除，慎用
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual EntityResponse<T> Remove(T entity)
        {
            return Execute(() =>
             {
                 DbContext.Set<T>().Attach(entity);
                 DbContext.Entry(entity).State = EntityState.Deleted;
                 DbContext.SaveChanges();
                 return entity;
             });
        }



        /// <summary>
        /// 执行查询任务，反馈查询结果，包装查询任务中的Exception信息
        /// </summary>
        /// <typeparam name="TReturn">返回值类型</typeparam>
        /// <param name="queryFunc">查询任务</param>
        /// <returns>查询结果</returns>
        protected virtual EntityResponse<TReturn> Execute<TReturn>(Func<TReturn> queryFunc)
        {
            try
            {
                return queryFunc.Invoke();
            }
            catch (Exception ex)
            {

                throw new InternalException(ErrorCode.DatabaseError, ex.Message);
            }
        }

        /// <summary>
        /// 执行查询任务，反馈查询结果，包装查询任务中的Exception信息
        /// </summary>
        /// <typeparam name="TReturn">返回值类型</typeparam>
        /// <param name="task">查询任务</param>
        /// <returns>查询结果</returns>
        protected virtual TReturn Execute<TReturn>(Task<TReturn> task)
        {
            try
            {
                task.Start();
                return task.Result;
            }
            catch (Exception ex)
            {
                return EntityResponse<TReturn>.Error(ErrorCode.DatabaseError, ex.Message);
            }
        }


        /// <summary>
        /// 根据指定的筛选器和排序字段进行筛选和排序
        /// </summary>
        /// <typeparam name="OTpye">排序字段类型</typeparam>
        /// <param name="pagination">分页设置</param>
        /// <param name="filter">筛选器</param>
        /// <param name="orderBy">排序字段表达式</param>
        /// <param name="isAsc">是否升序，默认升序</param>
        /// <returns></returns>
        public virtual IQueryable<T> GetDataByPage<OTpye>(Pagination pagination, Expression<Func<T, bool>> filter, Expression<Func<T, OTpye>> orderBy, bool isAsc = true)
        {
            var skip = (pagination.PageIndex - 1) * pagination.PageSize;
            var tike = pagination.PageSize;

            pagination.RecordCount = DbContext.Set<T>().Where(filter).Count();

            if (pagination.RecordCount < skip)
            {
                skip = 0;
                pagination.PageIndex = 1;
            }

            if (isAsc)
            {
                return DbContext.Set<T>().Where(filter).OrderBy(orderBy).Skip(skip).Take(tike);
            }
            else
            {
                return DbContext.Set<T>().Where(filter).OrderByDescending(orderBy).Skip(skip).Take(tike);
            }
        }

        /// <summary>
        /// 根据指定的筛选器和排序字段进行筛选和排序
        /// </summary>
        /// <typeparam name="T">筛选的对象类型</typeparam>
        /// <typeparam name="OTpye">排序字段类型</typeparam>
        /// <param name="pagination">分页设置</param>
        /// <param name="filter">筛选器</param>
        /// <param name="orderBy">排序字段表达式</param>
        /// <param name="isAsc">是否升序，默认升序</param>
        /// <returns></returns>
        public virtual IQueryable<T> GetDataByPage<T, OTpye>(Pagination pagination, Expression<Func<T, bool>> filter, Expression<Func<T, OTpye>> orderBy, bool isAsc = true)
            where T : BaseEntity
        {
            var skip = (pagination.PageIndex - 1) * pagination.PageSize;
            var tike = pagination.PageSize;

            pagination.RecordCount = DbContext.Set<T>().Where(filter).Count();

            if (pagination.RecordCount < skip)
            {
                skip = 0;
                pagination.PageIndex = 1;
            }

            if (isAsc)
            {
                return DbContext.Set<T>().Where(filter).OrderBy(orderBy).ThenByDescending(t => t.CreatedOn).Skip(skip).Take(tike);
            }
            else
            {
                return DbContext.Set<T>().Where(filter).OrderByDescending(orderBy).ThenByDescending(t => t.CreatedOn).Skip(skip).Take(tike);
            }
        }

     
    }
}