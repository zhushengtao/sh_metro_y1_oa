﻿using GeneralFramework.EntityFreamework;
using GeneralFramework.Model;
using GeneralFramework.ViewModel;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Xml;
using Newtonsoft.Json.Linq;
using Formatting = Newtonsoft.Json.Formatting;
using System.Diagnostics;
using System.Net;
using System;
using GeneralFramework.Common;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using Dapper;

namespace GeneralFramework.BLL
{
    public class SystemLogsBaseService:LogsBaseService<LogsBase>
    {
        private static SystemLogsBaseService _logsService;
        public static SystemLogsBaseService Instance
        {
            get
            {
                if (_logsService == null)
                {
                    return new SystemLogsBaseService(new GeneralLogContext());
                }
                else
                {
                    return _logsService;
                }
            }
        }

        public SystemLogsBaseService(GeneralLogContext context) : base(context)
        { }

        public void Create(LogsBase entity, bool saveToDbInstant = true)
        {
            if (entity.Id == Guid.Empty)
            {
                entity.Id = Guid.NewGuid();
                entity.CreatedOn = DateTime.Now;
            }
            base.Add(entity);
        }
        public static ResponseT Execute<ResponseT, ParamT>(string functionName, string typeCode, ParamT param, Func<LogsBase, ResponseT> Handle)
        {
            ResponseT response = default(ResponseT);
            var sw = new Stopwatch();
            sw.Start();
            var log = new LogsBase()
            {
                methodName = functionName,
                typeCode = typeCode,
                tags = LogTags.Info,
                Remarks = Dns.GetHostName()
            };
            try
            {
                if (param is XmlDocument && param != null)
                {
                    log.responseContent = (param as XmlDocument).OuterXml;
                }
                else
                {
                    log.requestContent = JsonConvert.SerializeObject(param);
                }

                response = Handle(log);
                if (response is XmlDocument && response != null)
                {
                    log.responseContent = (response as XmlDocument).OuterXml;
                }
                else
                {
                    log.responseContent = JsonConvert.SerializeObject(response);
                }
                return response;
            }
            catch (Exception ex)
            {
                log.tags = LogTags.Error;

                log.Id = Guid.NewGuid();
                log.AppendLine("Exception:" + ex.ToString());
                throw ex;
            }
            finally
            {
                var baseResponse = response as BaseResponse;
                if (baseResponse != null)
                {
                    if (!baseResponse.IsSuccess)
                    {
                        log.tags = LogTags.Warn;
                    }
                }
                sw.Stop();
                log.duration = sw.ElapsedMilliseconds;
                //trace 的不是很重要，走日志队列
                SystemLogsBaseService.Instance.Create(log, log.tags != LogTags.Trace);
            }
        }
        public static EntityResponse<IEnumerable<LogsVM>> QueryLogs(LogsQueryParam query)
        {
            using (var context = new GeneralLogContext())
            {
                using (var conn = context.Database.Connection)
                {
                    var sqlFilter = " where 1=1 ";
                    //精确查询
                    if (!string.IsNullOrEmpty(query.Keywords))
                    {
                        sqlFilter += string.Format(" and (MethodName='{0}') ", query.Keywords);
                    }
                    var paramObj = new DynamicParameters();
                    paramObj.Add("from", (query.PageIndex - 1) * query.PageSize);
                    paramObj.Add("to", query.PageIndex * query.PageSize);

                    //if (!string.IsNullOrEmpty(query.methodName))
                    //{
                    //    sqlFilter += string.Format(" and MethodName='{0}' ", query.methodName);
                    //}

                    dynamic paramsObj = new ExpandoObject();

                    paramsObj.from = (query.PageIndex - 1) * query.PageSize;
                    paramsObj.to = query.PageIndex * query.PageSize;

                    //if (!string.IsNullOrEmpty(query.typeCode))
                    //{
                    //    paramsObj.typeCodes = query.typeCode.Trim(',').Split(',').ToList();
                    //    sqlFilter += " and typeCode in @typeCodes ";
                    //}

                    //if (!string.IsNullOrEmpty(query.tags))
                    //{
                    //    paramsObj.tags = query.tags.Trim(',').Split(',').ToList();
                    //    sqlFilter += " and tags in @tags ";
                    //}


                    var sql = "select Id,typeCode,tags,objectId,createdOn,duration,methodName "
                              + "from "
                              + "("
                              + " select Id,typeCode,tags,objectId,createdOn,duration,methodName,ROW_NUMBER() OVER(ORDER BY createdOn desc) as RowNumber "
                              + " from LogsBase"
                              + sqlFilter
                              + " )Page "
                              + " Where Page.RowNumber>@from and Page.RowNumber<= @to "
                              + " ORDER BY createdOn DESC  "
                              + " select count(log.Id) from LogsBase as log"
                              + sqlFilter;
                    using (var multi = conn.QueryMultiple(sql, (object)paramsObj, commandTimeout: 60))
                    {
                        var list = multi.Read<LogsVM>().ToList();
                        var totalCount = multi.Read<int>().Single();
                        return new EntityResponse<IEnumerable<LogsVM>>(ErrorCode.None, string.Empty)
                        {
                            Entity = list,
                            Pagination = new ResponsePagination()
                            {
                                PageSize = query.PageSize,
                                PageIndex = query.PageIndex,
                                TotalRecords = totalCount,
                                PageCount = (int)Math.Ceiling((double)totalCount / query.PageSize)
                            }
                        };
                    }

                }
            }
        }
        public void AddTraceInfo(string msg, string code = null)
        {
            msg = msg ?? string.Empty;
            this.Create(new LogsBase()
            {
                requestContent = msg,
                methodName = "userLogin",
                CreatedOn = DateTime.Now,
                tags = LogTags.Trace,
                typeCode = code,
            }, false);
        }

        /// <summary>
        /// 剔除null 且按照字母排序
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string GetJson(object obj)
        {
            //var jsonStr = JsonConvert.SerializeObject(obj);
            //var dic = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStr);
            //var newDic = dic.Where(o => o.Value != null).OrderBy(o => o.Key).ToDictionary(o => o.Key, p => p.Value);
            //return JsonConvert.SerializeObject(newDic);

            //复杂结构的使用下边的：
            var jsonSetting = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
            var jsonStr = JsonConvert.SerializeObject(obj, jsonSetting);
            return SortJson(JToken.Parse(jsonStr));
        }
        private static string SortJson(JToken jobj, JToken obj = null)
        {
            if (obj == null)
            {
                obj = new JObject();
            }
            List<JToken> list = jobj.ToList<JToken>();
            if (jobj.Type == JTokenType.Object)//非数组
            {
                List<string> listsort = new List<string>();
                foreach (var item in list)
                {
                    string name = JProperty.Load(item.CreateReader()).Name;
                    listsort.Add(name);
                }
                listsort.Sort();
                List<JToken> listTemp = new List<JToken>();
                foreach (var item in listsort)
                {
                    listTemp.Add(list.Where(p => JProperty.Load(p.CreateReader()).Name == item).FirstOrDefault());
                }
                list = listTemp;
                //list.Sort((p1, p2) => JProperty.Load(p1.CreateReader()).Name.GetAnsi() - JProperty.Load(p2.CreateReader()).Name.GetAnsi());

                foreach (var item in list)
                {
                    JProperty jp = JProperty.Load(item.CreateReader());
                    if (item.First.Type == JTokenType.Object)
                    {
                        JObject sub = new JObject();
                        (obj as JObject).Add(jp.Name, sub);
                        SortJson(item.First, sub);
                    }
                    else if (item.First.Type == JTokenType.Array)
                    {
                        JArray arr = new JArray();
                        if (obj.Type == JTokenType.Object)
                        {
                            (obj as JObject).Add(jp.Name, arr);
                        }
                        else if (obj.Type == JTokenType.Array)
                        {
                            (obj as JArray).Add(arr);
                        }
                        SortJson(item.First, arr);
                    }
                    else if (item.First.Type != JTokenType.Object && item.First.Type != JTokenType.Array)
                    {
                        (obj as JObject).Add(jp.Name, item.First);
                    }
                }
            }
            else if (jobj.Type == JTokenType.Array)//数组
            {
                foreach (var item in list)
                {
                    List<JToken> listToken = item.ToList<JToken>();
                    if (listToken.Count == 0)
                    {
                        (obj as JArray).Add(item);
                        continue;
                    }
                    List<string> listsort = new List<string>();
                    foreach (var im in listToken)
                    {
                        string name = JProperty.Load(im.CreateReader()).Name;
                        listsort.Add(name);
                    }
                    listsort.Sort();
                    List<JToken> listTemp = new List<JToken>();
                    foreach (var im2 in listsort)
                    {
                        listTemp.Add(listToken.Where(p => JProperty.Load(p.CreateReader()).Name == im2).FirstOrDefault());
                    }
                    list = listTemp;

                    listToken = list;
                    // listToken.Sort((p1, p2) => JProperty.Load(p1.CreateReader()).Name.GetAnsi() - JProperty.Load(p2.CreateReader()).Name.GetAnsi());
                    JObject item_obj = new JObject();
                    foreach (var token in listToken)
                    {
                        JProperty jp = JProperty.Load(token.CreateReader());
                        if (token.First.Type == JTokenType.Object)
                        {
                            JObject sub = new JObject();
                            (obj as JObject).Add(jp.Name, sub);
                            SortJson(token.First, sub);
                        }
                        else if (token.First.Type == JTokenType.Array)
                        {
                            JArray arr = new JArray();
                            if (obj.Type == JTokenType.Object)
                            {
                                (obj as JObject).Add(jp.Name, arr);
                            }
                            else if (obj.Type == JTokenType.Array)
                            {
                                (obj as JArray).Add(arr);
                            }
                            SortJson(token.First, arr);
                        }
                        else if (item.First.Type != JTokenType.Object && item.First.Type != JTokenType.Array)
                        {
                            if (obj.Type == JTokenType.Object)
                            {
                                (obj as JObject).Add(jp.Name, token.First);
                            }
                            else if (obj.Type == JTokenType.Array)
                            {
                                item_obj.Add(jp.Name, token.First);
                            }
                        }
                    }
                    if (obj.Type == JTokenType.Array)
                    {
                        (obj as JArray).Add(item_obj);
                    }

                }
            }
            string ret = obj.ToString(Formatting.None);
            return ret;
        }
    }

    public class LogTypeCode
    {
        public const string WebApi = "10004";
    }
    public class LogTags
    {
        public const string Info = "Info";
        public const string Warn = "Warn";
        public const string Error = "Error";
        public const string Trace = "Trace";
    }
}
