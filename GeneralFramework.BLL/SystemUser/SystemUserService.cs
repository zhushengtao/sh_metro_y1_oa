﻿
using GeneralFramework.EntityFreamework;
using GeneralFramework.Model;
using GeneralFramework.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GeneralFramework.ViewModel;
using GeneralFramework.Common;
using GeneralFramework.Common.Utilities;
using Dapper;

namespace GeneralFramework.BLL
{
    public class SystemUserService : BaseService<SystemUser>
    {
        #region 说明
        //这里面实现与数据库交互的一系列操作
        //业务逻辑简单的创建、修改、查询、显示类操作可直接使用EF来进行操作、业务逻辑复杂，一次请求实现多表操作的建议使用事务进行处理
        //使用sql语句操作的请看下Dapper实体框架的操作模式
        #endregion
        public SystemUserService(GeneralContext dbContext)
            : base(dbContext)
        {
        }

        public BaseResponse Create(SystemUser param)
        {
            return this.Add(param);
        }
        public EntityResponse<SystemUser> CreateSystemUser(
           SystemUser user,
           string password,
           int? referrerCode = null,
           bool checkPassword = true)
        {
            if (!string.IsNullOrWhiteSpace(password) && checkPassword && !PasswordFactory.CheckComplexity(password))
            {
                return new EntityResponse<SystemUser>(ErrorCode.InvalidPassword, "密码错误");
            }

            using (var transaction = DbContext.Database.BeginTransaction())
            {
                if (!string.IsNullOrWhiteSpace(password))
                {
                    PasswordFactory.SetPassword(user, password);
                }

                if (user.Id == Guid.Empty)
                {
                    user.Id = Guid.NewGuid();
                }

                user.CreatedOn = DateTime.Now;
                user.Password = "";
                DbContext.SystemUser.Add(user);
                DbContext.SaveChanges();
                transaction.Commit();
            }

            return user;
        }
        /// <summary>
        /// 根据传入的表达式筛选账号并排序
        /// </summary>
        /// <typeparam name="T">被筛选对象类型</typeparam>
        /// <typeparam name="OTpye">排序条件对象类型</typeparam>
        /// <param name="pagination">分页参数</param>
        /// <param name="filter">筛选条件表达式</param>
        /// <param name="orderBy">排序条件表达式</param>
        /// <param name="isAsc">是否升序</param>
        /// <returns>筛选并排序后的IQueryable对象</returns>
        public IQueryable<T> GetSystemUsers<T, OTpye>(Pagination pagination, Expression<Func<T, bool>> filter, Expression<Func<T, OTpye>> orderBy, bool isAsc = true) where T : SystemUser
        {
            var skip = (pagination.PageIndex - 1) * pagination.PageSize;
            var tike = pagination.PageSize;

            pagination.RecordCount = DbContext.Set<T>().Where(filter).Count();

            if (pagination.RecordCount < skip)
            {
                skip = 0;
                pagination.PageIndex = 1;
            }

            if (isAsc)
            {
                return DbContext.Set<T>().Where(filter).OrderBy(orderBy).ThenByDescending(t => t.CreatedOn).Skip(skip).Take(tike);
            }
            else
            {
                return DbContext.Set<T>().Where(filter).OrderByDescending(orderBy).ThenByDescending(t => t.CreatedOn).Skip(skip).Take(tike);
            }
        }

        public EntityResponse<SystemUser> GetByUsername(string username, bool includeDeleted = false)
        {
            SystemUser acc = GetAll(includeDeleted).FirstOrDefault(x => x.Username == username);
            return acc;
        }
        /// <summary>
        /// 检验用户登陆密码
        /// </summary>
        /// <param name="login">登录名，可以使用户名或手机号或邮箱</param>
        /// <param name="password">密码</param>
        /// <returns>如果校验成功则返回账户，校验失败则返回错误代码</returns>
        public EntityResponse<SystemUser> Validate(string login, string password)
        {
            SystemUser account = GetByLogin(login);

            if (account == null)
            {
                return EntityResponse<SystemUser>.Error(ErrorCode.DatabaseError);
            }
            //bool pass = PasswordFactory.CheckPassword(account, password);
            //if (pass)
            //{

            //    return Update(account);
            //}
            var h = new CryptoHelper();
            if (account.PasswordHash == CryptoHelper.EncryptByBase64(password))
            {
                return account;
            }
            return EntityResponse<SystemUser>.Error(ErrorCode.DatabaseError);
        }
        /// <summary>
        /// 根据用户名或手机号码或邮箱获取用户
        /// </summary>
        /// <param name="login"></param>
        /// <returns>包含账户或错误的EntityResponse</returns>
        public EntityResponse<SystemUser> GetByLogin(string login)
        {
            SystemUser account = GetByUsername(login).Entity ;
            return account;
        }
        /// <summary>
        /// 检验用户登陆密码
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool CheckPassword(SystemUser user, string password)
        {
            return PasswordFactory.CheckPassword(user, password);
        }
        /// <summary>
        /// 修改账户登陆密码
        /// </summary>
        /// <param name="updatedUser">被修改的账户</param>
        /// <param name="password">新密码</param>
        /// <param name="checkComplexity">是否检查新密码的复杂度</param>
        /// <returns>包含修改密码结果的Response</returns>
        public BaseResponse UpdatePassword(SystemUser updatedUser, string password, bool checkComplexity = true)
        {
            if (checkComplexity && !PasswordFactory.CheckComplexity(password))
            {
                return ErrorCode.InvalidPassword;
            }
            SystemUser account = GetById(updatedUser.Id, true);
            if (account == null)
            {
                return BaseResponse.Error(ErrorCode.InvalidPassword, "账号不存在");
            }
            PasswordFactory.SetPassword(account, password);
            //account.ResetPasswordToken = null;
            //account.ResetPasswordTime = null;
            if (string.IsNullOrEmpty(updatedUser.MobilePhone))
            {
                account.MobilePhone = updatedUser.MobilePhone;
            }

            // account.LastLoginOn = DateTime.Now;
            DbContext.Entry(account).State = EntityState.Modified;
            DbContext.SaveChanges();

            return BaseResponse.Success();
        }

      
        /// <summary>
        /// 创建DEPT映射
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>

        public IEnumerable<Guid> GetUserCatalogsIdsFromDb(Guid systemUserId)
        {
            using (var context = new GeneralContext())
            {
                var sql = @"SELECT
	                            Id
                            FROM
	                            System_MenuBase
                            WHERE
	                            ID IN (
		                            SELECT
			                            MenuId
		                            FROM
			                            System_MenuMappingBase
		                            WHERE
			                            ObjectId IN (
				                            SELECT
					                            ObjectId
				                            FROM
					                            Human_EmpExtendMappingBase
				                            WHERE
					                            EmpId = (
						                            SELECT
							                            UserEmpID
						                            FROM
							                            System_UserBase
						                            WHERE
							                            Id =@systemUserId
					                            )
			                            )
	                            )";
                return context.Database.Connection.Query<Guid>(sql, new { systemUserId });
            }
        }

    }



}
