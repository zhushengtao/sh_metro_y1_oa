﻿using GeneralFramework.Common;
using GeneralFramework.EntityFreamework;
using GeneralFramework.Model;
using GeneralFramework.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
namespace GeneralFramework.BLL
{
    public class System_RoleBaseService:BaseService<System_RolesBase>
    {
        public System_RoleBaseService(GeneralContext dbContext)
            : base(dbContext)
        {
        }

        public BaseResponse Create(System_RolesBase param)
        {
            return this.Add(param);
        }
        public bool IsExists(string name, Guid? id = null)
        {
            return DbContext.System_RolesBase.Any(x => x.RoleName == name && (id.HasValue ? x.Id != id : true) && !x.IsDeleted);
        }
    }
}
