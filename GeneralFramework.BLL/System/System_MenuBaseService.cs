﻿using Dapper;
using GeneralFramework.Common;
using GeneralFramework.EntityFreamework;
using GeneralFramework.Model;
using GeneralFramework.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFramework.BLL
{
    public class System_MenuBaseService:BaseService<System_MenuBase>
    {
        public System_MenuBaseService(GeneralContext dbContext)
            : base(dbContext)
        {
        }

        public BaseResponse Create(System_MenuBase param)
        {
            return this.Add(param);
        }
        public bool IsExists(string name, Guid id, Guid? parentId = null)
        {
            return DbContext.System_MenuBase.Any(a => a.MenuName == name && a.Id != id && !a.IsDeleted && a.MenuParentID == parentId);
        }
        public virtual EntityResponse<IList<System_MenuBase>> GetByParentId(Guid? id)
        {
            try
            {
                if (id == Guid.Empty)
                {
                    id = null;
                }

                return DbContext.System_MenuBase.Where(r => r.MenuParentID == id && !r.IsDeleted).OrderBy(o => o.LineNum).ToList();
            }
            catch (Exception ex)
            {
                return ErrorCode.DatabaseError;
            }
        }
        /// <summary>
        /// 创建菜单映射
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public BaseResponse MenuMapping(MenuMappingParam param)
        {
            using (var context = new GeneralContext())
            {
                var conn = context.Database.Connection;
                conn.Open();
                var trans = conn.BeginTransaction();
                var sql = string.Empty;
                try
                {
                    var mType = (int)param.MenuMapingType;
                    var objectId = param.ObjectId;
                    RemoveMenuMappingByObjectId(objectId,mType);
                    var addList = new List<Guid>();
                    foreach (var item in param.MenuIds)
                    {
                        if (MenuIsExistend(item,mType))
                        {
                            continue;
                        }
                      
                        //var entity = this.GetById(item).Entity;
                        //if (entity.MenuParentID != null)
                        //{
                        //    addList.Add(entity.MenuParentID.Value);
                        //}
                        addList.Add(item);
                    }
                    addList = addList.Distinct().ToList();
                    foreach (var item in addList)
                    {
                        context.Database.Connection.Execute("Insert Into System_MenuMappingBase(Id,MenuId,MappingType,ObjectId,LongId,LineNum,IsDeleted,CreatedOn) Values (newid(),'" + item + "','" + mType + "','" + objectId + "',0,0,0,getdate())", null, trans);
                    }
                    trans.Commit();
                    return new BaseResponse();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    return ErrorCode.DatabaseError;
                }
            }
        }
        public bool RemoveMenuMappingByObjectId(Guid objectId,int type)
        {
            using (var context = new GeneralContext())
            {
                var conn = context.Database.Connection;
                conn.Open();
                try {
                    string sql = "Delete System_MenuMappingBase WHERE ObjectId = '" + objectId + "' and MappingType='"+ type + "'";
                    context.Database.Connection.Execute(sql);
                    return true;
                }
                catch (Exception ex)
                {
                
                    return false;
                }
            }
        }
        public bool MenuIsExistend(Guid menuId,int type)
        {

            using (var context = new GeneralContext())
            {
                var conn = context.Database.Connection;
                var sqlStr = @"SELECT * FROM System_MenuMappingBase WHERE MenuId=@menuId and MappingType=@type";
                return conn.Query(sqlStr, new { menuId,type }).Any();
            }
        }
        public IEnumerable<JsTreeVM> GetUserCatalogs(Guid UserId)
        {
            
            return null;
        }

        public IEnumerable<Guid> GetExtendMenuMapping(Guid objectId, int type)
        {
            using (var context = new GeneralContext())
            {
                var sql = @"SELECT MenuId FROM System_MenuMappingBase WHERE ObjectId='"+objectId+"' and MappingType='"+type+"'";
                return context.Database.Connection.Query<Guid>(sql);
            }
        }
    }
}
