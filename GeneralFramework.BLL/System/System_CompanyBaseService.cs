﻿using GeneralFramework.Common;
using GeneralFramework.EntityFreamework;
using GeneralFramework.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFramework.BLL
{
    public class System_CompanyBaseService : BaseService<System_CompanyBase>
    {
        public System_CompanyBaseService(GeneralContext dbContext)
            : base(dbContext)
        {
        }

        public BaseResponse Create(System_CompanyBase param)
        {
            return this.Add(param);
        }
    }
}
