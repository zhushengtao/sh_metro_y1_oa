﻿using GeneralFramework.Common;
using GeneralFramework.EntityFreamework;
using GeneralFramework.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFramework.BLL
{
    public class System_DepartmentBaseServicec:BaseService<System_DepartmentBase>
    {
        public System_DepartmentBaseServicec(GeneralContext dbContext)
            : base(dbContext)
        {
        }

        public BaseResponse Create(System_DepartmentBase param)
        {
            return this.Add(param);
        }
        public bool IsExists(string name, Guid id, Guid? parentId = null)
        {
            return DbContext.System_DepartmentBase.Any(a => a.DeptName == name && a.Id != id && !a.IsDeleted && a.DeptParentID == parentId);
        }
        public virtual EntityResponse<IList<System_DepartmentBase>> GetByParentId(Guid? id)
        {
            try
            {
                if (id == Guid.Empty)
                {
                    id = null;
                }

                return DbContext.System_DepartmentBase.Where(r => r.DeptParentID == id && !r.IsDeleted).OrderBy(o => o.LineNum).ToList();
            }
            catch (Exception ex)
            {
                return ErrorCode.DatabaseError;
            }
        }
        public EntityResponse<System_DepartmentBase> GetByDepartmentId(Guid? id)
        {
            try
            {
                return DbContext.System_DepartmentBase.FirstOrDefault(a => a.Id == id && !a.IsDeleted);
            }
            catch (Exception ex)
            {
                return ErrorCode.DatabaseError;
            }
        }



       
    }
}
