﻿using GeneralFramework.Common;
using GeneralFramework.EntityFreamework;
using GeneralFramework.Model;
using GeneralFramework.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
namespace GeneralFramework.BLL
{
    public class System_JobBaseService:BaseService<System_JobBase>
    {
        public System_JobBaseService(GeneralContext dbContext)
            : base(dbContext)
        {
        }

        public BaseResponse Create(System_JobBase param)
        {
            return this.Add(param);
        }
        public bool IsExists(string name, Guid id, Guid? parentId = null)
        {
            return DbContext.System_JobBase.Any(a => a.JobName == name && a.Id != id && !a.IsDeleted && a.JobParentID == parentId);
        }
        public virtual EntityResponse<IList<System_JobBase>> GetByParentId(Guid? id)
        {
            try
            {
                if (id == Guid.Empty)
                {
                    id = null;
                }

                return DbContext.System_JobBase.Where(r => r.JobParentID == id && !r.IsDeleted).OrderBy(o => o.LineNum).ToList();
            }
            catch (Exception ex)
            {
                return ErrorCode.DatabaseError;
            }
        }
        public EntityResponse<System_JobBase> GetByJobId(Guid? id)
        {
            try
            {
                return DbContext.System_JobBase.FirstOrDefault(a => a.Id == id && !a.IsDeleted);
            }
            catch (Exception ex)
            {
                return ErrorCode.DatabaseError;
            }
        }

        public System_JobBaseVM GetJobInfo(Guid id)
        {
            using (var context = new GeneralContext())
            {
                var conn = context.Database.Connection;
                var sqlStr = @"SELECT
	                                JB.*, DB.DeptName,
	                                CB.CoName as CompanyName,JBB.JobName as JobParaentName
                                FROM
	                                System_JobBase JB
                                LEFT JOIN System_DepartmentBase DB ON DB.Id = JB.JobDeptID
                                LEFT JOIN System_CompanyBase CB ON CB.Id = JB.JobCoID
                                LEFT JOIN System_JobBase JBB ON JB.JobParentID=JBB.Id

                                WHERE JB.Id=@id";
                return conn.QueryFirst<System_JobBaseVM>(sqlStr,new {id});
            }
        }
    }
}
